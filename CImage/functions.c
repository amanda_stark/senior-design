

/*
 *  functions.c - some functions used all over the place
 * This is a two-part package. Part 1 (this one) contains more
 * image-related functions and helper functions, and
 * part 2 contains more numerical algorithms and methods.
 */


/*		CRYSTAL IMAGE 0.9.0 BETA
		This program is copyrighted material.
		(C) 2002-2010 by Mark A. Haidekker, all rights reserved.
		
		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE:

		THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
		APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
		HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
		OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
		THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
		IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
		ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

		IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
		WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
		THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
		GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
		USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
		DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
		PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
		EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
		SUCH DAMAGES.

		For the full text of the license, please see the file COPYING
		in the project's main directory, or read it on the Web at
		http://www.gnu.org/copyleft/gpl.html

 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <fftw3.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>


#include "defs.h"
#include "functions.h"
#include "dlgbox.h"
#include "imgbuf.h"




/* We need a GSL random generator */

const gsl_rng_type *gsl_randgen_type;
gsl_rng *gsl_randgen;



/*****************************************************

	An important help for debugging. dp (debug-print)
	is equivalent to fprintf (stderr, ...), but
	only prints if the priority level pri of the message
	exceeds the currently selected debug level.
	If debuglevel == 0, no messages are printed.
*/



void dp (int pri, char *format,...)
{
va_list args;
char buf[1024];

	if (pri > debuglevel) return;
    va_start(args, format);
    vsnprintf(buf,1020,format,args);
    fprintf (stderr,"%s",buf);
}



/********************************************************

	Critical memory allocations lead to program exit

*********************************************************/


void fatal_memerr ()	/* Abort program when critical memory allocation failed */
{
	ShowMessageC ("Fatal error - no memory. Program exits","That sucks!");
	exit (1);
}


/********************************************************

	Small function to determine whether a string is empty 
   (null string or only blanks)

********************************************************/


char isempty (char* s)
{
int i;

	i=0;
	while ((i<strlen(s)) && ((s[i]==' ') || (s[i]=='\t') || (s[i]=='\n'))) i++;
	if (i<strlen(s)) return 0; else return 1;
}


/*************************************************************/

/* Initialize whatever we need for using GSL components
*/


void	init_gsl_fn ()
{

	/* Prepare a random generator */
	
	gsl_rng_env_setup();
	gsl_randgen_type = gsl_rng_default;
	gsl_randgen = gsl_rng_alloc (gsl_randgen_type);


	
	
}



/*************************************************************/


/* Function returns whether a slice is all-zero.
	Returns 1 if all components are zero or 0 if not */


char slice_is_zero (image_type* img, int slice)
{
char* p;
char b;
long i,l,e;

	l = img->xmax * img->ymax * DATALEN(img->imgtype);
	e = l*slice;
	b=0;
	p = img->data;
	
	for (i=e; i<e+l; i++) b |= p[i];
	
	if (b!=0) return 0; 
	
	dp (2, "Zero slice %d detected\n",slice);
	return 1;
}
	




void imgminmax (image_type* img, float* mmin, float* mmax)
{
float min,max;

	if (img->imgtype==B_FLOAT)
	{
		min = img->minval.fmin;
		max = img->maxval.fmax;
	}
	else
	{
		min = img->minval.imin;
		max = img->maxval.imax;
	}
	*mmin = min; *mmax = max;
}






/*********************************************+

	Color model conversions
	
***********************************************/


/* Convert a rgb pixel to hsv */

hsvtriplet rgb_to_hsv (rgbtriplet rgb)
{
hsvtriplet hsv;
int h,s,v,m;
	
	if ((rgb.red==0) && (rgb.green==0) && (rgb.blue==0))
	{
		hsv = (hsvtriplet){0,0,0};
	}
	else
	{
	
		if ((rgb.red>=rgb.green) && (rgb.red>=rgb.blue))
		{
			v = rgb.red; m = (rgb.green<rgb.blue? rgb.green : rgb.blue);
			h = (int) (60.0*(rgb.green-rgb.blue) / (float)(v-m));
		}
		else if ((rgb.green>rgb.red) && (rgb.green>=rgb.blue))
		{
			v = rgb.green; m = (rgb.red<rgb.blue? rgb.red : rgb.blue);
			h = (int) (120.0+60.0*(rgb.blue-rgb.red) / (float)(v-m));
		}
		else
		{
			v = rgb.blue; m = (rgb.red<rgb.green? rgb.red : rgb.green);
			h = (int) (240.0+60.0*(rgb.red-rgb.green) / (float)(v-m));
		}
		s = (int) (255.0 *(1.0 - (double) m / (double) v));
		hsv.hue= (h<0?  h+360 : h);
		hsv.sat=s & 0xff;
		hsv.val=v & 0xff;
	}
	return hsv;
}


rgbtriplet hsv_to_rgb (hsvtriplet hsv)
{
int sector;
float f, s;
int p,q,t;
rgbtriplet rgb;

	if( hsv.sat == 0 ) 	/* Sat==0 means achromatic (=grey) with r=b=g */
	{
		rgb.red = rgb.green = rgb.blue = hsv.val;
	}
	else
	{

		if (hsv.hue<0) hsv.hue+=360;
		sector = floor (hsv.hue / 60.0);	/* color sectors 0 through 5 */
		f = hsv.hue/60.0 - sector;			/* fractional part of h */
		s = (float)hsv.sat / 255.0;			/* Let sat go from 0 to 1 */
		p = (int)(0.5+hsv.val * ( 1 - s ));
		q = (int)(0.5+hsv.val * ( 1 - s * f ));
		t = (int)(0.5+hsv.val * ( 1 - s * ( 1 - f )));

		switch (sector)
		{
			case 0:
				rgb.red = hsv.val;
				rgb.green = t;
				rgb.blue = p;
				break;
			case 1:
				rgb.red = q;
				rgb.green = hsv.val;
				rgb.blue = p;
				break;
			case 2:
				rgb.red = p;
				rgb.green = hsv.val;
				rgb.blue = t;
				break;
			case 3:
				rgb.red = p;
				rgb.green = q;
				rgb.blue = hsv.val;
				break;
			case 4:
				rgb.red = t;
				rgb.green = p;
				rgb.blue = hsv.val;
				break;
			case 5:
			default:
				rgb.red = hsv.val;
				rgb.green = p;
				rgb.blue = q;
				break;
		}
	}
	return rgb;
}


rgbtriplet rgb_to_ycrcb (rgbtriplet rgb)
{
float y, cr, cb;
int i;
rgbtriplet result;

		y  = 0.299*rgb.red + 0.587*rgb.green + 0.114*rgb.blue;
		cr = -0.169*rgb.red - 0.331*rgb.green + 0.5*rgb.blue;
		cb = 0.5*rgb.red - 0.419*rgb.green - 0.081*rgb.blue;

		/* Pack back into uchar. Note that Cr and Cb are packed as signed short with offset */

		i = IROUND(y); if (i>255) i=255; else if (i<0) i=0;
		result.red=i;		/* Integer Y component */
		i = IROUND(127.0+cr); if (i>255) i=255; else if (i<0) i=0;
		result.green=i;		/* Integer Cr component */
		i = IROUND(127.0+cb); if (i>255) i=255; else if (i<0) i=0;
		result.blue=i;		/* Integer Cb component */
		return result;
}



/*************************************************************

	Karhunen-Loeve-Transform (Principal component analysis)

	The K-L transform acts on RGB images by interpreting
	each triplet as a point in three-dimensional RGB space.
	The K-L transform performs a coordinate system trans-
	formation so that one axis has the highest contrast
	(the principal component). The principal component
	is then returned as grayscale (luminance) value.

	NEW in 0.9.7: Can also handle 3-stacks of arbitrary type,
	and it returns all three principal components.

*************************************************************/


/* Compute covariance matrix in C */

void kl_comp_covar_mat (image_type* img, MFLOAT* C, int slice, int oorflg)
{
MFLOAT mr,mg,mb,sum;
MFLOAT c1, c2, c3;			/* R,G,B components in non-RGB types */
int i,j,x,y;
long l,z,cnt;
unsigned char* p;
rgbtriplet* prgb;
rgbtriplet rgb;


	p = img->data;

	/* Step 1: Compute the image mean for each pane */

	z = slice * img->xmax * img->ymax;

	if (img->imgtype == B_RGB)
	{
		prgb=img->data;
		mr=0.0; mg=0.0; mb=0.0; cnt=0;
		for (l=0; l<img->xmax * img->ymax; l++)
		{
			rgb = prgb[l+z];
			if ((!oorflg) || (rgb.red>0) || (rgb.green>0) || (rgb.blue>0))
			{
				mr += rgb.red; mg += rgb.green; mb += rgb.blue;
				cnt++;
			}
		}
	}
	else if ( (img->zmax %3) ==0)
	{
		mr=0.0; mg=0.0; mb=0.0; cnt=0;
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				c1 = readbuf_flt (img,x,y,slice);
				c2 = readbuf_flt (img,x,y,slice+1);
				c3 = readbuf_flt (img,x,y,slice+2);
				mr += c1; mg += c2; mb += c3;
				cnt++;
			}
	}
	else
	{
		ShowMessage ("Error: Incompatible stack size");
		return;
	}

	if (cnt>0)
	{
		mr/=cnt; mb/=cnt; mg/=cnt;
	}
	else
	{
		ShowMessage ("Cannot compute image mean - no data?");
		return;
	}


	/* Step 2: Compute the covariance matrix */
		
	cnt=0; sum=0.0;
	if (img->imgtype == B_RGB)
	{
		for (l=0; l<img->xmax * img->ymax; l++)
		{
			rgb = prgb[l+z];
			if ((!oorflg) || (rgb.red>0) || (rgb.green>0) || (rgb.blue>0))
			{
				C[0] += (rgb.red   - mr)*(rgb.red   - mr);	/* RR = 0,0 */
				C[1] += (rgb.red   - mr)*(rgb.green - mg);	/* RG = 0,1 */
				C[2] += (rgb.red   - mr)*(rgb.blue  - mb);	/* RB = 0,2 */
				C[4] += (rgb.green - mg)*(rgb.green - mg);	/* GG = 1,1 */
				C[5] += (rgb.green - mg)*(rgb.blue  - mg);	/* GB = 1,2 */
				C[8] += (rgb.blue  - mg)*(rgb.blue  - mg);	/* BB = 2,2 */
				cnt++;
			}
		}
	}
	else			/* Case of not a three-stack has already been trapped */
	{
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				c1 = readbuf_flt (img,x,y,slice);
				c2 = readbuf_flt (img,x,y,slice+1);
				c3 = readbuf_flt (img,x,y,slice+2);
				C[0] += (c1   - mr)*(c1  - mr);	/* RR = 0,0 */
				C[1] += (c1   - mr)*(c2  - mg);	/* RG = 0,1 */
				C[2] += (c1   - mr)*(c3  - mb);	/* RB = 0,2 */
				C[4] += (c2   - mg)*(c2  - mg);	/* GG = 1,1 */
				C[5] += (c2   - mg)*(c3  - mg);	/* GB = 1,2 */
				C[8] += (c3   - mg)*(c3  - mg);	/* BB = 2,2 */
				cnt++;
			}
	}

	C[3] = C[1];		/* RG = GR */
	C[7] = C[5];		/* GB = BG */
	C[6] = C[2];		/* BR = RB */
	for (i=0; i<9; i++) C[i] /= cnt;
	
	dp (2, "K-L Transform: This is the covariance matrix:\n");
	for (i=0; i<3; i++)
	{
		for (j=0; j<3; j++)
			dp (2, "%7.2f   ",C[3*i+j]);
		dp (2, "\n");
	}
	dp (2,"----------------------------------------\n");
}



/* Step 3: Compute the eigenvectors and the transposed and sorted
   eigenmatrix */

void kl_comp_transform_mat (MFLOAT* C)
{
MFLOAT D[3], E[3];
int i,j;

	householder_reduction (C,3,D,E);
	eigen_ql (C,3,D,E);
	
	dp (2, "K-L Transform: Eigenvectors ");
	for (i=0; i<3; i++) dp (2, "%f   ",D[i]);
	dp (2,"\n");
	dp (2, "K-L Transform: This is the transformation matrix:\n");
	for (i=0; i<3; i++)
	{
		for (j=0; j<3; j++)
			dp (2, "%7.2f   ",C[3*i+j]);
		dp (2, "\n");
	}
	dp (2,"----------------------------------------\n");

}


void karhunen_loeve_transform (image_type* img, int oorflg)
{
MFLOAT C[9];
image_type buf;
MFLOAT V[3];
int x,y,z,zinc;
long zidx;
rgbtriplet* p1;
float* p2;
rgbtriplet rgb;


	
	/* Allocate the output buffer (float grayscale) */

	if (img->imgtype == B_RGB)			/* Three stack slices per RGB slice */
	{
		if (allocate_image (&buf, B_FLOAT, img->xmax, img->ymax, 3*img->zmax) ==-1)
			fatal_memerr();
		zinc = 1;
	}
	else
	{
		if (allocate_image (&buf, B_FLOAT, img->xmax, img->ymax, img->zmax) ==-1)
			fatal_memerr();
		zinc = 3;
	}


	/* Transform slice by slice (TODO: is that a good idea?? Or should we
	   compute one transformation matrix for the entire stack?) */

	for (z=0; z<img->zmax; z+=zinc)
	{
		dp (1, "Karhunen-Loeve-Transform: Processing slice %d\n",z);

		kl_comp_covar_mat (img, C, z, oorflg);
		kl_comp_transform_mat (C);

		if (img->imgtype == B_RGB)
		{
			p1 = img->data;
			p2 = buf.data;
			zidx = img->xmax*img->ymax;
			
			/* Pixel-by-pixel conversion of RGB image into a stack */
			
			for (y=0; y<img->ymax; y++)
				for (x=0; x<img->xmax; x++)
				{
					rgb = p1[x+img->xmax*y+z*zidx];
					V[0] = (float)rgb.red;
					V[1] = (float)rgb.green;
					V[2] = (float)rgb.blue;
					matmult (C,V,3);
					p2[x+img->xmax*y+3*z*zidx	]		= V[0]; 	/* First principal component */
					p2[x+img->xmax*y+(3*z+1)*zidx] 	= V[1]; 	/* Second principal component */
					p2[x+img->xmax*y+(3*z+2)*zidx] 	= V[2]; 	/* Third principal component */
				}
		}
		else		/* Any non-RGB type */
		{
			p2 = buf.data;
			zidx = img->xmax*img->ymax;
			
			/* Pixel-by-pixel conversion of stack into a stack of same size */
			
			for (y=0; y<img->ymax; y++)
				for (x=0; x<img->xmax; x++)
				{
					V[0] = readbuf_flt (img,x,y,z);
					V[1] = readbuf_flt (img,x,y,z+1);
					V[2] = readbuf_flt (img,x,y,z+2);
					matmult (C,V,3);
					p2[x+img->xmax*y+z*zidx]		= V[0]; 	/* First principal component */
					p2[x+img->xmax*y+(z+1)*zidx] 	= V[1]; 	/* Second principal component */
					p2[x+img->xmax*y+(z+2)*zidx] 	= V[2]; 	/* Third principal component */
				}
		}

	}
	
	/* return the new b&w image */
	
	handback (img, &buf);

}









/***************************************************************

	Spline-based interpolation

 *	This section is based on the following three papers:
 *		[1]	M. Unser,
 *			"Splines: A Perfect Fit for Signal and Image Processing,"
 *			IEEE Signal Processing Magazine, vol. 16, no. 6, pp. 22-38,
 *			November 1999.
 *		[2]	M. Unser, A. Aldroubi and M. Eden,
 *			"B-Spline Signal Processing: Part I--Theory,"
 *			IEEE Transactions on Signal Processing, vol. 41, no. 2, pp. 821-832,
 *			February 1993.
 *		[3]	M. Unser, A. Aldroubi and M. Eden,
 *			"B-Spline Signal Processing: Part II--Efficient Design and Applications,"
 *			IEEE Transactions on Signal Processing, vol. 41, no. 2, pp. 834-848,
 *			February 1993.
 *
 *	Credits:  Philippe Thevenaz, EPFL/STI/IOA/BIG (Lausanne, CH)
 *
***************************************************************/

image_type splcoeff;


void compute_weights (double x, double y, int* xi, int* yi, double* xw, double* yw, int width, int height, int SplDeg)
{
int i,j,k;
int	Width2, Height2;
double	w, w2, w4, t, t0, t1;

	Width2 = 2 * width - 2;
	Height2 = 2 * height - 2;

	/* compute the interpolation indices */	
	
	if (SplDeg & 1) 	/* Degree 3 or 5 */
	{
		i = (int)floor(x) - SplDeg / 2;
		j = (int)floor(y) - SplDeg / 2;
		for (k = 0; k <= SplDeg; k++) 
		{
			xi[k] = i++;
			yi[k] = j++;
		}
	}
	else 	/* degree 2 or 4 */
	{
		i = (int)floor(x + 0.5) - SplDeg / 2;
		j = (int)floor(y + 0.5) - SplDeg / 2;
		for (k = 0; k <= SplDeg; k++) 
		{
			xi[k] = i++;
			yi[k] = j++;
		}
	}

	/* compute the interpolation weights */	
	
	switch (SplDeg) 
	{
		case 2:
			/* x */
			w = x - (double)xi[1];
			xw[1] = 3.0 / 4.0 - w * w;
			xw[2] = (1.0 / 2.0) * (w - xw[1] + 1.0);
			xw[0] = 1.0 - xw[1] - xw[2];
			/* y */
			w = y - (double)yi[1];
			yw[1] = 3.0 / 4.0 - w * w;
			yw[2] = (1.0 / 2.0) * (w - yw[1] + 1.0);
			yw[0] = 1.0 - yw[1] - yw[2];
			break;
		case 3:
			/* x */
			w = x - (double)xi[1];
			xw[3] = (1.0 / 6.0) * w * w * w;
			xw[0] = (1.0 / 6.0) + (1.0 / 2.0) * w * (w - 1.0) - xw[3];
			xw[2] = w + xw[0] - 2.0 * xw[3];
			xw[1] = 1.0 - xw[0] - xw[2] - xw[3];
			/* y */
			w = y - (double)yi[1];
			yw[3] = (1.0 / 6.0) * w * w * w;
			yw[0] = (1.0 / 6.0) + (1.0 / 2.0) * w * (w - 1.0) - yw[3];
			yw[2] = w + yw[0] - 2.0 * yw[3];
			yw[1] = 1.0 - yw[0] - yw[2] - yw[3];
			break;
		case 4:
			/* x */
			w = x - (double)xi[2];
			w2 = w * w;
			t = (1.0 / 6.0) * w2;
			xw[0] = 1.0 / 2.0 - w;
			xw[0] *= xw[0];
			xw[0] *= (1.0 / 24.0) * xw[0];
			t0 = w * (t - 11.0 / 24.0);
			t1 = 19.0 / 96.0 + w2 * (1.0 / 4.0 - t);
			xw[1] = t1 + t0;
			xw[3] = t1 - t0;
			xw[4] = xw[0] + t0 + (1.0 / 2.0) * w;
			xw[2] = 1.0 - xw[0] - xw[1] - xw[3] - xw[4];
			/* y */
			w = y - (double)yi[2];
			w2 = w * w;
			t = (1.0 / 6.0) * w2;
			yw[0] = 1.0 / 2.0 - w;
			yw[0] *= yw[0];
			yw[0] *= (1.0 / 24.0) * yw[0];
			t0 = w * (t - 11.0 / 24.0);
			t1 = 19.0 / 96.0 + w2 * (1.0 / 4.0 - t);
			yw[1] = t1 + t0;
			yw[3] = t1 - t0;
			yw[4] = yw[0] + t0 + (1.0 / 2.0) * w;
			yw[2] = 1.0 - yw[0] - yw[1] - yw[3] - yw[4];
			break;
		case 5:
			/* x */
			w = x - (double)xi[2];
			w2 = w * w;
			xw[5] = (1.0 / 120.0) * w * w2 * w2;
			w2 -= w;
			w4 = w2 * w2;
			w -= 1.0 / 2.0;
			t = w2 * (w2 - 3.0);
			xw[0] = (1.0 / 24.0) * (1.0 / 5.0 + w2 + w4) - xw[5];
			t0 = (1.0 / 24.0) * (w2 * (w2 - 5.0) + 46.0 / 5.0);
			t1 = (-1.0 / 12.0) * w * (t + 4.0);
			xw[2] = t0 + t1;
			xw[3] = t0 - t1;
			t0 = (1.0 / 16.0) * (9.0 / 5.0 - t);
			t1 = (1.0 / 24.0) * w * (w4 - w2 - 5.0);
			xw[1] = t0 + t1;
			xw[4] = t0 - t1;
			/* y */
			w = y - (double)yi[2];
			w2 = w * w;
			yw[5] = (1.0 / 120.0) * w * w2 * w2;
			w2 -= w;
			w4 = w2 * w2;
			w -= 1.0 / 2.0;
			t = w2 * (w2 - 3.0);
			yw[0] = (1.0 / 24.0) * (1.0 / 5.0 + w2 + w4) - yw[5];
			t0 = (1.0 / 24.0) * (w2 * (w2 - 5.0) + 46.0 / 5.0);
			t1 = (-1.0 / 12.0) * w * (t + 4.0);
			yw[2] = t0 + t1;
			yw[3] = t0 - t1;
			t0 = (1.0 / 16.0) * (9.0 / 5.0 - t);
			t1 = (1.0 / 24.0) * w * (w4 - w2 - 5.0);
			yw[1] = t0 + t1;
			yw[4] = t0 - t1;
			break;
		default:
			dp (0,"Invalid spline degree\n");
	}

	/* apply the mirror boundary conditions */	
	
	for (k = 0; k <= SplDeg; k++) 
	{
		xi[k] = (width == 1) ? (0) : ((xi[k] < 0) ?
			(-xi[k] - Width2 * ((-xi[k]) / Width2))
			: (xi[k] - Width2 * (xi[k] / Width2)));

		if (width <= xi[k]) 
		{
			xi[k] = Width2 - xi[k];
		}

		yi[k] = (height == 1) ? (0) : ((yi[k] < 0) ?
			(-yi[k] - Height2 * ((-yi[k]) / Height2))
			: (yi[k] - Height2 * (yi[k] / Height2)));

		if (height <= yi[k]) 
		{
			yi[k] = Height2 - yi[k];
		}
	}


}


/* Initialize the causal coefficients. Pass the sample values in c
   and the number of samples in ns. The actual pole is in z and the 
   admissible relative error in epsilon.
*/


double	init_causal (double* c, int ns, double z, double epsilon)
{
double	Sum, zn, z2n, iz;
long	n, Horizon;

	/* this initialization corresponds to mirror boundaries */
	Horizon = ns;
	if (epsilon > 0.0) 
	{
		Horizon = (long)ceil(log(epsilon) / log(fabs(z)));
	}
	
	if (Horizon < ns) 	/* accelerated loop */
	{
		zn = z;
		Sum = c[0];
		for (n = 1; n < Horizon; n++) 
		{
			Sum += zn * c[n];
			zn *= z;
		}
		return Sum;
	}
	else	/* full loop */ 
	{
		zn = z;
		iz = 1.0 / z;
		z2n = pow(z, (double)(ns - 1));
		Sum = c[0] + z2n * c[ns - 1];
		z2n *= z2n * iz;
		for (n = 1L; n <= ns - 2; n++) 
		{
			Sum += (zn + z2n) * c[n];
			zn *= z;
			z2n *= iz;
		}
		return Sum / (1.0 - zn * zn);
	}
}


/* Get initial anticausal coefficient. Parameters as above */

double	init_anticausal (double* c, int ns, double z)
{
	return ((z / (z * z - 1.0)) * (z * c[ns - 2] + c[ns - 1]));
}



/* Compute the interpolation coefficients. Input the sample values
   in c[]. C[] contains the coefficients on exit. Specify the number of
   samples (and coefficients) in ns, the poles in z[] and the 
   number of poles in np and the
   admissible relative error in epsilon.
*/


void get_interpol_coeff (double* c, int ns, double* z, int np, double epsilon)
{

double	Lambda = 1.0;
int	n, k;

	
	if (ns == 1) return; /* special case required by mirror boundaries */

	for (k = 0; k < np; k++) 	/* compute the overall gain */
	{
		Lambda = Lambda * (1.0 - z[k]) * (1.0 - 1.0 / z[k]);
	}

	for (n = 0; n < ns; n++) /* apply the gain */
	{
		c[n] *= Lambda;
	}
	
	for (k = 0L; k < np; k++) /* loop over all poles */
	{
		c[0] = init_causal (c, ns, z[k], epsilon);	/* causal initialization */
		
		for (n = 1; n < ns; n++) 	/* causal recursion */
		{
			c[n] += z[k] * c[n - 1];
		}
		
		c[ns - 1] = init_anticausal (c, ns, z[k]); 	/* anticausal initialization */
		
		for (n = ns - 2; n >= 0; n--) /* anticausal recursion */
		{
			c[n] = z[k] * (c[n + 1] - c[n]);
		}
	}
}


/* Compute the coefficients for one image plane (2D only). This is
   and in-place operation on img, where img must be pre-allocated
   and be of type B_FLOAT. The original image values must be copied
   into img->data. SplDeg is the spline degree.
*/


void compute_coeff (image_type* img, int SplDeg) 
{
double	*Line;
float* p;
double	Pole[2];
int np;
int x,y,idx;

	g_assert (img->imgtype == B_FLOAT);
	p = img->data;

	/* recover the poles from a lookup table. If an invalid spline degree
	   is given, pretend we wanted degree 2 */
	switch (SplDeg) 
	{
		case 2:
			np = 1; Pole[0] = sqrt(8.0) - 3.0;
			break;
		case 3:
			np = 1; Pole[0] = sqrt(3.0) - 2.0;
			break;
		case 4:
			np = 2;
			Pole[0] = sqrt(664.0 - sqrt(438976.0)) + sqrt(304.0) - 19.0;
			Pole[1] = sqrt(664.0 + sqrt(438976.0)) - sqrt(304.0) - 19.0;
			break;
		case 5:
			np = 2;
			Pole[0] = sqrt(135.0 / 2.0 - sqrt(17745.0 / 4.0)) + sqrt(105.0 / 4.0)
				- 13.0 / 2.0;
			Pole[1] = sqrt(135.0 / 2.0 + sqrt(17745.0 / 4.0)) - sqrt(105.0 / 4.0)
				- 13.0 / 2.0;
			break;
		default:
			np = 1; Pole[0] = sqrt(8.0) - 3.0;
			dp (0,  "Invalid spline degree selected\n");
	}

	/* Sample computation is an in-place separable process, so perform
	the operation twice; once along X then along Y. The tolerance epsilon
	is defined in math.h and is somewhere around 2.22e-16 */

	Line = calloc(img->xmax, sizeof(double));
	if (!Line) fatal_memerr();

	for (y = 0; y < img->ymax; y++) 
	{
		idx = img->xmax * y;
		for (x=0; x<img->xmax; x++) Line[x] = p[x+idx];
		get_interpol_coeff (Line, img->xmax, Pole, np, DBL_EPSILON);
		for (x=0; x<img->xmax; x++) p[x+idx] = Line[x];
	}
	free(Line);

	Line = calloc(img->ymax, sizeof(double));
	if (!Line) fatal_memerr();

	for (x = 0; x < img->xmax; x++) 
	{
		idx=0;
		for (y=0; y<img->ymax; y++) { Line[y] = p[x+idx]; idx+=img->xmax; }
		get_interpol_coeff (Line, img->ymax, Pole, np, DBL_EPSILON);
		idx=0;
		for (y=0; y<img->ymax; y++) { p[x+idx] = Line[y]; idx+=img->xmax; }
	}
	free(Line);
}


/* Here is the actual 2D interpolation function. It requires the coefficients
   to be pre-computed and cached in the "image" coeff. It does *NOT* require
   the original image data as this was already used to compute the coefficients.*/


double spline_interpol_core (image_type* coeff, float x, float y, int SplDeg)
{
float	*p;	
double	xWeight[6], yWeight[6];
double	interpolated;
double	w;
int		xIndex[6], yIndex[6];
int	i, j, idx;


	g_assert (coeff->imgtype == B_FLOAT);
	p = coeff->data;

	/* Indices and weights need to be computed for every x,y coordinate */

	compute_weights (x,y, xIndex, yIndex, xWeight, yWeight, coeff->xmax, coeff->ymax, SplDeg);

	/* perform interpolation */	
	
	interpolated = 0.0;
	for (j = 0; j <= SplDeg; j++) 
	{
		idx = yIndex[j]* coeff->xmax;
		w = 0.0;
		for (i = 0; i <= SplDeg; i++) 
		{
			w += xWeight[i] * p[xIndex[i] + idx];
		}
		interpolated += yWeight[j] * w;
	}

	return(interpolated);
}


/* The spline interpolation routine with coefficient caching mechanism */
/* TODO: Can't handle RGB */

double spline_interpol (image_type* img, float xf, float yf, int z, int SplDeg)
{
int x,y;
float* p;

	if (!spl_cache_invalidate)		/* Check if we need to invalidate */
	{
		if (spl_cache_z != z) spl_cache_invalidate=1;
		if (spl_cache_deg != SplDeg) spl_cache_invalidate=1;
		if (spl_cache_uid != img->uid) spl_cache_invalidate=1;
		
		if (spl_cache_invalidate)
		{
		 	freebuf (&splcoeff);
			dp (1, "Spline interpolation: Cache invalidated\n");
		}
	}

	if (spl_cache_invalidate)		/* Need to recompute the coefficients */
	{
		
		dp (1, "Spline interpolation: Recomputing coefficients\n");
		if (allocate_image (&splcoeff, B_FLOAT, img->xmax, img->ymax, 1)==-1)
			fatal_memerr();
		p = splcoeff.data;
		
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
				p[x + img->xmax*y] = readbuf_flt (img, x,y,z);
		
		compute_coeff (&splcoeff, SplDeg);
		
		spl_cache_invalidate=0;
		spl_cache_z = z;
		spl_cache_deg=SplDeg;
		spl_cache_uid = img->uid;
	}
	
	return spline_interpol_core (&splcoeff, xf,yf, SplDeg);

}







/******************************************************************/




