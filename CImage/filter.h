


#ifndef __IMAGER_FILTER_H__
#define __IMAGER_FILTER_H__

#include "defs.h"


void sobel_core (float* in, int xmax, int ymax);
void convolve (float* in, int xmax, int ymax, float* mtrx, int xm, int ym);

void sobel (image_type* img);
void sobel_rgb (image_type* img);
void sobel_main ();
void compass_edge (image_type* img);
void compass_edge_rgb (image_type* img);

void gaussian_general (image_type* img, int n, int LoG);
void frei_chen_filter (image_type* img, int linemode);
void frei_n_chen_main (int linemode);
void kuwahara_2D (image_type* img, int nghsize);

void local_variance_2D (image_type* img, int nghsize);
int locmax_transform_2d (image_type* img, int env);

void convolve_filter (image_type* img, int filttype);
void convolve_filter_main ();


#endif
