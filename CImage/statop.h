
#ifndef __IMAGER_STATOP_H__
#define __IMAGER_STATOP_H__


double find_quantile_standalone (image_type* img, int z, int numbins, float fQuantile, int oorflg);
double histogram_standalone (image_type* img, int z, int numbins, int what, int oorflg);


void statop_redraw_all (char sliceflg);
void statop_main ();



#endif
