

/*
 *  Filter.c	Any convolution and spatial-domain filter operations
 */


/*		CRYSTAL IMAGE 0.9.0 BETA
		This program is copyrighted material.
		(C) 2002-2010 by Mark A. Haidekker, all rights reserved.
		
		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE:

		THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
		APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
		HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
		OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
		THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
		IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
		ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

		IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
		WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
		THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
		GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
		USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
		DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
		PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
		EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
		SUCH DAMAGES.

		For the full text of the license, please see the file COPYING
		in the project's main directory, or read it on the Web at
		http://www.gnu.org/copyleft/gpl.html

 */



#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "application.h"
#include "filter.h"
#include "fractals.h"
#include "functions.h"
#include "functions2.h"
#include "gtkhelpers.h"
#include "imgbuf.h"



/*********************************************************

	Convolve: Do a convolution on a float matrix
	
**********************************************************/


void convolve (float* in, int xmax, int ymax, float* mtrx, int xm, int ym)
{

int x,y,i,j,x1,y1,xa,ya;
float* p;
long idx1,idx2;
float a,b;

	g_assert ( (xm & 1) > 0);	/* must be an odd number */
	g_assert ( (ym & 1) > 0);	/* must be an odd number */
	xa = xm/2;
	ya = ym/2;

	p = (float*) calloc (xmax*ymax, sizeof (float));
	if (!p) fatal_memerr();
	
	
	for (y=0; y<ymax; y++)
	{
		idx1 = y*xmax;
		for (x=0; x<xmax; x++)
		{
			a=0; b=0;
			for (j=-ya; j<=ya; j++)
			{
				y1=y+j-1; if (y1<0) y1=0; else if (y1>=ymax) y1=ymax-1;
				idx2=xmax*y1;
				for (i=-xa; i<=xa; i++)
				{
					x1=x+i-1; if (x1<0) x1=0; else if (x1>=xmax) x1=xmax-1;
					a += in[x1+idx2] * mtrx[ (i+xa) + xm*(j+ya) ];
					b += mtrx[ (i+xa) + xm*(j+ya) ]; /* todo: b is a constant */
				}
			}
			p[idx1++]= a/b;
			dp (3, "%f   %f   %f\n",a,b,sqrt (SQR(a)+SQR(b) ));
		}
	}
	
	
	memcpy (in, p, xmax*ymax*sizeof(float));
	free (p);
}




/*******************************************************

	SOBEL: An omnidirectional edge detector
	
********************************************************/

/* The sobel core acts on a raw matrix, not an actual image structure */

void sobel_core (float* in, int xmax, int ymax)
{

int x,y,i,j,x1,y1;
float sobelfac[3][3] = { {-1.0, -2.0, -1.0} , {0.0, 0.0, 0.0}, {1.0, 2.0, 1.0} };
float* p; float a,b;
long idx1,idx2;

	p = (float*) calloc (xmax*ymax, sizeof (float));
	if (!p) fatal_memerr();
	
	
	for (y=0; y<ymax; y++)
	{
		idx1 = y*xmax;
		for (x=0; x<xmax; x++)
		{
			a=0; b=0;
			for (j=0; j<=2; j++)
			{
				y1=y+j-1; if (y1<0) y1=0; else if (y1>=ymax) y1=ymax-1;
				idx2=xmax*y1;
				for (i=0; i<=2; i++)
				{
					x1=x+i-1; if (x1<0) x1=0; else if (x1>=xmax) x1=xmax-1;
					a += in[x1+idx2] * sobelfac[i][j];
					b += in[x1+idx2] * sobelfac[j][i];
				}
			}
			p[idx1++]= sqrt (SQR(a)+SQR(b) )*0.125;				/* Use an 1/8 normalzation factor to obtain true edge magnitude */
			dp (3, "%f   %f   %f\n",a,b,sqrt (SQR(a)+SQR(b) ));
		}
	}
	
	
	memcpy (in, p, xmax*ymax*sizeof(float));
	free (p);
}



/* SOBEL sobelizes (edge detects) an entire stack. It returns
   a B_FLOAT image. Should not be used for RGB.
   TODO: May be enhanced by using isqrt on integer types */


void sobel (image_type* img)
{
image_type sobelimg;
int x,y,z,i,j;
float sobelfac[3][3] = { {-1.0, -2.0, -1.0} , {0.0, 0.0, 0.0}, {1.0, 2.0, 1.0} };
float* p; float a,b;
long idx1;

	if (allocate_image (&sobelimg, B_FLOAT, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr();
	p = sobelimg.data;
	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		for (y=0; y<img->ymax; y++)
		{
			for (x=0; x<img->xmax; x++)
			{
				a=0; b=0;
				for (j=0; j<=2; j++)
				{
					for (i=0; i<=2; i++)
					{
						a += readbuf_flt(img,x+i-1,y+j-1,z) * sobelfac[i][j];
						b += readbuf_flt(img,x+i-1,y+j-1,z) * sobelfac[j][i];
					}
				}
				p[idx1++]= sqrt (SQR(a)+SQR(b) )*0.125;
				/* p[idx1++]= atan2 (b,a);				 Use this for edge direction */
				dp (3, "%f   %f   %f\n",a,b,sqrt (SQR(a)+SQR(b) ));
			}
		}
	
	}
	handback (img, &sobelimg);
}


void sobel_rgb (image_type* img)
{
image_type sobelimg;
int x,y,z,i,j,x1,y1,n;
rgbtriplet *p, *src;
int sobelfac[3][3] = { {-1, -2, -1} , {0, 0, 0}, {1, 2, 1} };
int hr,hg,hb,vr,vg,vb;
long idx1,idx2;

	g_assert (img->imgtype==B_RGB);	/* Never call this func with non-RGB image */

	if (allocate_image (&sobelimg, B_RGB, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr();
	p = sobelimg.data;
	src=img->data;
	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		for (y=0; y<img->ymax; y++)
		{
			for (x=0; x<img->xmax; x++)
			{
				hr=hg=hb=vr=vg=vb=0;
				for (j=0; j<=2; j++)
				{
					y1=y+j-1; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;
					idx2=img->xmax*y1;
					for (i=0; i<=2; i++)
					{
						x1=x+i-1; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;

						hr += src[x1+idx2].red * sobelfac[i][j];
						vr += src[x1+idx2].red * sobelfac[j][i];
						hg += src[x1+idx2].green * sobelfac[i][j];
						vg += src[x1+idx2].green * sobelfac[j][i];
						hb += src[x1+idx2].blue * sobelfac[i][j];
						vb += src[x1+idx2].blue * sobelfac[j][i];
					}
				}

				/* TODO: Isn't this a bit crude? I just force it back into range without rescaling. Bah. 
					See Compass operator - there is' done the right way */

				n = (isqrt (SQR(hr)+SQR(vr)))>>3; if (n<0) n=0; else if (n>255) n=255;
				p[idx1].red = n;
				n = (isqrt (SQR(hg)+SQR(vg)))>>3; if (n<0) n=0; else if (n>255) n=255;
				p[idx1].green = n;
				n = (isqrt (SQR(hb)+SQR(vb)))>>3; if (n<0) n=0; else if (n>255) n=255;
				p[idx1].blue = n;
				idx1++;
			}
		}
	
	}
	handback (img, &sobelimg);

}


/* and the menu function for Sobel ... */

void sobel_main ()
{
	if (mainimg.imgtype == B_RGB)
		sobel_rgb (&mainimg);
	else
		sobel (&mainimg);
}



/* Compass operator: Another omnidirectinal edge detector.
   It uses 8 (actually, three) convolution matrices, Sobel-style,
   and takes the maximum of the convolution operations as the edge value.
	A note on orientation. the first magnitude, b[0], represents vertical edges,
	b[1] a 45 degree edge CW from the X axis, b[2] a horizontal edge, and b[4]
	a 45 degree edge CCW from x axis:
	b[0] =  |
	b[1] = 	\
	b[2] =  -
	b[3] =  /

   */


void compass_edge (image_type* img)
{
image_type sobelimg;
int x,y,z,i,j,m,ornt;
float compsfac_a[3][3]= 	{	{-1.0, -2.0, -1.0} , {0.0, 0.0, 0.0}, {1.0, 2.0, 1.0}	};	/* Horizontal & vertical edge */
float compsfac_b[3][3]=	{	{ 0.0,-1.0,-2.0} , { 1.0, 0.0,-1.0}, {2.0, 1.0, 0.0}	};	/* 45 degrees */
float compsfac_c[3][3]=	{	{ -2.0,-1.0,0.0} , { -1.0, 0.0,1.0}, {0.0, 1.0, 2.0}	};	/* -45 degrees */

float *p;
float b[4];
float buf;
float max;
long idx1;


	if (allocate_image (&sobelimg, B_FLOAT, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr();
	p = sobelimg.data;
	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		for (y=0; y<img->ymax; y++)
		{
			for (x=0; x<img->xmax; x++)
			{
				for (m=0; m<4; m++) b[m]=0;
				for (j=0; j<=2; j++)
				{
					for (i=0; i<=2; i++)
					{
						buf = readbuf_flt(img,x+i-1,y+j-1,z);

						b[0] += buf * compsfac_a[i][j];
						b[1] += buf * compsfac_b[i][j];
						b[2] += buf * compsfac_a[j][i];		/* Transposing works here... */
						b[3] += buf * compsfac_c[i][j];		/* but not here */
					}
				}

				ornt=0; max=0;
				for (m=0; m<4; m++)
				{
					b[m] = fabs (b[m]);			/* The fabs takes care of the other 4 directions */
					if (max < b[m]) { max=b[m]; ornt=m*45; }
				}

				p[idx1++]= max;
			}
		}
	
	}
	make_minmax (&sobelimg);
	handback (img, &sobelimg);
}


void compass_edge_rgb (image_type* img)
{
image_type sobelimg;
int x,y,x1,y1,z,i,j,m;
float compsfac_a[3][3]= 	{	{-1.0, -2.0, -1.0} , {0.0, 0.0, 0.0}, {1.0, 2.0, 1.0}	};	/* Horizontal & vertical edge */
float compsfac_b[3][3]=	{	{ 0.0,-1.0,-2.0} , { 1.0, 0.0,-1.0}, {+2.0, 1.0, 0.0}	};	/* 45 degrees */
float compsfac_c[3][3]=	{	{ -2.0,-1.0,0.0} , { -1.0, 0.0,1.0}, {0.0, 1.0, 2.0}	};	/* -45 degrees */

rgbtriplet *p, *src; 
float rc[5], bc[5], gc[5];
float *rr, *gg, *bb;
rgbtriplet buf;
float max[3];
long idx1;


	g_assert (img->imgtype==B_RGB);	/* Never call this func with non-RGB image */

	if (allocate_image (&sobelimg, B_RGB, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr();
	p = sobelimg.data;
	src = img->data;

	/* Allocate space for the intermediate (float) result, to be scaled later */

	rr = calloc (img->xmax * img->ymax, sizeof (float));
	gg = calloc (img->xmax * img->ymax, sizeof (float));
	bb = calloc (img->xmax * img->ymax, sizeof (float));
	if (!rr || !bb || !gg) fatal_memerr();

	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		max[0]=0; max[1]=0; max[2]=0;

		for (y=0; y<img->ymax; y++)
		{
			for (x=0; x<img->xmax; x++)
			{

				for (m=0; m<5; m++) { rc[m]=0; bc[m]=0; gc[m]=0; }

				for (j=0; j<=2; j++)
				{
					y1=y+j-1; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;

					for (i=0; i<=2; i++)
					{
						x1=x+i-1; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;

						buf = src[x1 + img->xmax * y1 + idx1];

						rc[0] += buf.red * compsfac_a[i][j];	gc[0] += buf.green * compsfac_a[i][j];	bc[0] += buf.blue * compsfac_a[i][j];
						rc[1] += buf.red * compsfac_b[i][j];	gc[1] += buf.green * compsfac_b[i][j];	bc[1] += buf.blue * compsfac_b[i][j];
						rc[2] += buf.red * compsfac_a[j][i];	gc[2] += buf.green * compsfac_a[j][i];	bc[2] += buf.blue * compsfac_a[j][i];
						rc[3] += buf.red * compsfac_c[i][j];	gc[3] += buf.green * compsfac_c[i][j];	bc[3] += buf.blue * compsfac_c[i][j];
					}
				}

				rc[4] = 0; gc[4] = 0; bc[4] = 0;
				for (m=0; m<4; m++)
				{
					rc[m] = fabs (rc[m]);			/* The fabs takes care of the other 4 directions */
					gc[m] = fabs (gc[m]);
					bc[m] = fabs (bc[m]);
					if (rc[4] < rc[m]) rc[4]=rc[m];
					if (gc[4] < gc[m]) gc[4]=gc[m];
					if (bc[4] < bc[m]) bc[4]=bc[m];
				}

				/* Store the dominant edge value and find maximum for each color */

				rr[x + y*img->xmax] = rc[4]; if (max[0] < rc[4]) max[0] = rc[4];
				gg[x + y*img->xmax] = gc[4]; if (max[1] < gc[4]) max[1] = gc[4];
				bb[x + y*img->xmax] = bc[4]; if (max[2] < bc[4]) max[2] = bc[4];

			}
		}

		/* Done for this slice. Rescale to fit into 0..255. */

		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				i = (int) (256.0*rr[x + y*img->xmax]/max[0]); if (i<0) i=0; else if (i>255) i=255;
				p[x + y*img->xmax+idx1].red = i;
				i = (int) (256.0*gg[x + y*img->xmax]/max[1]); if (i<0) i=0; else if (i>255) i=255;
				p[x + y*img->xmax+idx1].green = i;
				i = (int) (256.0*bb[x + y*img->xmax]/max[2]); if (i<0) i=0; else if (i>255) i=255;
				p[x + y*img->xmax+idx1].blue = i;
			}
	
	}
	free (rr); free (gg); free (bb);

	make_minmax (&sobelimg);
	handback (img, &sobelimg);
}



void compass_main ()
{
	if (mainimg.imgtype == B_RGB)
		compass_edge_rgb (&mainimg);
	else
		compass_edge (&mainimg);

}


/**************************************************************************/
/* RANGE operator for a m x m window.
		The range operator looks for the max and min values in a 
		m by m window and returns ther range, i.e., max-min.
		Here, m is 2n+1.
*/



void rangeop (image_type* img, int n)
{
image_type dest;
int x,y,z,i,j,rgb;
float min,max, buf;
int imin, imax, ibuf;
float *fp1, *fp2;
short *sp1, *sp2;
unsigned char *cp1, *cp2;
long idx1;

	if (allocate_image (&dest, img->imgtype, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();

	cp1 = img->data; fp1 = img->data; sp1 = img->data;
	cp2 = dest.data; fp2 = dest.data; sp2 = dest.data;


	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;

		switch (img->imgtype)
		{
			case B_CHAR:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						ibuf = imin = imax = cp1[x+y*img->xmax+idx1];
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
								if (((x+i)>=0) && ((y+j)>=0) && ((x+i)<img->xmax) && ((y+j)<img->ymax))
								{
									ibuf = cp1[ (x+i) + img->xmax*(y+j) +idx1 ];
									imin=MIN(imin,ibuf); imax=MAX(imax,ibuf);
								}
						cp2[x + img->xmax*y] = imax-imin;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						cp1[x + img->xmax*y+idx1] = cp2[x + img->xmax*y];
				break;
			}

			case B_SHORT:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						ibuf = imin = imax = sp1[x+y*img->xmax+idx1];
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
								if (((x+i)>=0) && ((y+j)>=0) && ((x+i)<img->xmax) && ((y+j)<img->ymax))
								{
									ibuf = sp1[ (x+i) + img->xmax*(y+j) +idx1 ];
									imin=MIN(imin,ibuf); imax=MAX(imax,ibuf);
								}
						sp2[x + img->xmax*y] = imax-imin;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						sp1[x + img->xmax*y+idx1] = sp2[x + img->xmax*y];
				break;
			}

			case B_FLOAT:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						buf = min = max = fp1[x+y*img->xmax+idx1];
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
								if (((x+i)>=0) && ((y+j)>=0) && ((x+i)<img->xmax) && ((y+j)<img->ymax))
								{
									buf = fp1[ (x+i) + img->xmax*(y+j) +idx1 ];
									min=MIN(min,buf); max=MAX(max,buf);
								}
						fp2[x + img->xmax*y] = max-min;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						fp1[x + img->xmax*y +idx1] = fp2[x + img->xmax*y];
				break;
			}

			case B_RGB:
			{
				for (rgb=0; rgb<3; rgb++)
				{
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
						{
							ibuf = imin = imax = cp1[rgb+3*(x+y*img->xmax+idx1)];
							for (j=-n; j<=n; j++)
								for (i=-n; i<=n; i++)
									if (((x+i)>=0) && ((y+j)>=0) && ((x+i)<img->xmax) && ((y+j)<img->ymax))
									{
										ibuf = cp1[rgb+3*( (x+i) + img->xmax*(y+j) +idx1 )];
										imin=MIN(imin,ibuf); imax=MAX(imax,ibuf);
									}
							cp2[rgb+3*(x + img->xmax*y)] = imax-imin;
						}

					/* Copy back this slice */
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
							cp1[rgb+3*(x + img->xmax*y+idx1)] = cp2[rgb+3*(x + img->xmax*y)];
				}
				break;
			}

		}
	
	}
	
	freebuf (&dest);
	make_minmax (img);
	img->uid = next_uid++;
}




/**************************************************************************/

/* General 3x3 convolution filter. 
   Performs operation slice by slice (2D!) and stores back in img.
   Boundary: The boundary rows/columns remain unaltered.
   The filttype parameter determines the filter function,
   0	No operation
   1	Smooth (Gaussian)
   2	Sharpen (iweak)
   3	Sharpen more (istrong, based on Laplacian)
   4	Laplacian (ilaplace)
*/


/* TODO: This thing can only do 3x3 and should do at least
   3x3, 5x5, 7x7, 9x9, 11x11; maybe even completely general nxn */

/* TODO: We might dramatically speed this up by using separability property */

void convolve_filter (image_type* img, int filttype)
{
image_type dest;
int x,y,z,i,j,c1,w,rgb;
float c2;
int ineutral[3][3] = { {0, 0, 0} , {0, 1, 0}, {0, 0, 0} };
int istrong[3][3] = { {-1, -1, -1} , {-1, 9, -1}, {-1, -1, -1} };
int iweak[3][3] = { {-1, -1, -1} , {-1, 12, -1}, {-1, -1, -1} };
int ilaplace[3][3] = { {-1, -1, -1} , {-1, 8, -1}, {-1, -1, -1} };
int ismooth[3][3] = { {1, 2, 1} , {2, 4, 2}, {1, 2, 1} };
float ffilt[3][3];
int ifilt[3][3];
float *fp1, *fp2;
short *sp1, *sp2;
unsigned char *cp1, *cp2;
long idx1;

	if (allocate_image (&dest, img->imgtype, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();

	cp1 = img->data; fp1 = img->data; sp1 = img->data;
	cp2 = dest.data; fp2 = dest.data; sp2 = dest.data;

	/* Copy the appropriate filter function */
	
	w=0;
	switch (filttype)
	{
		case 0:
			dp (1, "Convolution filter: Neutral (no op)\n");
			for (i=0; i<3; i++) 
				for (j=0; j<3; j++) 
				{
					ffilt[i][j]=(float)ineutral[i][j];
					ifilt[i][j]=ineutral[i][j];
					w += ifilt[i][j];
				}
			break;
		case 1:
			dp (1, "Convolution filter: Smoothing (Gaussian)\n");
			for (i=0; i<3; i++) 
				for (j=0; j<3; j++) 
				{
					ffilt[i][j]=(float)ismooth[i][j];
					ifilt[i][j]=ismooth[i][j];
					w += ifilt[i][j];
				}
			break;
		case 2:
			dp (1, "Convolution filter: Weak sharpening\n");
			for (i=0; i<3; i++) 
				for (j=0; j<3; j++) 
				{
					ffilt[i][j]=(float)iweak[i][j];
					ifilt[i][j]=iweak[i][j];
					w += ifilt[i][j];
				}
			break;
		case 3:
			dp (1, "Convolution filter: Sharpen More\n");
			for (i=0; i<3; i++) 
				for (j=0; j<3; j++) 
				{
					ffilt[i][j]=(float)istrong[i][j];
					ifilt[i][j]=istrong[i][j];
					w += ifilt[i][j];
				}
			break;
		case 4:
			dp (1, "Convolution filter: Laplacian Operator\n");
			for (i=0; i<3; i++) 
				for (j=0; j<3; j++) 
				{
					ffilt[i][j]=(float)ilaplace[i][j];
					ifilt[i][j]=ilaplace[i][j];
				}
			w=1;	/* The sum of the Laplace weights is 0 */
			break;
	}
	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		
		switch (img->imgtype)
		{
			case B_CHAR:
			{
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
					{
						c1 = 0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								c1 += cp1[ (x+i-1) + img->xmax*(y+j-1) +idx1 ] * ifilt[i][j];
							}
						c1 /= w;
						if (c1<0) c1=0; else if (c1>255) c1=255;
						cp2[x + img->xmax*y] = c1;
					}

				/* Copy back this slice */
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
						cp1[x + img->xmax*y+idx1] = cp2[x + img->xmax*y];
				break;
			}

			case B_SHORT:
			{
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
					{
						c1 = 0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								c1 += sp1[ (x+i-1) + img->xmax*(y+j-1) +idx1 ] * ifilt[i][j];
							}
						sp2[x + img->xmax*y] = c1 / w;
					}

				/* Copy back this slice */
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
						sp1[x + img->xmax*y+idx1] = sp2[x + img->xmax*y];
				break;
			}

			case B_FLOAT:
			{
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
					{
						c2 = 0.0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								c2 += fp1[ (x+i-1) + img->xmax*(y+j-1) +idx1 ] * ffilt[i][j];
							}
						fp2[x + img->xmax*y] = c2 / w;
					}

				/* Copy back this slice */
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
						fp1[x + img->xmax*y +idx1] = fp2[x + img->xmax*y];
				break;
			}

			case B_RGB:
			{
				for (rgb=0; rgb<3; rgb++)
				{
					for (y=1; y<img->ymax-1; y++)
						for (x=1; x<img->xmax-1; x++)
						{
							c1 = 0;
							for (j=0; j<=2; j++)
								for (i=0; i<=2; i++)
								{
									c1 += cp1[rgb+ 3*((x+i-1) + img->xmax*(y+j-1) +idx1) ] * ifilt[i][j];
								}
							c1 /= w;
							if (c1<0) c1=0; else if (c1>255) c1=255;
							cp2[rgb + 3*(x + img->xmax*y)] = c1;
						}

					/* Copy back this slice */
					for (y=1; y<img->ymax-1; y++)
						for (x=1; x<img->xmax-1; x++)
							cp1[rgb+3*(x + img->xmax*y+idx1)] = cp2[rgb+3*(x + img->xmax*y)];
				}
				break;
			}

		}
	
	}
	
	freebuf (&dest);
	make_minmax (img);
	img->uid = next_uid++;
}



/* Generalized Gaussian smoothing and LoG operator.
	This operator works an a NxN neighborhood and computes the
	convolution mask analytically. Sigma depends on N.
	Set LoG=1 to obtain the Mexican Hat operator instead of
	Gaussian smoothing.
	Note: To speed this up, we don't use readbuf but rather index
	the image array directly. Standard (clamp) boundary condition applies */




void gaussian_general (image_type* img, int n, int LoG)
{
image_type dest;
int x,y,x1,y1,z,i,j,n1,rgb,c1,k;
float c2,buf,sigma,w, sumpos, sumneg;
float sigma1[11] = {0.0,0.391,0.625,0.8, 1.0,1.6, 2.1, 2.56, 3.0, 3.54, 4.1};
float *ffilt;
float *fp1, *fp2;
short *sp1, *sp2;
unsigned char *cp1, *cp2;
long idx1;
float xdata[1000], ydata[1000];

	if (allocate_image (&dest, img->imgtype, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();

	cp1 = img->data; fp1 = img->data; sp1 = img->data;
	cp2 = dest.data; fp2 = dest.data; sp2 = dest.data;

	/* Pick a suitable sigma dependent on fsize (empirical formula
		based on Russ pg 169f) */
	
	/* if (n>9) n=9;					/* Clamp kernel size */
	if (n>9)
		sigma = 0.16*pow(n,1.42);	/* Empirical. Not good for small n. */
	else
		sigma = sigma1[n];			/* Some of Russ' values, some of my own */

	/* For LoG, we need a smaller sigma to get more of the side lobes */

	if (LoG) sigma*=0.5;

	/* Compute one quadrant of the filter kernel. Note that LoG is tricky because
		a zero sum must be _guaranteed_ even if the support is finite. Here's the
		kludge: We compute the sum of positive and negative elements and the
		difference. Then, we increase the magnitude of the negative elements
		to create a zero difference. */

	n1 = n+1; 
	ffilt = calloc ( SQR(n1), sizeof (float));
	if (ffilt==NULL) fatal_memerr();

	for (i=0; i<=n; i++)
		for (j=0; j<=n; j++)
		{
			ffilt[i + j*n1] = Gauss(sqrt(i*i+j*j),sigma, LoG);
			dp (3, "%d  %d  %f\n",i,j,ffilt[i+j*n1]);
		}

	if (LoG) 
	{

		dp (3, "Here is your LoG kernel before correction:\n");
		for (i=0; i<=n; i++)
		{
			dp (2, "    ");
			for (j=0; j<=n; j++)
			{
				dp (3, "%5.2f  ",ffilt[i + j*n1]);
			}
			dp (2, "\n");
		}

		sumpos=0.0; sumneg=0.0; c1=0; k=0;
		for (i=-n; i<=n; i++)
			for (j=-n; j<=n; j++)
			{
				buf = Gauss(sqrt(i*i+j*j),sigma, LoG);
				if (buf>0.0) sumpos+=buf;
				if (buf<0.0) sumneg+=buf;
				if (buf<0.0) c1++;			/* Number of negative elements */
				if (buf>0.0) k++;			/* Number of positive elements */
			}

		buf = sumpos+sumneg;				/* Excess energy in positive cells */
		dp (3, "LoG: Sum of positive cells %f, sum of %d negative cells %f\n",sumpos,c1,sumneg);
		for (i=0; i<=n; i++)
			for (j=0; j<=n; j++)
			{
				if ((buf>0.0) && (ffilt[i + j*n1] < 0.0))		/* Adjust a negative cell */
					ffilt[i + j*n1] += fabs(buf)/c1;
				if ((buf<0.0) && (ffilt[i + j*n1] > 0.0))		/* Adjust a positive cell */
					ffilt[i + j*n1] += fabs(buf)/k;
			}
		dp (3, "\nHere is your LoG kernel after correction:\n");
		for (i=0; i<=n; i++)
		{
			dp (3, "    ");
			for (j=0; j<=n; j++)
			{
				dp (3, "%5.2f  ",ffilt[i + j*n1]);
			}
			dp (3, "\n");
		}
	}


	if (debuglevel>2)
	{	
		i=0;
		for (w=-n; w<=n; w+=(float)n/50.0)
		{
			xdata[i] = w;
			ydata[i] = Gauss (w,sigma,LoG);
			i++;
		}
		graphme (xdata, ydata, i);
	}


	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;

		switch (img->imgtype)
		{
			case B_CHAR:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						c2 = 0; w=0;
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
							{
								x1 = x+i; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;
								y1 = y+j; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;
								buf = ffilt[ abs(i) + n1*abs(j) ];
								w+=buf;
								c2 += cp1[ x1 + img->xmax*(y1) +idx1 ] * buf;
							}
						if (!LoG) c2 /= w; c1 = (int)(0.5+c2);
						if (c1<0) c1=0; else if (c1>255) c1=255;
						cp2[x + img->xmax*y] = c1;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						cp1[x + img->xmax*y+idx1] = cp2[x + img->xmax*y];
				break;
			}

			case B_SHORT:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						c2 = 0; w=0;
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
							{
								x1 = x+i; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;
								y1 = y+j; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;
								buf = ffilt[ abs(i) + n1*abs(j) ];
								w+=buf;
								c2 += sp1[ x1 + img->xmax*(y1) +idx1 ] * buf;
							}
						if (!LoG) c2 /= w; c1 = (int)(0.5+c2);
						sp2[x + img->xmax*y] = c1;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						sp1[x + img->xmax*y+idx1] = sp2[x + img->xmax*y];
				break;
			}

			case B_FLOAT:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						c2 = 0.0; w=0.0;
						for (j=-n; j<=n; j++)
							for (i=-n; i<=n; i++)
							{
								x1 = x+i; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;
								y1 = y+j; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;
								buf = ffilt[ abs(i) + n1*abs(j) ];
								w+=buf;
								c2 += fp1[ x1 + img->xmax*(y1) +idx1 ] * buf;
							}
						if (!LoG) c2/=w;
						fp2[x + img->xmax*y] = c2;
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						fp1[x + img->xmax*y +idx1] = fp2[x + img->xmax*y];
				break;
			}

			case B_RGB:
			{
				for (rgb=0; rgb<3; rgb++)
				{
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
						{
							c2 = 0; w=0;
							for (j=-n; j<=n; j++)
								for (i=-n; i<=n; i++)
								{
									x1 = x+i; if (x1<0) x1=0; else if (x1>=img->xmax) x1=img->xmax-1;
									y1 = y+j; if (y1<0) y1=0; else if (y1>=img->ymax) y1=img->ymax-1;
									buf = ffilt[ abs(i) + n1*abs(j) ];
									w+=buf;
									c2 += cp1[rgb+ 3*((x1) + img->xmax*(y1) +idx1) ] * buf;
								}
							if (!LoG) c2 /= w; c1 = (int)(0.5+c2);
							if (c1<0) c1=0; else if (c1>255) c1=255;
							cp2[rgb + 3*(x + img->xmax*y)] = c1;
						}

					/* Copy back this slice */
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
							cp1[rgb+3*(x + img->xmax*y+idx1)] = cp2[rgb+3*(x + img->xmax*y)];
				}
				break;
			}

		}
	
	}
	
	freebuf (&dest);
	free (ffilt);
	make_minmax (img);
	img->uid = next_uid++;
}





/****************************************************************************/


/* Frei and Chen Filter */

/* The idea here is to decompose any 3x3 matrix of pixels (i.e. any 3x3 subimage)
	into a weighted sum of fixed matrices (the Frei & Chen kernels) so that
	the subimage can be restored from the matrices and the 9-dimensional
	feature vector. The Frei-Chen angle is the elevation of the 9-dimensional
	feature vector into the line or edge subspace. The function returns an angle
	where 0 is best possible match (longest projection of vector) and PI/2
	is the poorest match (shortest projection )
*/



float frei_chen_proj (float* b, int linemode)
{
int i;
float buf1, buf2;

	buf1 = 0; 
	if (linemode)
	{
		for (i=5; i<=8; i++) buf1 += b[i]; /* line space */
		buf2 = buf1 + b[0];
		for (i=1; i<=4; i++) buf2 += b[i]; /* edge space */
	}
	else
	{
		for (i=1; i<=4; i++) buf1 += b[i];	/* edge space */
		buf2 = buf1 + b[0];
		for (i=5; i<=8; i++) buf2 += b[i];	/* line space */
	}

	/* If this is background (i.e. buf2=0), we return 90 degrees, the
	   lowest likelihood of edge / line */
	   
	if (F_ISZERO(buf2)) return PI/4.0;

	
	return acos(sqrt(buf1/buf2));
}



long fnc_index (int x, int y, int xmax, int ymax, long ofs)
{

	if (x<=0) x=1; else if (x>=xmax) x=xmax;
	if (y<=0) y=1; else if (y>=ymax) y=ymax;
	return (x-1) + xmax*(y-1) +ofs;
}

   
void frei_chen_filter (image_type* img, int linemode)
{
image_type dest;
int x,y,x1,y1,z,i,j,k,rgb;
float *fp1, *fp2;
short *sp1, *sp2;
char *cp1, *cp2;
long idx1, idx2;
float buf;
float b[9];		/* The feature vector */
float rbuf[9];

/* The Frei and Chen kernels 0 through 8. Kernels 1 to 4 define edge space, and kernels 5 to 8 define line space */

float fc0[3][3] =  { {1.0, 1.0, 1.0} , {1.0, 1.0, 1.0}, {1.0, 1.0, 1.0} };			/* box average kernel */
float fc1[3][3] =  { {-1.0, -SQ2, -1.0} , {0.0, 0.0, 0.0}, {1.0, SQ2, 1.0} };		/* Horizontal edge */
float fc2[3][3] =  { {-1.0, 0.0, 1.0} , {-SQ2, 0.0, SQ2}, {-1.0, 0.0, 1.0} };		/* vertical edge */
float fc3[3][3] =  { {0.0, -1.0, SQ2} , {1.0, 0.0, -1.0}, {-SQ2, 1.0, 0.0} };		/* NW-to-SE edge, strange signs on SQ2?? */
float fc4[3][3] =  { {SQ2, -1.0, 0.0} , {-1.0, 0.0, 1.0}, {0.0, 1.0, -SQ2} };		/* NE-to-SW edge, strange signs on SQ2?? */
float fc5[3][3] =  { {0.0, 1.0, 0.0} , {-1.0, 0.0, -1.0}, {0.0, 1.0, 0.0} };		/* X-shaped line intersection */
float fc6[3][3] =  { {-1.0, 0.0, 1.0} , {0.0, 0.0, 0.0}, {1.0, 0.0, -1.0} };		/* + shaped,line intersection */
float fc7[3][3] =  { {1.0, -2.0, 1.0} , {-2.0, 4.0, -2.0}, {1.0, -2.0, 1.0} };		/* any diagonal line */
float fc8[3][3] =  { {-2.0, 1.0, -2.0} , {1.0, 4.0, 1.0}, {-2.0, 1.0, -2.0} };		/* any horizontal or vertical line */
float weights[9]= 	 {1.0/3.0, 1.0/(2.0*SQ2), 1.0/(2.0*SQ2), 1.0/(2.0*SQ2), 1.0/(2.0*SQ2),
								0.5, 0.5, 1.0/6.0, 1.0/6.0};						/* Weight factors for subspace composition */


	if (allocate_image (&dest, img->imgtype, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();

	cp1 = img->data; fp1 = img->data; sp1 = img->data;
	cp2 = dest.data; fp2 = dest.data; sp2 = dest.data;

	
	for (z=0; z<img->zmax; z++)
	{
		idx1 = img->xmax * img->ymax * z;
		
		switch (img->imgtype)
		{
			case B_CHAR:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						b[0]=b[1]=b[2]=b[3]=b[4]=b[5]=b[6]=b[7]=b[8]=0.0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								idx2 = fnc_index(x+i, y+j, img->xmax, img->ymax, idx1);
								rbuf[0] = cp1[ idx2 ] * fc0[i][j] * weights[0];
								rbuf[1] = cp1[ idx2 ] * fc1[i][j] * weights[1];
								rbuf[2] = cp1[ idx2 ] * fc2[i][j] * weights[2];
								rbuf[3] = cp1[ idx2 ] * fc3[i][j] * weights[3];
								rbuf[4] = cp1[ idx2 ] * fc4[i][j] * weights[4];
								rbuf[5] = cp1[ idx2 ] * fc5[i][j] * weights[5];
								rbuf[6] = cp1[ idx2 ] * fc6[i][j] * weights[6];
								rbuf[7] = cp1[ idx2 ] * fc7[i][j] * weights[7];
								rbuf[8] = cp1[ idx2 ] * fc8[i][j] * weights[8];
								for (k=0; k<9; k++) b[k] += SQR(rbuf[k]);
							}
						buf = frei_chen_proj (b,linemode);
						cp2[x + img->xmax*y] = (int)(180.0*buf/PI);
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						cp1[x + img->xmax*y+idx1] = cp2[x + img->xmax*y];
				break;
			}

			case B_SHORT:
			{
				for (y=1; y<img->ymax-1; y++)
					for (x=1; x<img->xmax-1; x++)
					{
						b[0]=b[1]=b[2]=b[3]=b[4]=b[5]=b[6]=b[7]=b[8]=0.0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								idx2 = fnc_index(x+i, y+j, img->xmax, img->ymax, idx1);
								rbuf[0] = sp1[ idx2 ] * fc0[i][j] * weights[0];
								rbuf[1] = sp1[ idx2 ] * fc1[i][j] * weights[1];
								rbuf[2] = sp1[ idx2 ] * fc2[i][j] * weights[2];
								rbuf[3] = sp1[ idx2 ] * fc3[i][j] * weights[3];
								rbuf[4] = sp1[ idx2 ] * fc4[i][j] * weights[4];
								rbuf[5] = sp1[ idx2 ] * fc5[i][j] * weights[5];
								rbuf[6] = sp1[ idx2 ] * fc6[i][j] * weights[6];
								rbuf[7] = sp1[ idx2 ] * fc7[i][j] * weights[7];
								rbuf[8] = sp1[ idx2 ] * fc8[i][j] * weights[8];
								for (k=0; k<9; k++) b[k] += SQR(rbuf[k]);
							}
						buf = frei_chen_proj (b, linemode);
						sp2[x + img->xmax*y] = (int)(180.0*buf/PI);
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						sp1[x + img->xmax*y+idx1] = sp2[x + img->xmax*y];
				break;
			}

			case B_FLOAT:
			{
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
					{
						b[0]=b[1]=b[2]=b[3]=b[4]=b[5]=b[6]=b[7]=b[8]=0.0;
						for (j=0; j<=2; j++)
							for (i=0; i<=2; i++)
							{
								idx2 = fnc_index(x+i, y+j, img->xmax, img->ymax, idx1);
								rbuf[0] = fp1[ idx2 ] * fc0[i][j] * weights[0];
								rbuf[1] = fp1[ idx2 ] * fc1[i][j] * weights[1];
								rbuf[2] = fp1[ idx2 ] * fc2[i][j] * weights[2];
								rbuf[3] = fp1[ idx2 ] * fc3[i][j] * weights[3];
								rbuf[4] = fp1[ idx2 ] * fc4[i][j] * weights[4];
								rbuf[5] = fp1[ idx2 ] * fc5[i][j] * weights[5];
								rbuf[6] = fp1[ idx2 ] * fc6[i][j] * weights[6];
								rbuf[7] = fp1[ idx2 ] * fc7[i][j] * weights[7];
								rbuf[8] = fp1[ idx2 ] * fc8[i][j] * weights[8];
								for (k=0; k<9; k++) b[k] += SQR(rbuf[k]);
							}
						buf = frei_chen_proj (b, linemode);
						fp2[x + img->xmax*y] = (180.0*buf/PI);
					}

				/* Copy back this slice */
				for (y=0; y<img->ymax; y++)
					for (x=0; x<img->xmax; x++)
						fp1[x + img->xmax*y +idx1] = fp2[x + img->xmax*y];
				break;
			}

			case B_RGB:
			{
				for (rgb=0; rgb<3; rgb++)
				{
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
						{
							b[0]=b[1]=b[2]=b[3]=b[4]=b[5]=b[6]=b[7]=b[8]=0.0;
							for (j=0; j<=2; j++)
								for (i=0; i<=2; i++)
								{
									idx2 = fnc_index(x+i, y+j, img->xmax, img->ymax, idx1);
									rbuf[0] = cp1[ rgb + 3*(idx2) ] * fc0[i][j] * weights[0];
									rbuf[1] = cp1[ rgb + 3*(idx2) ] * fc1[i][j] * weights[1];
									rbuf[2] = cp1[ rgb + 3*(idx2) ] * fc2[i][j] * weights[2];
									rbuf[3] = cp1[ rgb + 3*(idx2) ] * fc3[i][j] * weights[3];
									rbuf[4] = cp1[ rgb + 3*(idx2) ] * fc4[i][j] * weights[4];
									rbuf[5] = cp1[ rgb + 3*(idx2) ] * fc5[i][j] * weights[5];
									rbuf[6] = cp1[ rgb + 3*(idx2) ] * fc6[i][j] * weights[6];
									rbuf[7] = cp1[ rgb + 3*(idx2) ] * fc7[i][j] * weights[7];
									rbuf[8] = cp1[ rgb + 3*(idx2) ] * fc8[i][j] * weights[8];
									for (k=0; k<9; k++) b[k] += SQR(rbuf[k]);
								}
							buf = frei_chen_proj (b, linemode);
							cp2[rgb + 3*(x + img->xmax*y)] = (int)(180.0*buf/PI);
						}

					/* Copy back this slice */
					for (y=0; y<img->ymax; y++)
						for (x=0; x<img->xmax; x++)
							cp1[rgb+3*(x + img->xmax*y+idx1)] = cp2[rgb+3*(x + img->xmax*y)];
				}
				break;
			}

		}
	
	}
	
	freebuf (&dest);
	make_minmax (img);
	img->uid = next_uid++;
}


void frei_n_chen_main (int linemode)
{

	frei_chen_filter (&mainimg, linemode);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
}



/***************************************************************************/

void local_variance_2D (image_type* img, int nghsize)
{
image_type dest;
float Sx, Sxx, buf, avg, sd;
int cnt;
int x,y,z,i,j,x1,y1;
float* p;


	if (allocate_image (&dest, B_FLOAT, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr ();
	p = dest.data;
		
	for (z=0; z<img->zmax; z++)		/* run over all slices */
	{
	
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				Sx=0.0; Sxx=0.0; cnt=0; avg=0.0; sd=0.0;
				for (j=-nghsize; j<=nghsize; j++)
					for (i=-nghsize; i<=nghsize; i++)
					{
						x1=x+i; y1=y+j;
						if ((x1>=0) && (x1<img->xmax) && (y1>=0) && (y1<img->ymax))
						{
							buf = readbuf_flt (img,x1,y1,z);		/* Ulgy RGB handling! */
							Sx+=buf;
							Sxx+=SQR(buf);
							cnt++;
						}
					}
					
				if (cnt>0) 
				{
					avg=Sx/cnt;
					sd = (Sxx - SQR(Sx)/cnt)/cnt;
				}
				p[x + dest.xmax*(y + dest.ymax*z)] = sd;
			}
	}
					
	handback (img, &dest);


}


/* Extension of the local variance computation - compute local variance, local skew, and local kurtosis */
/* This is a crude kludge and has no proper interface */

void local_variance_ext (image_type* img, int nghsize)
{
image_type dest;
float Sx, Sxx, buf, avg, sd, Axxx, Axxxx;
int cnt;
int x,y,z,i,j,x1,y1;
float* p;


	if (allocate_image (&dest, B_FLOAT, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr ();
	p = dest.data;
		
	for (z=0; z<img->zmax; z++)		/* run over all slices */
	{
	
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				Sx=0.0; Sxx=0.0; cnt=0; avg=0.0; sd=0.0;
				for (j=-nghsize; j<=nghsize; j++)
					for (i=-nghsize; i<=nghsize; i++)
					{
						x1=x+i; y1=y+j;
						if ((x1>=0) && (x1<img->xmax) && (y1>=0) && (y1<img->ymax))
						{
							buf = readbuf_flt (img,x1,y1,z);		/* Ulgy RGB handling! */
							Sx+=buf;
							Sxx+=SQR(buf);
							cnt++;
						}
					}
					
				if (cnt>0) 
				{
					avg=Sx/cnt;
					sd = (Sxx - SQR(Sx)/cnt)/cnt;
				}

				Sx=0.0; Sxx=0.0; cnt=0; Axxx=0.0; Axxxx=0.0;
				for (j=-nghsize; j<=nghsize; j++)
					for (i=-nghsize; i<=nghsize; i++)
					{
						x1=x+i; y1=y+j;
						if ((x1>=0) && (x1<img->xmax) && (y1>=0) && (y1<img->ymax))
						{
							buf = readbuf_flt (img,x1,y1,z);		/* Ulgy RGB handling! */
							if (sd==0.0)
								Sx=0.0;
							else
								Sx = (buf - avg)/sqrt(sd);					/* used as buffer variable */
							Axxx+= Sx*Sx*Sx;
							Axxxx += Sx*Sx*Sx*Sx;
							cnt++;
						}
					}
				
				Axxx /= cnt;										/* Skew */
				Axxxx = Axxxx/cnt-3.0;								/* Kurtosis */
				if (Axxx>2) Axxx=0;
				if ((Axxxx<-3) || (Axxxx>3)) Axxxx=0;

				p[x + dest.xmax*(y + dest.ymax*z)] = Axxxx;
			}
	}
					
	handback (img, &dest);

}



void local_variance_main (int nghsize)
{

	local_variance_2D (&mainimg, nghsize);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
}




/***************************************************************************/

/* Kuwahara filter:
	A nonlinear, edge-preserving filter. Uses a 4N+1 square neighborhood.
	The neighborhood is divided into four regions, and each region's
	standard deviation is computed. The filter output is the mean value
	of the region with the lowest standard deviation.
	
	TODO: This filter needs to conserve the image type (mean!)
*/


/* Read functions wit a mirror boundary condition */

float kuwahara_read (image_type *img, int x, int y, int z)
{
	
	if (x<0) x=abs(x); else if (x>= img->xmax) x=img->xmax-x-1;
	if (y<0) y=abs(y); else if (y>= img->ymax) y=img->ymax-y-1;

	return readbuf_flt (img,x,y,z);
}

unsigned char kuwahara_read_rgb (image_type *img, int x, int y, int z, int elem)
{
	
	if (x<0) x=abs(x); else if (x>= img->xmax) x=img->xmax-x-1;
	if (y<0) y=abs(y); else if (y>= img->ymax) y=img->ymax-y-1;

	if (elem==0)
		return readbuf_rgb (img,x,y,z).red;
	else if (elem==1)
		return readbuf_rgb (img,x,y,z).green;
	else
		return readbuf_rgb (img,x,y,z).blue;
}


void kuwahara_2D_gray (image_type* img, int nghsize)
{
image_type dest;
float Sx, Sxx, buf;
float sd[4], mean[4];		/* mean and SD of the 4 regions */
float finalsd, finalmean;
int cnt;
int x,y,z,i,j,n;
long idx;
float* p;


	if (allocate_image (&dest, B_FLOAT, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr ();
	p = dest.data;

	n = 2*nghsize;		/* extent of neighborhood */
	
	for (z=0; z<img->zmax; z++)		/* run over all slices */
	{
		dp (1, "Kuwahara: Computing slice %d\n",z);

		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				
				/* Compute sdev & average for all 4 regions. Compute both because average
					is hardly any additional effort if we compute SD anyawy. */
				
				Sx=0.0; Sxx=0.0; cnt=0; for (i=0; i<4; i++) { mean[i]=0.0; sd[i]=0.0; }
				for (j=-n; j<=-1; j++)
					for (i=0; i<=n; i++)		/* Mirror boundary condition required or boundary always wins */
					{
						buf = kuwahara_read (img,x+i,y+j,z);
						Sx+=buf;	Sxx+=SQR(buf);		cnt++;
					}
				mean[0]=Sx/cnt;
				sd[0] = (Sxx - SQR(Sx)/cnt)/cnt;
				
				Sx=0.0; Sxx=0.0; cnt=0;
				for (j=0; j<=n; j++)
					for (i=1; i<=n; i++)
					{
						buf = kuwahara_read (img,x+i,y+j,z);
						Sx+=buf;	Sxx+=SQR(buf);		cnt++;
					}
				mean[1]=Sx/cnt;
				sd[1] = (Sxx - SQR(Sx)/cnt)/cnt;

				Sx=0.0; Sxx=0.0; cnt=0;
				for (j=0; j<=n; j++)
					for (i=-n; i<=0; i++)
					{
						buf = kuwahara_read (img,x+i,y+j,z);
						Sx+=buf;	Sxx+=SQR(buf);		cnt++;
					}
				mean[2]=Sx/cnt;
				sd[2] = (Sxx - SQR(Sx)/cnt)/cnt;
				
				Sx=0.0; Sxx=0.0; cnt=0;
				for (j=-n; j<=0; j++)
					for (i=-n; i<=-1; i++)
					{
						buf = kuwahara_read (img,x+i,y+j,z);
						Sx+=buf;	Sxx+=SQR(buf);		cnt++;
					}
				mean[3]=Sx/cnt;
				sd[3] = (Sxx - SQR(Sx)/cnt)/cnt;

				/* Find region with smallest SD and get its mean */
				
				finalsd=sd[0]; finalmean=mean[0];
				for (i=1; i<4; i++)
				{
					if (finalsd > sd[i]) { finalsd=sd[i]; finalmean=mean[i]; }
				}

				p[x + dest.xmax*(y + dest.ymax*z)] = finalmean;
			}
		
		/* Filter is done for this slice. Stuff back into original image space */
		
		switch (img->imgtype)
		{
			case B_CHAR:
			{
				unsigned char *pc = img->data;
				int ibuf;
				for (idx=0; idx < img->xmax*img->ymax; idx++)
				{
					buf = p[idx + dest.xmax*dest.ymax*z];
					ibuf = IROUND(buf);
					if (ibuf<0) ibuf=0; else if (ibuf>255) ibuf=255;
					pc[idx + dest.xmax*dest.ymax*z] = ibuf;
				}
				break;
			}
			case B_SHORT:
			{
				short *ps = img->data;
				int ibuf;
				for (idx=0; idx < img->xmax*img->ymax; idx++)
				{
					buf = p[idx + dest.xmax*dest.ymax*z];
					ibuf = IROUND(buf);
					ps[idx + dest.xmax*dest.ymax*z] = ibuf;
				}
				break;
			}
			case B_FLOAT:
			{
				float *pf = img->data;
				for (idx=0; idx < img->xmax*img->ymax; idx++)
					pf[idx + dest.xmax*dest.ymax*z] = p[idx + dest.xmax*dest.ymax*z];
				break;
			}
		}
	}
					
	make_minmax (img);
	freebuf (&dest);

}



void kuwahara_2D_rgb (image_type* img, int nghsize)
{
image_type dest;
float Sx, Sxx, buf;
float sd[4], mean[4];		/* mean and SD of the 4 regions */
float finalsd, finalmean;
int cnt;
int x,y,z,i,j,n,e,ibuf;
rgbtriplet *p;


	if (allocate_image (&dest, B_RGB, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr ();
	p = dest.data;

	n = 2*nghsize;		/* extent of neighborhood */
	
	for (z=0; z<img->zmax; z++)		/* run over all slices */
	{
		dp (1, "Kuwahara-rgb: Computing slice %d\n",z);

		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				
				/* Compute sdev & average for all 4 regions. Compute both because average
					is hardly any additional effort if we compute SD anyawy.
					For RGB; compute the result for all three channels R, G, and B */
				
				for (e=0; e<3; e++)
				{
					Sx=0.0; Sxx=0.0; cnt=0; for (i=0; i<4; i++) { mean[i]=0.0; sd[i]=0.0; }
					for (j=-n; j<=-1; j++)
						for (i=0; i<=n; i++)		/* Mirror boundary condition required or boundary always wins */
						{
							buf = kuwahara_read_rgb (img,x+i,y+j,z,e);
							Sx+=buf;	Sxx+=SQR(buf);		cnt++;
						}
					mean[0]=Sx/cnt;
					sd[0] = (Sxx - SQR(Sx)/cnt)/cnt;
					
					Sx=0.0; Sxx=0.0; cnt=0;
					for (j=0; j<=n; j++)
						for (i=1; i<=n; i++)
						{
							buf = kuwahara_read_rgb (img,x+i,y+j,z,e);
							Sx+=buf;	Sxx+=SQR(buf);		cnt++;
						}
					mean[1]=Sx/cnt;
					sd[1] = (Sxx - SQR(Sx)/cnt)/cnt;
	
					Sx=0.0; Sxx=0.0; cnt=0;
					for (j=0; j<=n; j++)
						for (i=-n; i<=0; i++)
						{
							buf = kuwahara_read_rgb (img,x+i,y+j,z,e);
							Sx+=buf;	Sxx+=SQR(buf);		cnt++;
						}
					mean[2]=Sx/cnt;
					sd[2] = (Sxx - SQR(Sx)/cnt)/cnt;
					
					Sx=0.0; Sxx=0.0; cnt=0;
					for (j=-n; j<=0; j++)
						for (i=-n; i<=-1; i++)
						{
							buf = kuwahara_read_rgb (img,x+i,y+j,z,e);
							Sx+=buf;	Sxx+=SQR(buf);		cnt++;
						}
					mean[3]=Sx/cnt;
					sd[3] = (Sxx - SQR(Sx)/cnt)/cnt;
	
					/* Find region with smallest SD and get its mean */
					
					finalsd=sd[0]; finalmean=mean[0];
					for (i=1; i<4; i++)
					{
						if (finalsd > sd[i]) { finalsd=sd[i]; finalmean=mean[i]; }
					}

					ibuf = IROUND(finalmean);
					if (ibuf<0) ibuf=0; else if (ibuf>255) ibuf=255;
					if (e==0)
						p[x + dest.xmax*(y + dest.ymax*z)].red = ibuf;
					else if (e==1)
						p[x + dest.xmax*(y + dest.ymax*z)].green = ibuf;
					else
						p[x + dest.xmax*(y + dest.ymax*z)].blue = ibuf;
				}
			}
	}

	handback (img, &dest);
}


void kuwahara_2D (image_type* img, int nghsize)
{
	if (img->imgtype == B_RGB)
		kuwahara_2D_rgb (img, nghsize);
	else
		kuwahara_2D_gray (img, nghsize);
}



void kuwahara_main (int nghsize)
{

	kuwahara_2D (&mainimg, nghsize);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
}




/***************************************************************/


/*	Find local maxima - deletes all points that are not local maxima
	in a neighborhood defined by env.
	NOTE (rgb): Luminance maxima are used in the RGB case.
	TODO: Should we fix this?
*/



/* Check if point at (x,y) in slice z is local maximum, return 1 if yes.
   Env determines what "local" means. This function is similar, but not
   identical, to the one used for the Watershed transform .

	Note that under this definition a local maximum is an ISOLATED
	pixel that has a value larger than ALL of its neighbors.
	This is a very strict definition. For a less strict definition,
	we'd have to use the Top Hat filter */


char is_locmax_2d (image_type* img, int x, int y, int z, int env)
{
char ismax; 
float maxval, curval;
int x1,y1,x2,y2,i,j;

	curval = readbuf_flt (img,x,y,z);

	/* Clamped square of size env */
	x1=x-env; if (x1<0) x1=0;
	x2=x+env; if (x2>=img->xmax) x2=img->xmax-1;
	y1=y-env; if (y1<0) y1=0;
	y2=y+env; if (y2>=img->ymax) y2=img->ymax-1;
	
	/* Maximum of the square environmetn */
	ismax = 1; maxval = readbuf_flt (img,x1,y1,z);
	for (i=y1; i<=y2; i++)
		for (j=x1; j<=x2; j++)
		{
			if (((i!=y) || (j!=x)) && (maxval<readbuf_flt(img,j,i,z))) maxval=readbuf_flt(img,j,i,z);
		}

	if (maxval>curval) ismax=0; /* use > for ridges and >= for single points...? */
								/* Interesting discussion, because >= does not detect all local
									maxima (it does not in flat regions where the maxval occurs in
									repeated pixels). OTOH, using > leaves the flat regions as such,
									meaning that a local maximum can be more than one pixel big under this
									definition. If a single pixel mut be STRICTLY enforced, we should use
									> in combination with a conditional-erode step to leave only UEPs. */

	return ismax;
}


int locmax_transform_2d (image_type* img, int env)
{
int x,y,z,e,cnt;
image_type temp;
unsigned char *src, *dest;


	e = DATALEN(img->imgtype);
	cnt=0;

	for (z=0; z<img->zmax; z++)
	{
		/* Start with one all-zero slice */
		
		if (allocate_image (&temp, img->imgtype, img->xmax, img->ymax, 1)==-1)
			fatal_memerr();
		src = img->data;
		dest = temp.data;

		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				if (is_locmax_2d (img,x,y,z,env))	/* Copy the value into buffer */
				{
					dp (2, "Local maximum at %d,%d,%d\n",x,y,z);
					memcpy (dest+(e*(x+y*img->xmax)), src+(e*(x+img->xmax*(y+z*img->ymax))), e);
					cnt++;
				}
			}
			
		/* Done with this slice - copy back into original image */
		
		memcpy (src+(e*z*img->xmax*img->ymax), dest, e*img->xmax*img->ymax);
		freebuf (&temp);
	}
	dp (1, "%d local maxima found.\n",cnt);

	make_minmax(img);
	img->uid = next_uid++;
	return cnt;
}				


void locmax_transform_main (int env)
{

	locmax_transform_2d (&mainimg, env);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
}


void gaussian_general_main (int env, int LoG)
{

	gaussian_general (&mainimg, env, LoG);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
}





/*************************************************************

	Convolution Filter GUI
	
*************************************************************/

GtkAdjustment* convolve_ngh_adj;
GtkWidget *convolve_ngh;
GtkWidget* convolve_label3;
int convolve_function;


void on_convolve_cancel_clicked  (GtkButton *button, gpointer user_data)
{

	gtk_widget_destroy (GTK_WIDGET(user_data));
}

void on_convolve_ok_clicked  (GtkButton *button, gpointer user_data)
{
int nghsize;

	undo_nexus (&mainimg);

	nghsize = convolve_ngh_adj->value;

	if (convolve_function==0)
		sobel_main ();
	else if (convolve_function==6)
		local_hoelder_main (nghsize);
	else if (convolve_function==7)
		local_variance_main (nghsize);
	else if (convolve_function==8)
		frei_n_chen_main (0);
	else if (convolve_function==9)
		frei_n_chen_main (1);
	else if (convolve_function==10)
		locmax_transform_main (nghsize);
	else if (convolve_function==11)
		gaussian_general_main (nghsize,0);
	else if (convolve_function==12)
		gaussian_general_main (nghsize,1);
	else if (convolve_function==13)
		compass_main ();
	else if (convolve_function==14)
		kuwahara_main (nghsize);
	else if (convolve_function==15)
		rangeop (&mainimg, nghsize);
	else
		convolve_filter (&mainimg, convolve_function);
	
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (0);
	gtk_widget_destroy (GTK_WIDGET(user_data));
}

void convolve_set_menu (GtkWidget *widget, gpointer user_data)
{

	convolve_function = *(int*) user_data;
	switch (convolve_function)
	{
		case 0:		/* Sobel */
		case 1:		/* Smooth */
		case 2:		/* Sharpen */
		case 3:		/* Sharpen more */
		case 4:		/* Laplace */
		case 8:		/* Frei & Chen */
		case 9:		/* Frei & Chen */
		case 13:	/* Compass */
		{									/* Filters restricted to a 3x3 neighborhood */
			gtk_widget_hide (convolve_ngh);
			gtk_widget_show (convolve_label3);
			break;
		}
		case 6:		/* Local Hoelder */
		case 7:		/* Local variance */
		case 10:	/* Local maxima */
		case 11:	/* Generalized Gaussian */
		case 12:	/* LoG */
		case 14:	/* Kuwahara */
		case 15:	/* Range operator */
		{								/* Filters with a general neighborhood */
			gtk_widget_hide (convolve_label3);
			gtk_widget_show (convolve_ngh);
			break;
		}
	}


}

void create_convolve_wnd (image_type* img)
{
  GtkWidget *convolve_wnd;
  GtkWidget *vbox1;
  GtkWidget *table1;
  GtkWidget *convolve_label1, *convolve_label2;
  GtkWidget *convolve_optionmenu1;
  GtkWidget *convolve_optionmenu1_menu;
  GtkWidget *glade_menuitem;
  GtkWidget *hbox1, *hsep1;
  GtkWidget *convolve_okbutton;
  GtkWidget *convolve_cancelbutton;

	convolve_function=0;

	convolve_wnd = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (convolve_wnd), "convolve_wnd", convolve_wnd);
	gtk_window_set_title (GTK_WINDOW (convolve_wnd), "Spatial-Domain Filters");

	vbox1 = vbox_in_window (convolve_wnd,"vbox1"); 
	table1 = MakeTable (convolve_wnd, vbox1, "table1", 2,2);

	convolve_label1 = MakeLabel (convolve_wnd, table1, "Filter type", "label1", 0,0);
	convolve_label2 = MakeLabel (convolve_wnd, table1, "Neighborhood size", "convolve_label2", 0,1);
	convolve_label3 = MakeLabel (convolve_wnd, table1, "(always 3x3)", "convolve_label3", 1,1);

	convolve_optionmenu1 = Make_Optionmenu (convolve_wnd, table1,"convolve_menu",1,0);
	gtk_container_set_border_width (GTK_CONTAINER (convolve_optionmenu1), 1);
	convolve_optionmenu1_menu = gtk_menu_new ();
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Sobel", (gpointer) &menuopt0);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Compass", (gpointer) &menuopt13);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Smooth (3x3 Gaussian)", (gpointer) &menuopt1);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Sharpen", (gpointer) &menuopt2);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Sharpen More", (gpointer) &menuopt3);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Laplacian", (gpointer) &menuopt4);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Local Hoelder", (gpointer) &menuopt6);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Local Variance", (gpointer) &menuopt7);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Frei & Chen (edge)", (gpointer) &menuopt8);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Frei & Chen (line)", (gpointer) &menuopt9);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Local maxima", (gpointer) &menuopt10);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Generalized Gaussian", (gpointer) &menuopt11);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Generalized LoG", (gpointer) &menuopt12);
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Kuwahara", (gpointer) &menuopt14);	
	glade_menuitem = Option_menuitem (convolve_optionmenu1_menu, GTK_SIGNAL_FUNC(convolve_set_menu), "Range Operator", (gpointer) &menuopt15);	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (convolve_optionmenu1), convolve_optionmenu1_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (convolve_optionmenu1), 0);

	convolve_ngh_adj = GTK_ADJUSTMENT(gtk_adjustment_new (1, 1, 12, 1, 1, 1));
	convolve_ngh = MakeSpinbutton (convolve_ngh_adj, convolve_wnd, table1,"convolve_ngh", 1,1);
	gtk_widget_hide (convolve_ngh);

	hsep1 = MakeHsep (convolve_wnd, vbox1, "hsep1", -1,-1);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox1);
	gtk_object_set_data_full (GTK_OBJECT (convolve_wnd), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox1, TRUE, TRUE, 0);

	convolve_okbutton = Button_in_table (convolve_wnd, hbox1, "OK", "convolve_okbutton",-1,-1,NULL);
	convolve_cancelbutton = Button_in_table (convolve_wnd, hbox1, "Cancel", "convolve_cancelbutton",-1,-1,NULL);

	gtk_signal_connect (GTK_OBJECT (convolve_okbutton), "clicked", GTK_SIGNAL_FUNC (on_convolve_ok_clicked), convolve_wnd);
	gtk_signal_connect (GTK_OBJECT (convolve_cancelbutton), "clicked", GTK_SIGNAL_FUNC (on_convolve_cancel_clicked), convolve_wnd);
	gtk_signal_connect (GTK_OBJECT (convolve_wnd), "destroy", GTK_SIGNAL_FUNC (on_convolve_cancel_clicked), convolve_wnd);

	gtk_widget_show (convolve_wnd); 

}



/* and the menu function for the convolution filters ... */

void convolve_filter_main ()
{

	create_convolve_wnd(&mainimg);

}


