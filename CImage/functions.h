






#ifndef __IMAGER_FUNCTIONS_H__
#define __IMAGER_FUNCTIONS_H__

int spl_cache_z;
int spl_cache_uid;
int spl_cache_deg;
long rndreg;

void dp (int pri, char *format,...);
void fatal_memerr ();
char isempty (char* s);

void	init_gsl_fn ();

char slice_is_zero (image_type* img, int slice);

void imgminmax (image_type* img, float* mmin, float* mmax);


hsvtriplet rgb_to_hsv (rgbtriplet rgb);
rgbtriplet hsv_to_rgb (hsvtriplet hsv);
rgbtriplet rgb_to_ycrcb (rgbtriplet rgb);
void karhunen_loeve_transform (image_type* img, int oorflg);


/* Interploation functions */

double spline_interpol (image_type* img, float xf, float yf, int z, int SplDeg);



#endif
