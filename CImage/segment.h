


#ifndef __IMAGER_SEGMENT_H__
#define __IMAGER_SEGMENT_H__

#include "defs.h"


void regiongrow_3d (image_type* img, vertex3d* seeds, int seednum, float lthresh, float uthresh, int radius, int lungprocessing);
void regiongrow_2d (image_type* img, int z, vertex3d* seeds, int seednum, float lthresh, float uthresh, int radius, int lungprocessing);
void regiongrow_main ();

void hysteresis_threshold (image_type *img, float tlow, float thigh);
void hysteresis_threshold_main (image_type* img);

void grow_merge (image_type* img, int zslice, int seedmeth, float tolerance, int clust_sep, int preview);
void grow_merge_main ();

void k_means_main ();


/* Prototypes for segment2.c */

void hillclimb_segmentation (image_type *img, float thresh, int locmax_radius, int numrays, int feature_radius);
void hillclimbing_main ();


#endif
