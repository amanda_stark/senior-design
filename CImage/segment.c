
/*
 *	Segment.c: Algorithms for segmentation
 * (excluding spiderop and snakes - they have extra files)
 */



/*		CRYSTAL IMAGE 0.9.0 BETA
		This program is copyrighted material.
		(C) 2002-2010 by Mark A. Haidekker, all rights reserved.
		
		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE:

		THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
		APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
		HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
		OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
		THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
		IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
		ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

		IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
		WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
		THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
		GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
		USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
		DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
		PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
		EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
		SUCH DAMAGES.

		For the full text of the license, please see the file COPYING
		in the project's main directory, or read it on the Web at
		http://www.gnu.org/copyleft/gpl.html

 */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "application.h"
#include "binary.h"
#include "dlgbox.h"
#include "functions.h"
#include "filter.h"
#include "findclust.h"
#include "graphdsp.h"
#include "gtkhelpers.h"
#include "imgbuf.h"
#include "roi.h"
#include "secondwnd.h"
#include "segment.h"
#include "special1.h"




/*****************************************************

	Helper functions.

	img_avg determines the average image value 
	inside a box with 2*radius side length centered 
	at position (x,y,z) 

	environ checks if a point at (x,y) may be set.
	This is the case if the point lies within the
	ROI and at least one neighbor is non-zero.
	
	environ3d is the same for 3D. While points in
	the x-y-plane can be 8-connected, adjoining slices
	have to be 2-connected.

******************************************************/


float img_avg (image_type* img, int x,int y,int z,int radius)
{
int i,j;
int cnt;
float avg;

	if (radius==0) return readbuf_flt (img,x,y,z);

	avg=0; cnt=0;
	for (i=x-radius; i<=x+radius; i++)
		for (j=y-radius; j<=y+radius; j++)
			if ((i>=0) && (i<img->xmax) && (j>=0) && (j<img->ymax))
			{
				avg += readbuf_flt (img,i,j,z);
				cnt++;
			}
			
	if (cnt==0) 
		return 0.0;
	else
		return avg/cnt;
}


/* Determine if there is a pixel set in the 3x3 neighborhood of (x,y) */

int environ (image_type* img, int x,int y)
{
int i,j;
int radius=1;

	if (!in_roi(x,y)) return 0;	/* Obey ROI if defined */


	if (img->imgtype==B_CHAR)
	{
		unsigned char *p = img->data;

		for (i=x-radius; i<=x+radius; i++)
			for (j=y-radius; j<=y+radius; j++)
				if ((i>=0) && (i<img->xmax) && (j>=0) && (j<img->ymax))
				{
					if (p[i + img->xmax*j]) return 1;
				}
	}
	else if (img->imgtype==B_SHORT)
	{
		short *p = img->data;

		for (i=x-radius; i<=x+radius; i++)
			for (j=y-radius; j<=y+radius; j++)
				if ((i>=0) && (i<img->xmax) && (j>=0) && (j<img->ymax))
				{
					if (p[i + img->xmax*j]) return 1;
				}
	}


	return 0;
}


int environ3d (image_type* img, int x,int y, int z)
{
int i,j; long idx;
int radius=1;
unsigned char* p;

	if (!in_roi(x,y)) return 0;	/* Obey ROI if defined */

	g_assert (img->imgtype==B_CHAR);
	p = img->data; idx = img->xmax * img->ymax * z;

	for (i=x-radius; i<=x+radius; i++)
		for (j=y-radius; j<=y+radius; j++)
			if ((i>=0) && (i<img->xmax) && (j>=0) && (j<img->ymax))
			{
				if (p[i + img->xmax*j + idx]) return 1;
			}
			
	if ((z>0) && (p[x + img->xmax*(y + img->ymax*(z-1))])) return 1;
	if ((z<img->zmax-1) && (p[x + img->xmax*(y + img->ymax*(z+1))])) return 1;
	return 0;
}


/* Alternative version, no ROI, selectable 2D / 3D. NOTE that 3D is 4-connected only */

int environ2d3d (image_type* img, int x,int y, int z, int is_3d)
{
int i,j,k; long idx;
int radius=1;
unsigned char* p;

	g_assert (img->imgtype==B_CHAR);
	p = img->data; idx = img->xmax * img->ymax * z;

	for (i=x-radius; i<=x+radius; i++)
		for (j=y-radius; j<=y+radius; j++)
			if ((i>=0) && (i<img->xmax) && (j>=0) && (j<img->ymax))
			{
				if (p[i + img->xmax*j + idx]) return 1;
			}
			
	if (!is_3d) return 0;			/* If slected 2D, we are done */
	
	if ((z>0) && (p[x + img->xmax*(y + img->ymax*(z-1))])) return 1;
	if ((z<img->zmax-1) && (p[x + img->xmax*(y + img->ymax*(z+1))])) return 1;
	return 0;
}






/**********************************************************************************************************/

/*         ****    *****    ***     ***     ***    *   *            ***    ****     ***    *   *          */
/*         *   *   *       *   *     *     *   *   *   *           *   *   *   *   *   *   *   *          */
/*         *   *   *       *         *     *   *   **  *           *       *   *   *   *   *   *          */
/*         ****    ****    *  **     *     *   *   * * *           *  **   ****    *   *   * * *          */
/*         * *     *       *   *     *     *   *   *  **           *   *   * *     *   *   * * *          */
/*         *  *    *       *   *     *     *   *   *   *           *   *   *  *    *   *   * * *          */
/*         *   *   *****    ***     ***     ***    *   *            ***    *   *    ***     * *           */
/*                                                                                                        */

/* The post-processing step for the lung segmentation. First,
   flood-fill the background to isolate deselected areas inside
   the lung. then, patch those isolated areas. Finally, erode
   the outside boundary to remove the transition.
   The mask is assumed to be 2D. The mask is a binary B_CHAR
   image and contains the values 0 for background and 255 (!)
   for foreground. The various values are set to allow visualization.
   In the end, anything below 128 is background (zero) and
   anything above is foreground (1) */



void postproc (image_type* mask, int z)
{
int x,y,j, level;
long i,idx;
unsigned char* p;
unsigned char* p1;
vertex seed;

	g_assert (mask->imgtype==B_CHAR);
	p = mask->data;
	p1 = (unsigned char*) calloc (mask->xmax*mask->ymax, sizeof (unsigned char));
	if (!p1) fatal_memerr();
	
	level = 0;
	/* rg_floodfill (mask, 1,1,z, &level);	Floodfill the background */

	seed = find_background_pixel (mask, z);
	if ( (seed.x==-1) || (seed.y==-1) )
		return;								/* Do nothing if no background seed pixel exists */
	rg_regiongrow (mask,seed, z);
	
	idx = z*mask->xmax*mask->ymax;
	for (y=0; y<mask->ymax; y++)
		for (x=0; x<mask->xmax; x++)
			if (p[x + mask->xmax*y+idx] == 0)	/* isolated hole */
				p[x + mask->xmax*y+idx] = 200;	/* patch it */
				
	for (j=0; j<2; j++) /* Two iterations of the erosion */
	{
		for (y=1; y<mask->ymax-1; y++)
			for (x=1; x<mask->xmax-1; x++)
			{
				idx = x + mask->xmax*(y+mask->ymax*z);
				if (p[idx] >= 200)	/* lung area */
				{
					if ((p[idx-1]<128) || (p[idx+1]<128) 
						|| (p[idx-mask->xmax]<128) || (p[idx+mask->xmax]<128)
						|| (p[idx-mask->xmax-1]<128) || (p[idx+mask->xmax+1]<128)
						|| (p[idx-mask->xmax+1]<128) || (p[idx+mask->xmax-1]<128))
						p1[x+mask->xmax*y]=100;	/* Erode flag */	
				}
			}

		idx = z*mask->xmax*mask->ymax;
		for (y=1; y<mask->ymax-1; y++)
			for (x=1; x<mask->xmax-1; x++)
			{
				i = x + mask->xmax*y;
				if (p1[i]==100) p[i+idx]=100; 
				p1[i]=0;
			}
	}
	free (p1);

}


/****************************************************+

	3D region growing. This algorithm starts
	at the 3D seed points provided in the seeds array.
	The average gray value computation remains 2D (!)

******************************************************/

/* TODO: for macros, we need to return an error code in case of illegal seed pts
         rather than using ShowMessage and having a garage-door sized memory leak */

void regiongrow_3d (image_type* img, vertex3d* seeds, int seednum, float lthresh, float uthresh, int radius, int lungprocessing)
{
unsigned char *p;	/* for the mask image */
image_type mask;
int i,x,y,z,changecnt,itercnt;
int x1, x2, y1, y2;
long idx;
float buf;
char s[256];
char zero[4] = {0,0,0,0};

	if (allocate_image (&mask, B_CHAR, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr ();
		
	p = mask.data;	/* This is calloc'ed above, so it should be all-zero */
	mask.minval.imin=0;  mask.maxval.imax=255;

	/* If a ROI is set, stay within ROI */
	
	roi_bbox (img, &x1,&y1,&x2,&y2);

	for (i=0; i<seednum; i++)
	{
		x = seeds[i].x; y = seeds[i].y; z = seeds[i].z;
		buf = img_avg (img,x,y,z,radius);
		if ((buf<lthresh) || (buf>uthresh) || (!in_roi(x,y)))
		{
			sprintf (s, "Error: Illegal seed point at %d,%d,%d",x,y,z);
			ShowMessage (s);
		}
		else
		{
			p[x + mask.xmax*(y + mask.ymax*z)] = 255;	/* Set the seed points */
			dp (1, "Seed point at %d,%d,%d\n",x,y,z);
		}
	}


	/* Region growing starts here */
	
	itercnt=0;
	do
	{
		itercnt++; changecnt=0;
		dp (1, "Iteration %d: ", itercnt);
	
		/* For speed-up, we switch directions in XY here */

		for (z=0; z<img->zmax; z++)
		{
			if (itercnt & 0x01)
			{
				for (y=y1; y<=y2; y++)
					for (x=x1; x<=x2; x++)
					{
						/* Consider only points which are not yet set and whcih have a 
						   "1" in the mask neighborhood */

						idx = x + mask.xmax* (y + mask.ymax*z);
						if (in_roi(x,y) && (p[idx]==0) && environ3d(&mask,x,y,z))
						{
							buf = img_avg (img,x,y,z,radius);
							if ((buf>=lthresh) && (buf<=uthresh))
							{
								p[idx] = 255;
								changecnt++;
							}
						}
							
					}
			}
			else
			{
				for (y=y2; y>=y1; y--)
					for (x=x2; x>=x1; x--)
					{
						/* Consider only points which are not yet set and which have a 
						   "1" in the mask neighborhood */

						idx = x + mask.xmax* (y + mask.ymax*z);
						if (in_roi(x,y) && (p[idx]==0) && environ3d(&mask,x,y,z))
						{
							buf = img_avg (img,x,y,z,radius);
							if ((buf>=lthresh) && (buf<=uthresh))
							{
								p[idx] = 255;
								changecnt++;
							}
						}
							
					}
			}
		}
	
		dp (1, "%d pixels\n",changecnt);

	}
	while (changecnt>0);

	/* post-processing step. Do this slice by slice */

	if (lungprocessing)
		for (z=0; z<img->zmax; z++)
			postproc (&mask,z);

	if (!macrorun) secondary_display (&mask,cur_slice,"Region Growing Mask");


	/* Perform the actual segmentation. Zero all non-selected points */
	
	for (z=0; z<img->zmax; z++)
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
				if (p[x + mask.xmax*(y + mask.ymax*z)]<128)
					writebuf (img, &zero, x,y,z);

	make_minmax (img);
	
	freebuf (&mask);


}


/****************************************************+

	2D region growing. This algorithm stays within
	the slice z and grows from the seed points
	provided in the seeds array. Only x and y coordinates
	of the seed points will be observed.

******************************************************/


void regiongrow_2d (image_type* img, int z, vertex3d* seeds, int seednum, float lthresh, float uthresh, int radius, int lungprocessing)
{
unsigned char *p;	/* for the mask image */
image_type mask;
int i,x,y,changecnt,itercnt;
int x1, x2, y1, y2;
float buf;
char s[256];
char zero[4] = {0,0,0,0};

	if (allocate_image (&mask, B_CHAR, img->xmax, img->ymax, 1)==-1)
		fatal_memerr ();
		
	p = mask.data;	/* This is calloc'ed above, so it should be all-zero */
	mask.minval.imin=0;  mask.maxval.imax=255;

	/* If a ROI is set, stay within ROI */
	
	roi_bbox (img, &x1,&y1,&x2,&y2);
	
	for (i=0; i<seednum; i++)
	{
		x = seeds[i].x; y = seeds[i].y; buf = img_avg (img,x,y,z,radius);
		if ((buf<lthresh) || (buf>uthresh) || (!in_roi(x,y)))
		{
			sprintf (s, "Error: Illegal seed point at %d,%d",x,y);
			ShowMessage (s);
		}
		else
		{
			p[x + mask.xmax*y] = 255;	/* Set the seed points */
			dp (1, "Seed point at %d,%d\n",x,y);
		}
	}

	itercnt=0;
	do
	{
		itercnt++; changecnt=0;
		if (!(itercnt & 0x07)) dp (2, "Iteration %d: ", itercnt);
	
		for (y=y1; y<=y2; y++)
			for (x=x1; x<=x2; x++)
			{
				/* Consider only points which are not yet set and which have a 
				   "1" in the mask neighborhood */
			
				if (in_roi(x,y) && (p[x + mask.xmax*y]==0) && environ(&mask,x,y))
				{
					buf = img_avg (img,x,y,z,radius);
					if ((buf>=lthresh) && (buf<=uthresh))
					{
						p[x + mask.xmax*y] = 255;
						changecnt++;
					}
				}
			
			}
	
		if (!(itercnt & 0x07)) dp (2, "%d pixels\n",changecnt);
	}
	while (changecnt>0);

	/* Postprocess if required and display the mask */

	if (lungprocessing) postproc (&mask,0);

	secondary_display (&mask,0,"Region Growing Mask");

	/* Perform the actual segmentation. Zero all non-selected points */
	
	for (y=0; y<img->ymax; y++)
		for (x=0; x<img->xmax; x++)
			if (p[x + mask.xmax*y]<128)
				writebuf (img, &zero, x,y,z);

	make_minmax (img);
	
	freebuf (&mask);

}




/******************************************
/*                                        */
/*          ***    *   *    ***           */
/*         *   *   *   *     *            */
/*         *       *   *     *            */
/*         *  **   *   *     *            */
/*         *   *   *   *     *            */
/*         *   *   *   *     *            */
/*          ***     ***     ***           */
/*                                        */
/* The region-growing GUI                 */
/*                                        */
/******************************************/


GtkWidget *rg_window;
GtkAdjustment* rg_seedpts_adj;
GtkAdjustment* rg_lthresh_adj;
GtkAdjustment* rg_uthresh_adj;
GtkAdjustment* rg_avgrad_adj;
GtkWidget *rg_2d3d;
GtkWidget *rg_label4;
int lungprocessing;



void  on_rg_2d3d_toggled (GtkWidget *nothing, gpointer user_data)
{

	if (gtk_toggle_button_get_active ((GtkToggleButton*) rg_2d3d))
		gtk_label_set_text (GTK_LABEL(rg_label4), "3D");
	else
		gtk_label_set_text (GTK_LABEL(rg_label4), "2D");

}

void on_rg_ok_clicked ( GtkButton * button, gpointer data )
{
float uthresh, lthresh;
int rad, seed;
int do_3d;
image_type* img;

	img = data;
	undo_nexus (img);

	do_3d = gtk_toggle_button_get_active ((GtkToggleButton*) rg_2d3d);
	lthresh = rg_lthresh_adj->value;
	uthresh = rg_uthresh_adj->value;
	rad =     rg_avgrad_adj->value;
	seed =    rg_seedpts_adj->value;
	
	dp (1, "Region growing: threshold range %f to %f, radius %d, %d seed points\n",
		lthresh, uthresh, rad, seed);

	dp (1, "Clickstack: %d, Seedpoints %d\n", clickstack_last, seed);

	if (clickstack_last<seed)
		ShowMessage ("Not enough seed points");
	else
	{

		if ((do_3d) && (img->zmax>1))
			regiongrow_3d (img, &clickstack[clickstack_last-seed],seed,lthresh,uthresh,rad,lungprocessing);
		else
			regiongrow_2d (img, cur_slice, &clickstack[clickstack_last-seed],seed,lthresh,uthresh,rad,lungprocessing);

		if (img == &mainimg) 
		{
			make_imlib_buffer_from_main_image (cur_slice);
			redraw (1);
		}
	}

	gtk_widget_destroy (rg_window);
}


void on_rg_cancel_clicked ( GtkButton * button, gpointer data )
{


	gtk_widget_destroy (rg_window);
}

void on_rg_toggled ( GtkButton * button, gpointer data )
{

	lungprocessing = !lungprocessing;
}



void create_rg_window (image_type* img)
{
  GtkWidget* vbox;
  GtkWidget *rg_table1;
  GtkWidget *rg_label1;
  GtkWidget *rg_label2;
  GtkWidget *rg_label3;
  GtkWidget *rg_label5;
  GtkWidget *rg_label6;
  GtkWidget *rg_lthresh;
  GtkWidget *rg_uthresh;
  GtkWidget *rg_avgrad;
  GtkWidget *rg_seedpts;
  GtkWidget* rg_toggle;
  GtkWidget *rg_okbutton;
  GtkWidget *rg_cancelbutton;
  
  float min, max;
  
	imgminmax (img, &min, &max);

	rg_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (rg_window), "rg_window", rg_window);
	gtk_container_set_border_width (GTK_CONTAINER (rg_window), 3);
	gtk_window_set_title (GTK_WINDOW (rg_window), "Region Growing");
	vbox = vbox_in_window (rg_window, "rg_vbox");
	rg_table1 = MakeTable (rg_window, vbox, "rg_table1", 2,7);

	rg_label1 = MakeLabel (rg_window,rg_table1,"Lower threshold","rg_label1",0,0);
	rg_label2 = MakeLabel (rg_window,rg_table1,"Upper threshold","rg_label2",0,1);
	rg_label3 = MakeLabel (rg_window,rg_table1,"Averaging radius","rg_label3",0,2);
	rg_label4 = MakeLabel (rg_window,rg_table1,"2D","rg_label4",0,3);
	rg_label5 = MakeLabel (rg_window,rg_table1,"# of seed points","rg_label5",0,4);
	rg_label6 = MakeLabel (rg_window,rg_table1,"Lung post-processing","rg_label6",0,5);

	rg_lthresh_adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, min, max, 1, 10, 0));
	rg_lthresh = MakeHScale (rg_lthresh_adj,rg_window,rg_table1,"rg_lthresh",1,0);
	gtk_scale_set_digits (GTK_SCALE (rg_lthresh), 0);

	rg_uthresh_adj = GTK_ADJUSTMENT (gtk_adjustment_new (max, min, max, 1, 10, 0));
	rg_uthresh = MakeHScale (rg_uthresh_adj,rg_window,rg_table1,"rg_uthresh",1,1);
	gtk_scale_set_digits (GTK_SCALE (rg_uthresh), 0);

	rg_avgrad_adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 10, 1, 1, 0));
	rg_avgrad = MakeHScale (rg_avgrad_adj,rg_window,rg_table1,"rg_avgrad",1,2);
	gtk_scale_set_digits (GTK_SCALE (rg_avgrad), 0);

	rg_2d3d = Togglebutton_in_table (rg_window,rg_table1,"2D / 3D","rg_2d3d",1,3,on_rg_2d3d_toggled);

	rg_seedpts_adj = GTK_ADJUSTMENT(gtk_adjustment_new (1, 1, CLICKSTACK_MAX, 1, 1, 0));
	rg_seedpts = MakeSpinbutton (rg_seedpts_adj,rg_window,rg_table1,"rg_seedpts",1,4);

	rg_toggle = Checkbutton_in_table (rg_window, rg_table1, " ", "rg_toggle", 1,5,on_rg_toggled);

	rg_okbutton = Button_in_table(rg_window,rg_table1,"OK","rg_okbutton",0,6,NULL);
	rg_cancelbutton = Button_in_table(rg_window,rg_table1,"Cancel","rg_cancelbutton",1,6,NULL);

	gtk_signal_connect (GTK_OBJECT (rg_window), "destroy",
                      GTK_SIGNAL_FUNC (on_rg_cancel_clicked), rg_window);
	gtk_signal_connect (GTK_OBJECT (rg_okbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_rg_ok_clicked),img);
	gtk_signal_connect (GTK_OBJECT (rg_cancelbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_rg_cancel_clicked),rg_window);

	gtk_widget_show (rg_window);
}



void regiongrow_main ()
{

	lungprocessing=0;
	create_rg_window (&mainimg);
	
}



/**********************************************************************************************************************/

/*   *   *   *    ***    *****   *****   ****    *****    ***    ***    ***       *****   *   *   ****     ***    *   */
/*   *   *   *   *   *     *     *       *   *   *       *   *    *    *   *        *     *   *   *   *   *   *   *   */
/*   *    * *    *         *     *       *   *   *       *        *    *            *     *   *   *   *   *       *   */
/*****     *      ***      *     ****    ****    ****     ***     *     ***         *     *****   ****     ***    *****/
/*   *     *         *     *     *       * *     *           *    *        *        *     *   *   * *         *   *   */
/*   *     *     *   *     *     *       *  *    *       *   *    *    *   *        *     *   *   *  *    *   *   *   */
/*   *     *      ***      *     *****   *   *   *****    ***    ***    ***         *     *   *   *   *    ***    *   */


/*  Hysteresis thresholding thresholds the image at a fairly high threshold (pass 1) and region-grows each 
	first-pass pixel using a lower threshold.
	NOTE - do we want to obey the ROI?
*/


void hysteresis_threshold (image_type *img, float tlow, float thigh)
{
image_type mask;
unsigned char *p;
int x,y,z;
int pixelcnt,itercnt;

	/* Allocate result image. This is a binary mask, so we use B_CHAR */

	if (allocate_image (&mask, B_CHAR, img->xmax, img->ymax, img->zmax)==-1)
		fatal_memerr();
	p = mask.data;
	mask.maxval.imax=1; mask.minval.imin=0;
	dp (1, "Hysteresis threshold: Upper limit %f, lower limit %f\n", thigh, tlow);

	/* Pass 1: Set all mask pixels to 1 where the image value exceeds thigh */
	
	
	for (z=0; z<img->zmax; z++)
		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				if (readbuf_flt (img,x,y,z) >= thigh)
					p[x + img->xmax*(y + img->ymax*z)] = 1;
			}

	/* Pass 2: Iteratively set all mask pixels in the 8-neighborhood of a set
		mask pixel provided that the image value exceeds tlow.
		This specific implementation is short, but slightly on the inefficient side. */

	itercnt = 0;
	do
	{
		pixelcnt  =0;			/* Number of pixels we set during this iteration */
	
		for (z=0; z<img->zmax; z++)
			for (y=0; y<img->ymax; y++)
				for (x=0; x<img->xmax; x++)
				{
					if (!p[x + img->xmax*(y + img->ymax*z)]		/* Mask bit must not be set */
						&& (readbuf_flt (img,x,y,z)>=tlow)		/* Do this first for computational efficiency */
						&& environ2d3d (&mask, x,y,z, 0) )	/* Choose 2D for now */
					{
						p[x + img->xmax*(y + img->ymax*z)] = 1;
						pixelcnt++;
					}
				}

		dp (1, "Hyteresis_threshold: Iteration %d, pixels added to mask: %d\n",++itercnt, pixelcnt);	
	}
	while (pixelcnt > 0);

	/* At this time, we can do one of two things.
		- we can hand back the mask, or
		- we can multiply the original image with the mask and return the thresholded image */

	handback (img, &mask);
}


GtkAdjustment *hystthr_minscale_adj;
GtkAdjustment *hystthr_maxscale_adj;


void on_hystthr_minscale_adj_changed (GtkWidget *adj, gpointer user_data)
{
		/* These are here just in case we want to try a preview */
}

void on_hystthr_maxscale_adj_changed (GtkWidget *adj, gpointer user_data)
{

}


void on_hystthr_ok_clicked ( GtkButton * button, gpointer data )
{

	undo_nexus (&mainimg);
	
	hysteresis_threshold (&mainimg, hystthr_minscale_adj->value, hystthr_maxscale_adj->value);
	make_imlib_buffer_from_main_image (cur_slice);
	redraw (1);
	
	gtk_widget_destroy ( GTK_WIDGET ( data ) );
}


void on_hystthr_cancel_clicked ( GtkButton * button, gpointer data )
{

	gtk_widget_destroy ( GTK_WIDGET ( data ) );
}




void create_hystthresh_wnd (image_type* img)
{
  GtkWidget *thresh_wnd;
  GtkWidget *hystthr_vbox1;
  GtkWidget *table1;
  GtkWidget *label1, *label2, *hsep1;
  GtkWidget *hystthr_minscale;
  GtkWidget *hystthr_maxscale;
  GtkWidget *hystthr_okbutton;
  GtkWidget *hystthr_cancelbutton;

  float min, max, stepsz;


	imgminmax (img, &min, &max);
	thresh_return.t = min; thresh_return.u = max;
	thresh_return.thresh_type = TH_BINARY;
	stepsz=1.0; 	/* for all int images (rgb, char, short) */
	if (img->imgtype==B_FLOAT) stepsz=0.1;
	
	thresh_wnd = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (thresh_wnd), "thresh_wnd", thresh_wnd);
	gtk_window_set_title (GTK_WINDOW (thresh_wnd), "Hysteresis Threshold");
	gtk_window_set_modal (GTK_WINDOW (thresh_wnd), TRUE);

	hystthr_vbox1 = vbox_in_window (thresh_wnd,"hystthr_vbox1");
	table1 = MakeTable (thresh_wnd, hystthr_vbox1, "hy_table1", 2,4);

	label1 = MakeLabel (thresh_wnd,table1,"Upper Threshold","hy_label1",0,0);
	hystthr_maxscale_adj = GTK_ADJUSTMENT (gtk_adjustment_new (min, min, max, stepsz, 10*stepsz, 0));
	hystthr_maxscale = MakeHScale (GTK_ADJUSTMENT(hystthr_maxscale_adj), thresh_wnd, table1, "hystthr_maxscale", 1,0);

	label2 = MakeLabel (thresh_wnd,table1,"Lower Threshold","hy_label2",0,1);
	hystthr_minscale_adj = GTK_ADJUSTMENT (gtk_adjustment_new (max, min, max, stepsz, 10*stepsz, 0));
	hystthr_minscale = MakeHScale (GTK_ADJUSTMENT(hystthr_minscale_adj), thresh_wnd, table1, "hystthr_minscale", 1,1);

	if (img->imgtype!=B_FLOAT)
	{
		gtk_scale_set_digits (GTK_SCALE (hystthr_minscale), 0);
		gtk_scale_set_digits (GTK_SCALE (hystthr_maxscale), 0);
	}
	
	hsep1 = MakeHsep (thresh_wnd, table1, "hsep1", 0,2);
	hsep1 = MakeHsep (thresh_wnd, table1, "hsep2", 1,2);

	hystthr_okbutton = Button_in_table (thresh_wnd,table1,"OK","hystthr_okbutton",0,3,NULL);
	hystthr_cancelbutton = Button_in_table (thresh_wnd,table1,"Cancel","hystthr_cancelbutton",1,3,NULL);

	gtk_signal_connect (GTK_OBJECT (thresh_wnd), "destroy",
                      GTK_SIGNAL_FUNC (on_hystthr_cancel_clicked), thresh_wnd);
	gtk_signal_connect (GTK_OBJECT (hystthr_okbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_hystthr_ok_clicked),thresh_wnd);
	gtk_signal_connect (GTK_OBJECT (hystthr_cancelbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_hystthr_cancel_clicked),thresh_wnd);

	gtk_widget_show (thresh_wnd);
}


void hysteresis_threshold_main (image_type* img)
{

	create_hystthresh_wnd (img);
}



/**************************************************************************/

/*         *   *           *   *   *****     *     *   *    ***           */
/*         *  *            ** **   *        * *    *   *   *   *          */
/*         * *             * * *   *       *   *   **  *   *              */
/*         **      *****   * * *   ****    *   *   * * *    ***           */
/*         * *             *   *   *       *****   *  **       *          */
/*         *  *            *   *   *       *   *   *   *   *   *          */
/*         *   *           *   *   *****   *   *   *   *    ***           */
/*                                                                        */
/**************************************************************************

	K-Means Clustering as segmentation method

	k-Means assumes a pre-thresholded image where background is zero.
 	First, we determine the initial centroids using either the Euclidean
 	distance map (UEPs) or local maxima, depending on seedmeth.
*/


typedef struct
{
	int x,y;
	float val,newx,newy;
	int num,cnt;
}
kmlist_type;


float kmdist (int x, int y, float buf, kmlist_type km, char usegrey)
{
float r;

	if (usegrey)
		r = SQR(x - km.x) + SQR(y - km.y) + SQR (buf - km.val);
	else
		r = SQR(x - km.x) + SQR(y - km.y);

	return r;
}

/* Fuzzy c-Means membership function */

float cmdist (int x, int y, kmlist_type km, int dist)
{
float r;

	r = SQR(x - km.x) + SQR(y - km.y);
	if (r>dist) return 0;
	return 1 - r/dist;
}




void k_means_2d (image_type* img, int z, kmlist_type* kmlist, int cnt, int mindist, int maxdist, char usegrey)
{
image_type temp;
int x,y,xmax,ymax,minidx,i,itercnt;
short *ptemp;
float buf, mindst,epsilon;


	if (allocate_image (&temp, B_SHORT, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();
	xmax=img->xmax; ymax=img->ymax;
	ptemp = temp.data;


	/* For each selected pixel, find the closest centroid
	   and assign its number to the pixel (in temp).
	   At the same time, compute the new centroids */
	
	itercnt=0;
	do
	{
		for (i=0; i<cnt; i++)
		{
			kmlist[i].newx = 0.0; kmlist[i].newy = 0.0; kmlist[i].cnt=0;
		}

		/* Assign each pixel to its closest centroid */

		for (y=0; y<ymax; y++)	
			for (x=0; x<xmax; x++)
			{
				buf = readbuf_flt (img,x,y,z);
				if (buf > 0) 					/* Remember: Pre-thresholded */
				{
					mindst = kmdist (x,y,buf, kmlist[0], usegrey);
					minidx = 0;
					for (i=1; i<cnt; i++)	/* Find closest centroid */
					{
						if (kmdist (x,y,buf, kmlist[i], usegrey) < mindst)
						{
							mindst = kmdist (x,y,buf, kmlist[i], usegrey);
							minidx = i;
						}
					}
					ptemp[x + xmax*y] = kmlist[minidx].num;
					kmlist[minidx].newx += x;
					kmlist[minidx].newy += y;
					kmlist[minidx].cnt++;
				}
			}
		
		/* Update the centroids */
		
		epsilon = 0;
		for (i=0; i<cnt; i++)
			if (kmlist[i].cnt>0)
			{
				kmlist[i].newx /= kmlist[i].cnt;
				kmlist[i].newy /= kmlist[i].cnt;
				x = (int)(0.5+kmlist[i].newx);
				y = (int)(0.5+kmlist[i].newy);

				dp (3, "Updating cluster %d from %d,%d to %d,%d\n",i,
					kmlist[i].x,kmlist[i].y, x, y);

				buf = SQR(x-kmlist[i].x) + SQR(y-kmlist[i].y);
				if (buf>epsilon) epsilon = buf;
				kmlist[i].x=x;
				kmlist[i].y=y;
			}
			
		dp (2, "k-Means: Iteration %d, epsilon=%f\n",++itercnt,epsilon);
	}
	while ((epsilon > 1) && (itercnt < 2000));

	handback (img, &temp);

}




void fuzzy_c_means_2d (image_type* img, int z, kmlist_type* kmlist, int cnt, int mindist, int maxdist, char usegrey)
{
image_type temp;
int x,y,xmax,ymax,minidx,i,itercnt;
long idx;
short *ptemp;
float buf, r,w,epsilon,min;
float *U;


	if (allocate_image (&temp, B_SHORT, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();
	xmax=img->xmax; ymax=img->ymax;
	ptemp = temp.data;

	/* As opposed to k-Means, we need the full membership matrix here */
	
	U = calloc (img->xmax * img->ymax * cnt, sizeof (float));
	if (U==NULL) fatal_memerr ();


	/* For each selected pixel, find the closest centroid
	   and assign its number to the pixel (in temp).
	   At the same time, compute the new centroids */
	
	itercnt=0;
	do
	{
		for (i=0; i<cnt; i++)
		{
			kmlist[i].newx = 0.0; kmlist[i].newy = 0.0; kmlist[i].val=0.0;
		}

		/* Compute the membership function for each pixel */

		for (y=0; y<ymax; y++)	
			for (x=0; x<xmax; x++)
			{
				idx = cnt*(x + y*img->xmax);
				buf = readbuf_flt (img,x,y,z);
				if (buf > 0) 					/* Remember: Pre-thresholded */
				{
					w=0.0;
					for (i=0; i<cnt; i++)	/* for each centroid... */
					{
						r = cmdist (x,y,kmlist[i],mindist);
						U[idx+i] = r;
						w += r;
					}

					/* Normalize so that total membership is 1 */
					
					if (w>0)
						for (i=0; i<cnt; i++)	/* for each centroid... */
						{
							U[idx+i] /= w;
							kmlist[i].x += U[idx+i]*x;
							kmlist[i].y += U[idx+i]*y;
							kmlist[i].val += U[idx+i];
						}
					
				}
			}
					
		
		/* Update the centroids */
		
		epsilon = 0;
		for (i=0; i<cnt; i++)
			if (kmlist[i].val>0)
			{
				kmlist[i].newx /= kmlist[i].val;
				kmlist[i].newy /= kmlist[i].val;
				x = (int)(0.5+kmlist[i].newx);
				y = (int)(0.5+kmlist[i].newy);

				dp (3, "Updating cluster %d from %d,%d to %d,%d\n",i,
					kmlist[i].x,kmlist[i].y, x, y);

				buf = SQR(x-kmlist[i].x) + SQR(y-kmlist[i].y);
				if (buf>epsilon) epsilon = buf;
				kmlist[i].x=x;
				kmlist[i].y=y;
			}
			
		dp (2, "c-Means: Iteration %d, epsilon=%f\n",++itercnt,epsilon);
	}
	while ((epsilon > 1) && (itercnt < 2000));

	/* Last, get the maximum membership for each pixel and store in temp */

	for (y=0; y<ymax; y++)	
		for (x=0; x<xmax; x++)
		{
			idx = cnt*(x + y*img->xmax);
			min=U[idx]; minidx=0;

			for (i=1; i<cnt; i++)	/* for each centroid... */
			{
				if (min<U[idx+i]) { min=U[idx+i]; minidx=i; }
			}
			
			ptemp[x + y*img->xmax] = minidx;
		}
			
			


	free (U);

	handback (img, &temp);

}




int k_means_seed (image_type* img, kmlist_type** km_list, int z, int ngh, char seedmeth, int numverts, float thresh)
{
image_type seed;
int e,cnt,x,y,xmax,ymax,i;
unsigned char nullelem[4] = {0,0,0,0};
unsigned char *psrc, *pdest;
cluster_type* clist;
kmlist_type* kmlist;


	if (allocate_image (&seed, img->imgtype, img->xmax, img->ymax, 1)==-1)
		fatal_memerr();
	xmax=img->xmax; ymax=img->ymax;
	e = DATALEN(img->imgtype);
	psrc = img->data; pdest = seed.data;
	memcpy (pdest, psrc + z*xmax*ymax*e, xmax*ymax*e);

	kmlist = calloc (32767, sizeof (kmlist_type));
	if (!kmlist) fatal_memerr();


	/* Part 1: Initialize the seedpoints or initial centroids */

	if (seedmeth==0)			/* clickstack to get seed points */
	{
		cnt = MIN(numverts, clickstack_last);
		for (i=0; i<cnt; i++)
		{
			kmlist[i].x = clickstack[i].x;
			kmlist[i].y = clickstack[i].y;
			kmlist[i].val = readbuf_flt (img,kmlist[i].x,kmlist[i].y,z);
			kmlist[i].num = i+1;
		}
		
	}
	else if (seedmeth==1)		/* UEP as seed method */
	{
		dp (1, "k-Means: Computing UEPs\n");
		thresh_return.t=thresh_return.r=thresh_return.g=thresh_return.b=1;
		cnt=create_UEP (&seed, 0);

		/* Since the UEP allows for ridges and other multiple points,
		   clusterize and use only the centroids */

		clist = calloc (32760, sizeof (cluster_type));
		cnt = findclust_2d (&seed, 0,32700, 1, 1, 0, clist);
		for (i=0; i<cnt; i++)
		{
			kmlist[i].x = clist[i].cog_x;
			kmlist[i].y = clist[i].cog_y;
			kmlist[i].val = readbuf_flt (img,kmlist[i].x,kmlist[i].y,z);
			kmlist[i].num = i+1;
		}
		free (clist);

	}
	else if (seedmeth==2)	/* Local maxima as seed method */
	{
		dp (1, "k-Means: Computing local maxima\n");
		cnt = locmax_transform_2d (&seed, ngh);

		/* Extract all seed points into a table
		   and remove whatever is below threshold */

		cnt=0;
		for (y=0; y<ymax; y++)	
			for (x=0; x<xmax; x++)
			{
				if ((readbuf_flt (&seed,x,y,0) < thresh) || (cnt>=37260))
				{
					writebuf (&seed, &nullelem, x,y,0);
				}
				else
				{
					kmlist[cnt].x = x; kmlist[cnt].y = y;
					kmlist[cnt].val = readbuf_flt (img,x,y,z);
					kmlist[cnt].num=cnt+1;
					cnt++;
				}
			}
	}
	else if (seedmeth==3)	/* Seed a number of points by random */
	{
		cnt = numverts;
		for (i=0; i<cnt; i++)
		{
			kmlist[i].x = rand() % img->xmax;
			kmlist[i].y = rand() % img->ymax;
			kmlist[i].val = readbuf_flt (img,kmlist[i].x,kmlist[i].y,z);
			kmlist[i].num = i+1;
		}
	
	}
	else
		return 0;

	dp (1, "k-Means: %d seed points\n",cnt);
	
	freebuf (&seed);
	*km_list = kmlist;
	return cnt;
}



/****************************************************************
			k-Means GUI
*/


GtkAdjustment* km_thresh_adj;
GtkAdjustment* km_nghsize_adj;
GtkAdjustment* km_mindist_adj;
GtkAdjustment* km_maxdist_adj;
GtkAdjustment* km_clickstack_adj;
GtkWidget *km_toggle;
int km_seedfunction;



void on_km_cancel_clicked ( GtkButton * button, gpointer data )
{

	gtk_widget_destroy (GTK_WIDGET(data));
}


void on_km_ok_clicked ( GtkButton * button, gpointer data )
{
char seedmeth;
int ngh, numverts, cnt;
float thresh;
int mindist;
int maxdist;
char usegrey;
kmlist_type* kmlist = NULL;

	undo_nexus (&mainimg);

	thresh = km_thresh_adj->value;
	mindist = km_mindist_adj->value;
	maxdist = km_maxdist_adj->value;
	seedmeth = km_seedfunction;
	numverts = km_clickstack_adj->value;
	ngh = km_nghsize_adj->value;
	usegrey = gtk_toggle_button_get_active ((GtkToggleButton*)km_toggle);

	/* Two-step process. First, we create a list of initial vertices (kmlist),
	   then we perform the actual k-Means clustering on the mainimage */

	cnt = k_means_seed (&mainimg, &kmlist, cur_slice, ngh, seedmeth, numverts, thresh);
	k_means_2d (&mainimg, cur_slice, kmlist, cnt, mindist, maxdist, usegrey);
	free (kmlist);

	cur_slice=0;
	make_imlib_buffer_from_main_image (cur_slice);

	gtk_widget_destroy (GTK_WIDGET(data));
}


void km_set_menu (GtkWidget *widget, gpointer user_data)
{
	km_seedfunction = *(int*) user_data;
	if (km_seedfunction==0)
	{
		km_clickstack_adj->upper=5;
		if (km_clickstack_adj->value > 5) km_clickstack_adj->value=5;
	}
	else if (km_seedfunction==3)
	{
		km_clickstack_adj->upper=1000;
	}
}



void create_kmeans_window (image_type* img)
{
  GtkWidget *km_window;
  GtkWidget* vbox;
  GtkWidget *km_table1;
  GtkWidget *km_label1, *km_label2, *km_label3, *km_label4, *km_label5, *km_label6, *km_label7;
  GtkWidget *km_thresh, *km_clickstack;
  GtkWidget *km_nghsize;
  GtkWidget *km_mindist, *km_maxdist;
  GtkWidget *km_optionmenu1, *km_optionmenu1_menu, *glade_menuitem;
  GtkWidget *km_okbutton;
  GtkWidget *km_cancelbutton;
  
  float min, max;
  
	imgminmax (img, &min,&max);

	km_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (km_window), "km_window", km_window);
	gtk_container_set_border_width (GTK_CONTAINER (km_window), 3);
	gtk_window_set_title (GTK_WINDOW (km_window), "k-Means Clustering");
	vbox = vbox_in_window (km_window, "km_vbox");
	km_table1 = MakeTable (km_window, vbox, "km_table1", 2,8);

	km_label1 = MakeLabel (km_window,km_table1,"Seed points by","km_label1",0,0);
	km_label7 = MakeLabel (km_window,km_table1,"Number of clickstack points","km_label7",0,1);
	km_label2 = MakeLabel (km_window,km_table1,"Threshold","km_label2",0,2);
	km_label3 = MakeLabel (km_window,km_table1,"Neighborhood size","km_label3",0,3);
	km_label4 = MakeLabel (km_window,km_table1,"Use img value in distance","km_label4",0,4);
	km_label5 = MakeLabel (km_window,km_table1,"Min allowable distance","km_label5",0,5);
	km_label6 = MakeLabel (km_window,km_table1,"Max allowable distance","km_label6",0,6);

	km_optionmenu1 = Make_Optionmenu (km_window, km_table1,"km_menu",1,0);
	gtk_container_set_border_width (GTK_CONTAINER (km_optionmenu1), 1);
	km_optionmenu1_menu = gtk_menu_new ();
	glade_menuitem = Option_menuitem (km_optionmenu1_menu, GTK_SIGNAL_FUNC(km_set_menu), "Clickstack", (gpointer) &menuopt0);
	glade_menuitem = Option_menuitem (km_optionmenu1_menu, GTK_SIGNAL_FUNC(km_set_menu), "UEP", (gpointer) &menuopt1);
	glade_menuitem = Option_menuitem (km_optionmenu1_menu, GTK_SIGNAL_FUNC(km_set_menu), "Local maxima", (gpointer) &menuopt2);
	glade_menuitem = Option_menuitem (km_optionmenu1_menu, GTK_SIGNAL_FUNC(km_set_menu), "Random", (gpointer) &menuopt3);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (km_optionmenu1), km_optionmenu1_menu);
	km_seedfunction=0;

	km_clickstack_adj = GTK_ADJUSTMENT (gtk_adjustment_new (1, 1,5, 1, 1, 0));
	km_clickstack = MakeHScale (km_clickstack_adj,km_window,km_table1,"km_clickstack",1,1);
	gtk_scale_set_digits (GTK_SCALE (km_clickstack), 0);

	km_thresh_adj = GTK_ADJUSTMENT (gtk_adjustment_new (1, min, max, 1, 10, 0));
	km_thresh = MakeHScale (km_thresh_adj,km_window,km_table1,"km_thresh",1,2);
	if (img->imgtype!=B_FLOAT) gtk_scale_set_digits (GTK_SCALE (km_thresh), 0);

	km_nghsize_adj = GTK_ADJUSTMENT (gtk_adjustment_new (1, 1, 7, 1,1,0));
	km_nghsize = MakeHScale (km_nghsize_adj,km_window,km_table1,"km_nghsize",1,3);
	gtk_scale_set_digits (GTK_SCALE (km_nghsize), 0);

	km_toggle = Checkbutton_in_table (km_window, km_table1, " ", "km_toggle", 1,4,NULL);

	km_mindist_adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, MIN(img->xmax,img->ymax), 1, 10, 0));
	km_mindist = MakeHScale (km_mindist_adj,km_window,km_table1,"km_mindist",1,5);
	gtk_scale_set_digits (GTK_SCALE (km_mindist), 0);

	km_maxdist_adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, MIN(img->xmax,img->ymax), 1, 10, 0));
	km_maxdist = MakeHScale (km_maxdist_adj,km_window,km_table1,"km_maxdist",1,6);
	gtk_scale_set_digits (GTK_SCALE (km_maxdist), 0);

	km_okbutton = Button_in_table(km_window,km_table1,"OK","km_okbutton",0,7,NULL);
	km_cancelbutton = Button_in_table(km_window,km_table1,"Cancel","km_cancelbutton",1,7,NULL);

	gtk_signal_connect (GTK_OBJECT (km_window), "destroy",
                      GTK_SIGNAL_FUNC (on_km_cancel_clicked), km_window);
	gtk_signal_connect (GTK_OBJECT (km_okbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_km_ok_clicked),km_window);
	gtk_signal_connect (GTK_OBJECT (km_cancelbutton), "clicked",
                      GTK_SIGNAL_FUNC (on_km_cancel_clicked),km_window);

	gtk_widget_show (km_window);
}



void k_means_main ()
{

	create_kmeans_window (&mainimg);
	
}



