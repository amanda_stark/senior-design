
/*
	StatOp: Histogram and general statistical operations 
*/



/*		CRYSTAL IMAGE 0.9.0 BETA
		This program is copyrighted material.
		(C) 2002-2010 by Mark A. Haidekker, all rights reserved.
		
		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE:

		THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
		APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
		HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
		OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
		THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
		IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
		ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

		IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
		WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
		THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
		GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
		USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
		DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
		PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
		EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
		SUCH DAMAGES.

		For the full text of the license, please see the file COPYING
		in the project's main directory, or read it on the Web at
		http://www.gnu.org/copyleft/gpl.html

 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>



#include "defs.h"
#include "imgbuf.h"
#include "dlgbox.h"
#include "statop.h"
#include "gtkhelpers.h"
#include "graphdsp.h"
#include "roi.h"
#include "application.h"
#include "functions.h"
#include "functions2.h"
#include "viz.h"


/* TODO: We need to combine several drawing functions in
   graphdsp.c with this file.
*/


/* Number of classes limited to 256 in "numbins" spinbutton */

#define MAXCLASS 300

#define ADDRPT(C)	strncat (report, C, 16382)


rgbtriplet scolortable[16] = { {0,0,0}, {0xff,0,0}, {0,0xff,0}, {0,0,0xff}, 
							 {0xff,0,0xff}, {0xff,0xff,0}, {0,0xff,0xff}, 
							 {0x90,0x00,0x90}, {0x7f,0x7f,0x7f}, {0x7f,0,0},
							 {0,0x7f,0}, {0,0,0x7f}, 
							 {0x7f,0,0x7f}, {0x7f,0x7f,0}, {0,0x7f,0x7f}, 
							 {0xff,0xff,0xff} };




float hxmin, hxmax, hymin, hymax;		/* Data value range */
double lhxmin, lhxmax, lhymin, lhymax;	/* Drawing area value range */
int fhxmin, fhxmax, fhymin, fhymax;	/* useable area within graph window, pixels */
float delta;							/* Increment between bins */
int buffered_uid;						/* The last image buffered */
int buffered_slice;						/* last slice buffered */
int last_numbins;						/* Number of bins in last calculation */
float statsums[8];						/* Sx,Sxx, Sxr,Sxg,Sxb, Sxxr, Sxxg, Sxxb */
long statcnt[4];						/* cnt, rcnt, gcbt, bcnt */
int   cls[MAXCLASS];	    /* Class counters (bins), grayscale */
int   rcls[MAXCLASS];	    /* Class counters (bins), red */
int   bcls[MAXCLASS];	    /* Class counters (bins), green */
int   gcls[MAXCLASS];	    /* Class counters (bins), blue */
int   fcls[MAXCLASS];	    /* filtered class counters */
int   pvcls[MAXCLASS];	    /* filtered class counters - peaks and valleys */
char report[16384];			/* Text results */
char	histogram_logdsp;	/* To display y axis in log scale */

GtkWidget *stat_drawingarea;
GtkWidget *stat_numbins_spinbutton;
GtkWidget *stat_quantile_spinbutton;
GtkWidget *stat_zeroisoor_button;
GtkWidget *stat_currentslice_button;
GtkWidget *stat_cumulative_button;
GtkWidget *stat_cluster_button;
GtkWidget *stat_logview_button;
GtkWidget *stat_recalc_button;
GtkWidget *stat_textview;




void StatClearDraw(GtkWidget* draw)
{
int ix,iy;
GdkGC* gc;

	ix = draw->allocation.width;
	iy = draw->allocation.height;
	
	/* Initialize the fx/fy values */
	
	fhxmin = 70;	/* Leave some space at the left side for y axis numbering */
	fhxmax = ix-20;
	fhymin = 20;
	fhymax = iy-40;
	
	gc = gdk_gc_new (draw->window);	/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->white_gc); /* Copy some values */

	gdk_draw_rectangle (draw->window, gc, TRUE, 0,0,ix,iy);
	gdk_gc_destroy (gc);	/* free the memory associated with the gc */

}



int SUserX(double x)
{
int ix;
	
	ix = IROUND ((x-lhxmin)*(fhxmax-fhxmin)/(lhxmax-lhxmin) + fhxmin);
	if (ix>fhxmax) ix=fhxmax;
	else if (ix<fhxmin) ix = fhxmin;
	return ix;
}

int SUserY(double y)
{
int iy;

	if (histogram_logdsp)
	{
		if (y < 1)
			iy=fhymax;
		else
		{
			y = log10 (y);
			iy = (int)(fhymax - (y-lhymin)*(fhymax-fhymin)/(lhymax-lhymin));
		}
	}
	else
		iy = IROUND(fhymax - (y-lhymin)*(fhymax-fhymin)/(lhymax-lhymin));

	if (iy>fhymax) iy=fhymax;
	else if (iy<fhymin) iy = fhymin;
	return iy;
}


void stat_determine_scale ()		/* hxmin, hxmax, hymin, hymax determined elsewhere */
{

}


void stat_frame(GtkWidget* draw)
{
double x,y;
double lstep;
int ix,iy;
char s[255];
GdkGC* gc;
PangoLayout *playout;
int ox, oy, fontwidth, fontheight;
int numticks;

	StatClearDraw (draw);		/* Clear and initialize */
	stat_determine_scale ();

	lhxmin=hxmin; lhxmax=hxmax;
	minimax (&lhxmin, &lhxmax);

	if (histogram_logdsp)			/* Note that unlike hymin,hymax we have lhymin,lhymax in LOG10 scale */
	{
		if (hymin<10) 
			lhymin=0;				/* Minimum floor: 10^0 = 1 */
		else
			lhymin = floor(log10 (hymin));

		lhymax = ceil(log10(hymax));		/* Extend to the next order of magnitude */
		dp (2, "Log y-axis scale: Value %f to %f, decades %f to %f\n",hymin,hymax, lhymin, lhymax);
	}
	else
	{
		lhymin=hymin; lhymax=hymax;
		minimax (&lhymin, &lhymax);
	}



	/* Now draw axes, then ticks etc */

	gc = gdk_gc_new (draw->window);	/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->black_gc); /* Copy some values */
	if (histogram_logdsp) y = pow (10,lhymin); else y = lhymin;
	gdk_draw_line (draw->window, gc, SUserX(lhxmin),SUserY(y), SUserX(lhxmax),SUserY(y)); 
	gdk_draw_line (draw->window, gc, SUserX(lhxmin),SUserY(y)+1, SUserX(lhxmax),SUserY(y)+1); 

	/* handle the x and y axis ticks. First, we need a dummy Pango layout to extract font size */
	
	sprintf (s, "X");			/* a single dummy character */
	playout = gtk_widget_create_pango_layout (draw, s);
	pango_layout_get_pixel_size (playout, &fontwidth, &fontheight);
	g_object_unref(playout);
	numticks = draw->allocation.width/(6*fontwidth);
	lstep = calcstep (lhxmin, lhxmax, numticks);

	for (x=lhxmin; x<=lhxmax; x+=lstep)
	{
		ix = SUserX(x); iy = SUserY(lhymin);
		gdk_draw_line (draw->window, gc, ix,iy, ix,iy+5);
		sprintf (s, "%4.1f",x);
		playout = gtk_widget_create_pango_layout (draw, s);
		pango_layout_get_pixel_size (playout, &ox, &oy);
		gdk_draw_layout (draw->window, gc, ix-ox/2, iy+8, playout);
		g_object_unref(playout);
	}

	if (histogram_logdsp)
	{
		y = pow (10,lhymin); x = pow (10,lhymax);
		gdk_draw_line (draw->window, gc, SUserX(lhxmin),SUserY(y), SUserX(lhxmin),SUserY(x)); 
		gdk_draw_line (draw->window, gc, SUserX(lhxmin)-1,SUserY(y), SUserX(lhxmin)-1,SUserY(x)); 
		numticks=lhymax-lhymin;
		lstep=10;
		for (y=pow(10,lhymin); y<=pow(10,lhymax); y*=lstep)
		{
			ix = SUserX(lhxmin); iy = SUserY(y);
			gdk_draw_line (draw->window, gc, ix,iy, ix-5,iy);
			sprintf (s, "%4.0f",y);	/* Counts are always integer */
			playout = gtk_widget_create_pango_layout (draw, s);
			pango_layout_get_pixel_size (playout, &ox, &oy);
			gdk_draw_layout (draw->window, gc, ix-ox-15, iy-oy/2, playout);
			g_object_unref(playout);
		}
	}
	else
	{
		gdk_draw_line (draw->window, gc, SUserX(lhxmin),SUserY(lhymin), SUserX(lhxmin),SUserY(lhymax)); 
		gdk_draw_line (draw->window, gc, SUserX(lhxmin)-1,SUserY(lhymin), SUserX(lhxmin)-1,SUserY(lhymax)); 
		numticks = draw->allocation.height/(2*fontheight);
		lstep = calcstep (lhymin, lhymax, numticks);

		for (y=lhymin; y<=lhymax; y+=lstep)
		{
			ix = SUserX(lhxmin); iy = SUserY(y);
			gdk_draw_line (draw->window, gc, ix,iy, ix-5,iy);
			sprintf (s, "%4.0f",y);	/* Counts are always integer */
			playout = gtk_widget_create_pango_layout (draw, s);
			pango_layout_get_pixel_size (playout, &ox, &oy);
			gdk_draw_layout (draw->window, gc, ix-ox-15, iy-oy/2, playout);
			g_object_unref(playout);
		}
	}

	gdk_gc_destroy (gc);	/* free the memory associated with the gc */

}

void stat_drawbar (GtkWidget* draw, int col, float x0, float y0, float x1, float y1)
{
int xl, xr, yl, yr, i;
GdkGC* gc;
GdkColor color;

	gc = gdk_gc_new (draw->window);	/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->black_gc); /* Copy some values */
	color.red = scolortable[(col) & 0xf].red << 8;
	color.blue = scolortable[(col) & 0xf].blue << 8;
	color.green = scolortable[(col) & 0xf].green << 8;
	gdk_color_alloc (gdk_colormap_get_system(), &color);
	gdk_gc_set_foreground (gc, &color);

	xl = SUserX (x0);
	xr = SUserX (x1);
	yl = SUserY (y0);
	yr = SUserY (y1);

	for (i=xl; i<=xr; i++)
	{
		gdk_draw_line (draw->window, gc, i, yl, i, yr);
	}
	gdk_gc_destroy (gc);	/* free the memory associated with the gc */
}


void stat_drawline (GtkWidget* draw, int col, float x0, float y0, float x1, float y1)
{
int xl, xr, yl, yr;
GdkGC* gc;
GdkColor color;

	gc = gdk_gc_new (draw->window);	/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->black_gc); /* Copy some values */
	color.red = scolortable[(col) & 0xf].red << 8;
	color.blue = scolortable[(col) & 0xf].blue << 8;
	color.green = scolortable[(col) & 0xf].green << 8;
	gdk_color_alloc (gdk_colormap_get_system(), &color);
	gdk_gc_set_foreground (gc, &color);

	xl = SUserX (x0);
	xr = SUserX (x1);
	yl = SUserY (y0);
	yr = SUserY (y1);

	gdk_draw_line (draw->window, gc, xl, yl, xr, yr);
	gdk_gc_destroy (gc);	/* free the memory associated with the gc */
}



void stat_draw_colorbar (GtkWidget* draw)
{
int xl, xr, x, iy;
float xsc, min , max, dx;
GdkGC* gc;
GdkColor color;
rgbtriplet rgb;



	xl = SUserX(lhxmin); xr = SUserX(lhxmax); 	/* x-axis left and right */
	iy = SUserY(lhymin);							/* where the bar gets positioned */
	dx = (lhxmax-lhxmin)/(xr-xl);					/* Value increment per pixel */
	xsc = lhxmin;									/* running x value for color */
	imgminmax (&mainimg, &min, &max);				/* Use the main image's scale */
	gc = gdk_gc_new (draw->window);				/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->black_gc);		/* Copy some values */

	for (x=xl; x<=xr; x++)
	{
		if ( (xsc>=min) && (xsc<=max))
		{
			rgb = cmap (xsc, min, max);
			color.red = rgb.red << 8;
			color.green = rgb.green << 8;
			color.blue = rgb.blue << 8;
			gdk_color_alloc (gdk_colormap_get_system(), &color);
			gdk_gc_set_foreground (gc, &color);
			gdk_draw_line (draw->window, gc, x,iy+25, x,iy+30);
		}
		xsc += dx;
	}


}






void stat_drawcog (GtkWidget* draw, int col, float x0, float y0)
{
int x,y;
GdkGC* gc;
GdkColor color;

	gc = gdk_gc_new (draw->window);	/* allocate mem & assign null values */
	gdk_gc_copy (gc, draw->style->black_gc); /* Copy some values */
	color.red = scolortable[(col) & 0xf].red << 8;
	color.blue = scolortable[(col) & 0xf].blue << 8;
	color.green = scolortable[(col) & 0xf].green << 8;
	gdk_color_alloc (gdk_colormap_get_system(), &color);
	gdk_gc_set_foreground (gc, &color);

	x = SUserX (x0);
	y = SUserY (y0);
	gdk_draw_line (draw->window, gc, x-1,y,x+1,y);
	gdk_draw_line (draw->window, gc, x,y-1,x,y+1);

}




double find_quantile (int* bins, int nbins, double fQuantile,long area, double delta)
{
int i;
double qu_area, qu_target;

	qu_area=0; i=-1;
	qu_target = 0.01*fQuantile*area*delta;
	
	/* find the appropriate bin */
	
	while ((i<nbins) && (qu_area < qu_target))
	{
		i++; qu_area += bins[i]*delta;
	}
	
	/* Interpolate within bin */

	return (i+1 - (qu_area-qu_target)/(bins[i]*delta) )*delta+hxmin;

}


/* This is a stand-alone variation to be used in the macro interpreter.
   FIXME: Any RGB image will be converted to gray scale */

double find_quantile_standalone (image_type* img, int z, int numbins, float fQuantile, int oorflg)
{
int i, area,x,y,bin;
float qu_area, qu_target,min,max,buf,t,t1,t2;
int bx1,bx2,by1,by2;
int* bins;

	imgminmax (img,&min,&max);
	roi_bbox (img, &bx1,&by1,&bx2,&by2);

	/* Get the appropriate number of bins ready */
	
	bins = calloc (numbins+1, sizeof (int));
	if (bins==0) fatal_memerr();
	
	/* read the image */
	
	area=0;
	for (y=by1; y<=by2; y++)
		for (x=bx1; x<=bx2; x++)
		{
			if (in_roi(x,y))
			{
				buf = readbuf_flt (img,x,y,z);
				if ((!oorflg) || (fabs(buf)>1e-12))
				{
					bin = (int) (numbins*(buf - min)/(max-min));
					if ((bin>=0) && (bin<=numbins)) 
					{
						bins[bin]++;
						area++;
					}
				}
			}
		}

	dp (2, "Image read, Area = %d points, value range %f to %f\n",area,min,max);
	for (i=0; i<=numbins; i++)
		dp (3, "Bin %d, value %f, count %d\n",i, i*(max-min)/numbins+min, bins[i]);

	qu_area=0; i=-1;
	qu_target = 0.01*fQuantile*area;
	
	/* find the appropriate bin */
	
	while ((i<=numbins) && (qu_area < qu_target))
	{
		i++; qu_area += bins[i];
		dp (3, " bin %d has %d points\n",i,bins[i]);
	}
	dp (3, " -- Bin %d: area %7.0f \n",i,qu_area);
	
	/* Interpolate within bin -- bin i just exceeds the area */

	if (i>0)
	{
		t1 = min + (float)(i-1.0)*(max-min)/(float)numbins;
		t2 = min + (float)i*(max-min)/(float)numbins;
		
		dp (2, "Bin %d, result=%f\n",i-1,t1);
		dp (2, "Bin %d, result=%f\n",i,t2);
		
		qu_target = qu_area - qu_target;
		t = t2*(1-qu_target/bins[i]) + t1*(qu_target/bins[i]);
		dp (2, "interpolated result %f\n",t);
	}
	else		/* The exception is bin 0 where we interpolate within the same bin */
	{
		t1 = min + (float)i*(max-min)/(float)numbins;
		t2 = min + (float)(i+1)*(max-min)/(float)numbins;
		t = (t2-t1)*((float)bins[0]/area)*0.01*fQuantile;
		dp (2, "interpolated result at bin 0: %f\n",t);
	}

	free (bins);
	return t;

}


/* This is a stand-alone version of the histogram computation to
	be used with the macro interpreter.
    Caveat: Any RGB image will be converted to gray scale */

double histogram_standalone (image_type* img, int z, int numbins, int what, int oorflg)
{
int i, area,x,y,bin,modeidx;
float min,max;
double buf,t,delta,mean,sdev,mode,Sx,Sxx;
int* bins;
int bx1,bx2,by1,by2;


	/* Trap median here, find_quantile is better for that purpose */
	
	if (what==1)
		return find_quantile_standalone (img, z, numbins, 50.0, oorflg);

	imgminmax (img,&min,&max);
	roi_bbox (img, &bx1,&by1,&bx2,&by2);

	/* Get the appropriate number of bins ready */
	
	bins = calloc (numbins+1, sizeof (int));
	if (bins==0) fatal_memerr();
	
	/* read the image */
	
	delta = (max-min)/numbins;	/* The width of one bin, starting with min */
	area=0; t=0; Sx=0; Sxx=0;
	for (y=by1; y<=by2; y++)
		for (x=bx1; x<=bx2; x++)
		{
			if (in_roi(x,y))
			{
				buf = readbuf_flt (img,x,y,z);
				if ((!oorflg) || (fabs(buf)>1e-12))
				{
					bin = (int) ((buf - min)/delta);
					if ((bin>=0) && (bin<=numbins)) 
					{
						bins[bin]++;
						area++;
						Sx += buf; Sxx+= SQR(buf);
					}
				}
			}
		}

	dp (2, "Image read, Area = %d points\n",area);
	if (area<2) return 0;
	
	for (i=0; i<=numbins; i++)
		dp (3, "Bin %d, value %f, count %d\n",i, i*delta+min, bins[i]);

	mean = Sx/area; sdev = SDEV(Sx,Sxx,area);

	/* Now determine the value requested */
	
	switch (what)
	{
		case 0:				/* Mode value */
			mode=0; modeidx=0;
			for (i=0; i<=numbins; i++)
			{
				if (bins[i] > mode)
				{
					modeidx=i; mode=bins[i];
				}
			}
			t = modeidx*delta+min;
			dp (2, "Mode value %f\n", t);
			break;
		case 2:				/* Mean value from raw data */
			t=mean;
			dp (2, "Mean value %f\n", t);
			break;
		case 3:				/* Variance */
			t=sdev*sdev;
			dp (2, "Variance %f\n", t);
			break;
		case 4:				/* Skew */
			t=0;
			for (y=0; y<img->ymax; y++)
				for (x=0; x<img->xmax; x++)
				{
					buf = readbuf_flt (img,x,y,z);
					if ((!oorflg) || (fabs(buf)>1e-12))
					{
						bin = (int) ((buf - min)/delta);
						if ((bin>=0) && (bin<=numbins)) 
						{
							t += pow (buf-mean, 3);
						}
					}
				}
			t = t / ( (area-1) * pow (sdev,3));
			dp (2, "Skew %f\n", t);
			break;
		case 5:				/* Kurtosis */
			t=0;
			for (y=0; y<img->ymax; y++)
				for (x=0; x<img->xmax; x++)
				{
					buf = readbuf_flt (img,x,y,z);
					if ((!oorflg) || (fabs(buf)>1e-12))
					{
						bin = (int) ((buf - min)/delta);
						if ((bin>=0) && (bin<=numbins)) 
						{
							t += pow (buf-mean, 4);
						}
					}
				}
			t = t / ( (area-1) * pow (sdev,4));
			dp (2, "Kurtosis %f\n", t);
			break;
	}

	free (bins);
	return t;

}




float find_cog (int* bins, int nbins, float delta, char xy)
{
int i,j;
float xc, yc;
float area,xi,xj;

	if (nbins<2) return 0;

	/* We use a non-bar approximation here */

	/* Summation loops for pre-area and pre-cog area of the histogram. */
	
	area = 0; xc=0; yc=0;
	for (i=0; i<nbins-1; i++)
	{
		j=i+1;
		xi = hxmin+delta*i; xj = hxmin+delta*j; 
		area += xi*bins[j] - xj*bins[i];
		xc += (xi+xj)*(xi*bins[j] - xj*bins[i]);
		yc += (bins[i]+bins[j])*(xi*bins[j] - xj*bins[i]);
		
	}
	
	/* We need to add three more vertices at the base */

	xi=xj=hxmin+delta*(nbins-1);	/* Drop-off at last bar */
	area += xi*bins[nbins-1];		/* Final Y is zero */
	xc += (xi+xj)*(xj*bins[nbins-1]);
	yc += bins[i]*( - xj*bins[nbins-1]);
	/* Baseline does not add to area because y[n] = y[n+1] = 0 */
	xi=xj=hxmin;					/* vertical line at first bar */
	area += xi*bins[0];
	xc += (xi+xj)*xi*bins[0];
	yc += bins[0]*xi*bins[0];

	/* Adjust to get final area, cog */
	
	area = 0.5*area;
	xc /= (6*area);
	yc /= (6*area);

	if (xy) return yc; else return xc;

}



/*  Statsum translation as follows:
		Sx		0
		Sxx		1
		Sxr		2
		Sxg		3
		Sxb		4
		Sxxr	5
		Sxxg	6
		Sxxgb	7

		cnt		0
		rcnt	1
		bcnt	2
		gcnt	3
		
		Those sums are stored globally to allow a caching mechanism.
		The sums/counts and the bins are computed once (and recomputed
		whenever a parameter such as numbins changed). Therefore,
		screen regeneration can be done from buffered data, speeding it
		up dramatically. The buffer becomes invalid if the image uid
		changed over buffered_uid
		
	This is the main statistic computation function. We can handle standard
	histograms for grayscale and rgb, and we can also handle cluster-mode
	histograms for grayscale images. In a standard histogram, each image value
	belongs to one bin, the the bin value is increased with each occurrence of
	a member value. We then display bin count over bin number.
	Cluster mode requires a two-step approach. First, we need to generate 
	an "overview" of the cluster sizes we have. This is the exact same step
	as above. However, the result is a list of each cluster (having a different
	number = image value) with its number of pixels. From this data, we also
	know min and max cluster size. In a second step, we arrange the sizes
	into bins, assuming we have fewer bins than different sizes.
		
*/

void statistics (GtkWidget* draw, image_type* img, int numbins, int z_oor, int oneslice, float fQuantile, int cumulative, int clustermode)
{
int x,y,z,z1,z2,i,j;
int bx1,bx2,by1,by2;
char* p;							/* used in RGB image only */
float buf; int cbuf;
int minclust,maxclust;
long k;
unsigned char ibuf;
int idx, ridx,gidx,bidx;		/* index into bin */
int clust[32768];				/* Intermediate step for clusters */
float Quantile,av,sd,xc,yc,cumul,cumul_sc;
float moment[6];
char s[1024];
int recompute;
GtkTextBuffer *buffer;
GtkTextIter start;
GtkTextIter end;



	recompute=0;
	if (buffered_uid != img->uid) recompute=1; 						/* Image changed - need to recompute all */
	if (oneslice && (buffered_slice != cur_slice)) recompute=1;

	buf=0; minclust=0; maxclust=0; 								/* silence a compiler warning */

	if (recompute)
	{

		for (i=0; i<MAXCLASS; i++)
		{
			fcls[i]=0; cls[i]=0; rcls[i]=0; bcls[i]=0; gcls[i]=0;	/* gray classes and rgb classes */
		}
		for (i=0; i<32768; i++) clust[i]=0;							/* Reset size accumulator */

		/* Determine bin size. For 8-bit type images, let's override min and max values
			and use 0 and 255 instead, with a bin size of 1 when 256 bins are selected.
			User can override this and get "crooked" bin sizes. */

		if (img->imgtype==B_RGB) 
		{
			hxmin = 0; hxmax = 255; 
			delta = (1.0+hxmax-hxmin) / (numbins);
		}
		else
		{
			imgminmax (img, &hxmin, &hxmax);
			delta = (1.0+hxmax-hxmin) / (numbins);
		}

		hymin=0; hymax=0; 
		for (i=0; i<8; i++) statsums[i]=0.0;
		for (i=0; i<4; i++) statcnt[i]=0;
		p = img->data;

		/* Run over the entire dataset. If ROI defined, observe ROI.
		   If only current slice, observe that, too */

		roi_bbox (img, &bx1,&by1,&bx2,&by2);
		z1 = 0; z2=img->zmax;
		if (oneslice) { z1=cur_slice; z2=z1+1; }

		for (z=z1; z<z2; z++)
		{
			if (img->imgtype==B_RGB)	/* RGB images handled separately - 3 channels */
			{
				for (y=by1; y<=by2; y++)
					for (x=bx1; x<=bx2; x++)
						if (in_roi(x,y))
						{
							k=3*(x + img->xmax*(y + img->ymax*z));
							ibuf=p[k]; buf=(float)ibuf;
							if ((ibuf!=0) || (!z_oor))
							{
								ridx = floor (buf/delta);		/* Index into bin, RED */
								rcls[ridx]++; if (rcls[ridx]>hymax) hymax=rcls[ridx];
								statsums[2]+=buf; statsums[5]+=buf*buf; statcnt[1]++;
							}
							ibuf=p[k+1]; buf=(float)ibuf;
							if ((ibuf!=0) || (!z_oor))
							{
								gidx = floor (buf/delta);	/* Index into bin, GRN */
								gcls[gidx]++; if (gcls[gidx]>hymax) hymax=gcls[gidx];
								statsums[3]+=buf; statsums[6]+=buf*buf; statcnt[2]++;
							}
							ibuf=p[k+2]; buf=(float)ibuf;
							if ((ibuf!=0) || (!z_oor))
							{
								bidx = floor (buf/delta);	/* Index into bin, BLU */
								bcls[bidx]++; if (bcls[bidx]>hymax) hymax=bcls[bidx];
								statsums[4]+=buf; statsums[7]+=buf*buf; statcnt[3]++;
							}
						}
			}
			else if (!clustermode)			/* Regular histo for grayscale images */
			{
				for (y=by1; y<=by2; y++)
					for (x=bx1; x<=bx2; x++)
						if (in_roi(x,y))
						{
							buf = readbuf_flt (img, x,y,z);
							if ((fabs(buf)>1e-8) || (!z_oor))
							{
								idx = floor ((buf-hxmin)/delta);	/* Index into bin */
								if ( (idx>=0) && (idx<MAXCLASS) && (idx <numbins))
								{
									cls[idx]++; 
									if (cls[idx]>hymax) hymax=cls[idx];
								}
								else
									dp (2, "Statop: Illegal bin %d for value %f at (%d,%d,%d)\n",idx,buf,x,y,z);

								statsums[0]+=buf; statsums[1]+=buf*buf; statcnt[0]++;
							}
						}
			}
			else		/* Clustermode pass 1 for grayscale only */
			{
				for (y=by1; y<=by2; y++)
					for (x=bx1; x<=bx2; x++)
						if (in_roi(x,y))
						{
							cbuf = (int)(0.5+readbuf_flt (img, x,y,z));
							if ((cbuf>0) || (!z_oor))	/* z_oor enables the "background cluster" */
							{
								if ((cbuf>0) && (cbuf<32768)) clust[cbuf]++;
							}
						}
			}
		}		/* Done: for z */
		
		/* In cluster mode, we now perform step 2, generate the cluster size histogram */
		
		if (clustermode)
		{
			minclust=999999; maxclust=0;		/*for correct bin assignment,  find smallest */
			for (i=0; i<32768; i++)				/* and largest cluster */
			{
				if (clust[i] > maxclust) maxclust = clust[i];
				if ((clust[i]>0) && (clust[i] < minclust)) minclust = clust[i];
			}
			hxmax = maxclust; hxmin = minclust;
			delta = (int)((1.0+hxmax-hxmin) / (numbins));	/* Force delta to correspond to an integer size */
			hymin=0; hymax=0; 
			
			for (i=0; i<32768; i++)
				if (clust[i]>0)
				{
					idx = floor ((clust[i]-minclust)/delta);	/* Index into bin */
					if ( (idx>=0) && (idx<MAXCLASS) && (idx <numbins))
					{
						cls[idx]++; if (cls[idx]>hymax) hymax=cls[idx];
					}
					statsums[0]+=buf; statsums[1]+=buf*buf; statcnt[0]++;
				}
		}
		
		/* For grayscale images, compute a filtered histogram */
		
		if (img->imgtype!=B_RGB)
		{
			for (i=0; i<MAXCLASS; i++) fcls[i]=cls[i];
		
			for (j=0; j<3; j++) 		/* Number of filter runs */
			{
				fcls[1] = 0.25*(fcls[0]+fcls[2])+0.5*fcls[1];
				for (i=2; i<MAXCLASS-2; i++) fcls[i] = (3*fcls[i]+2*fcls[i-1]+2*fcls[i+1]+fcls[i-2]+fcls[i+2])/9.0;
			}
			
			/* Peaks and valleys defined as a local maximum within 7 points */
			
			for (i=0; i<MAXCLASS; i++) pvcls[i]=0;
			for (i=3; i<MAXCLASS-3; i++)
			{
				if (	(fcls[i] > fcls[i+1]) && (fcls[i] > fcls[i-1])
					 && (fcls[i+1] > fcls[i+2]) && (fcls[i-1] > fcls[i-2])
					 && (fcls[i+2] > fcls[i+3]) && (fcls[i-2] > fcls[i-3])	)
					pvcls[i] = 1;

				if (	(fcls[i] < fcls[i+1]) && (fcls[i] < fcls[i-1])
					 && (fcls[i+1] < fcls[i+2]) && (fcls[i-1] < fcls[i-2])
					 && (fcls[i+2] < fcls[i+3]) && (fcls[i-2] < fcls[i-3])	)
					pvcls[i] = 2;
			}
		}
		
		buffered_uid = img->uid;		/* indicate that we buffered stats for this image */
		buffered_slice = cur_slice;
	}
			
	/* Prepare the report and draw the histogram bars */
	
	report[0]=0;
	strcpy (s, "\n\n"); ADDRPT(s);

	stat_frame (stat_drawingarea);
	
	if (img->imgtype==B_RGB)	/* 3 staggered bars for RGB */
	{
	
		/* Finalize Avg and SD */
		double av_r = statsums[2]/statcnt[1];
		double sd_r = SDEV(statsums[2],statsums[5],statcnt[1]);
		double av_g = statsums[3]/statcnt[2];
		double sd_g = SDEV(statsums[3],statsums[6],statcnt[2]);
		double av_b = statsums[4]/statcnt[3];
		double sd_b = SDEV(statsums[4],statsums[7],statcnt[3]);
	
		for (i=0; i<numbins; i++)
		{
			stat_drawbar (draw, 1, delta*i+hxmin, 0, delta*(i+0.5)+hxmin, rcls[i]);
			stat_drawbar (draw, 2, delta*(i+0.25)+hxmin, 0, delta*(i+0.75)+hxmin, gcls[i]);
			stat_drawbar (draw, 3, delta*(i+0.5)+hxmin, 0, delta*(i+1)+hxmin, bcls[i]);
		}

		sprintf (s, "Average and SD values for RGB image\n");
		ADDRPT (s);
		sprintf (s, "Red %7.2f (%7.2f)  Green %7.2f (%7.2f)  Blue  %7.2f (%7.2f)\n",
					av_r,sd_r,av_g,sd_g,av_b,sd_b);
		ADDRPT(s);
		sprintf (s, "\nHistogram values (Order R-G-B)\n");
		ADDRPT(s);
		for (i=0; i<numbins; i++)
		{
			sprintf (s, "%8.2f  %8.2f     %7d  %7d  %7d\n",
				delta*i+hxmin,delta*(i+1)+hxmin,rcls[i],gcls[i],bcls[i]);
			ADDRPT(s);
		}
		sprintf (s, "\nQuantile value for %3.0f percent quantile\n",fQuantile);
		ADDRPT (s);

	   	Quantile = find_quantile (rcls, numbins, fQuantile, statcnt[1], delta);
		stat_drawbar (draw, 9, Quantile, hymin, Quantile, hymax);
		sprintf (s, "%7.1f",Quantile);
		ADDRPT(s);

	   	Quantile = find_quantile (gcls, numbins, fQuantile, statcnt[2], delta);
		stat_drawbar (draw, 10, Quantile, hymin, Quantile, hymax);
		sprintf (s, "  %7.1f",Quantile);
		ADDRPT(s);

	   	Quantile = find_quantile (bcls, numbins, fQuantile, statcnt[3], delta);
		stat_drawbar (draw, 11, Quantile, hymin, Quantile, hymax);
		sprintf (s, "  %7.1f\n",Quantile);
		ADDRPT(s);


	}
	else		/* Single black bar graph for gray */
	{
	
		/* search the requested quantile (interpolate within class found) */
	   	Quantile = find_quantile (cls, numbins, fQuantile, statcnt[0], delta);
		av = statsums[0]/statcnt[0];
		sd = SDEV(statsums[0],statsums[1],statcnt[0]);
		xc = find_cog (cls, numbins, delta, 0);
		yc = find_cog (cls, numbins, delta, 1);
	
		for (i=0; i<numbins; i++)
		{
			stat_drawbar (draw, 0, delta*i+hxmin, 0, delta*(i+1)+hxmin, cls[i]);
		}
		for (i=1; i<numbins; i++)
		{
			stat_drawline (draw, 6, delta*(i-0.5)+hxmin, fcls[i-1], delta*(i+0.5)+hxmin, fcls[i]);
		}
		stat_drawbar (draw, 1, Quantile, hymin, Quantile, hymax);
		stat_drawcog (draw, 3, xc,yc);

		/* If requested, draw cumulative histogram (green line) */
		
		if (cumulative && !clustermode)
		{
			cumul=0; cumul_sc = (hymax-hymin)/statcnt[0];
			for (i=0; i<numbins; i++)
			{
				stat_drawline (draw, 2, delta*i+hxmin, cumul*cumul_sc, delta*(i+1)+hxmin, (cumul+cls[i])*cumul_sc);
				cumul += cls[i];
			}
		}

		if (!clustermode)
			stat_draw_colorbar (draw);			/* Colorbar if requested */

		/* Generate the text report */
		if (clustermode)
		{
			sprintf (s, "\nCluster-size histogram\n");
			ADDRPT(s);
			sprintf (s, "%li clusters total. \nLargest: %d pixels, smallest: %d pixels\n\n",statcnt[0],maxclust,minclust);
		}
		else
		{
			sprintf (s, "Average and SD values for grayscale image\n");
			ADDRPT (s);
			sprintf (s, "%10.2f  +/-  %10.2f (%li points)\n", av,sd,statcnt[0]);
			ADDRPT(s);

			/* For grayscale and non-cluster mode, compute the four moments  */

			for (k=0; k<6; k++) moment[k]=0.0;
			cumul=0.0;
			for (i=0; i<numbins; i++)
			{
				moment[0] += (delta*(i+0.5)+hxmin)*cls[i];
				cumul+=cls[i];
			}
			
			if (cumul>0)
			{
				moment[0] /= cumul;			/* first moment must coincide with image mean intensity */
				for (i=0; i<numbins; i++)
				{
					buf = delta*(i+0.5)+hxmin - moment[0];				/* Norm'ed center of bin value (intensity) */
					moment[1] += buf*buf*cls[i];
					moment[2] += buf*buf*buf*cls[i];
					moment[3] += buf*buf*buf*buf*cls[i];
					moment[4] += SQR(cls[i]);
					if (cls[i]>0) moment[5] -= cls[i]*log10(cls[i]/cumul);
				}
				moment[1] /= cumul;			/* Second moment must coincide with variance, i.e. square of standard deviation */
				moment[2] /= cumul*sqrt(moment[1]*moment[1]*moment[1]);		/* Skew */
				moment[3] /= cumul*sqrt(moment[1]*moment[1]*moment[1]*moment[1]);		/* Kurtosis */
				moment[3] -= 3.0;								/* Don't ask me. My guess is as good as yours. */
				moment[4] /= SQR(cumul);						/* Not really a moment, but rather the ENEGRY, sum of SQR(P) */
				moment[5] /= cumul*log10(2.0);					/* sum of P*log2(P) is entropy, not really a moment */
				sprintf (s, "\n Moments of the histogram:\n"); ADDRPT(s);
				sprintf (s, "     1st moment, mean    : %f\n",moment[0]); ADDRPT(s);
				sprintf (s, "     2nd moment, variance: %f\n",moment[1]); ADDRPT(s);
				sprintf (s, "     3rd moment, skew    : %f\n",moment[2]); ADDRPT(s);
				sprintf (s, "     4th moment, kurtosis: %f\n",moment[3]); ADDRPT(s);
				sprintf (s, "Energy of histogram: %f\n",moment[4]); ADDRPT(s);
				sprintf (s, "Entropy of histogram: %f\n",moment[5]); ADDRPT(s);
			}

			sprintf (s, "\nHistogram values\n");
		}
		ADDRPT(s);
		for (i=0; i<numbins; i++)
		{
			sprintf (s, "%8.2f  %8.2f     %7d\n",
				delta*i+hxmin,delta*(i+1)+hxmin,cls[i]);
			ADDRPT(s);
		}
		sprintf (s, "\nQuantile value for %3.0f percent quantile\n",fQuantile);
		ADDRPT (s);
		sprintf (s, "  %7.1f\n",Quantile);
		ADDRPT(s);
		sprintf (s, "Center of gravity: (%f, %f)\n\n", xc, yc);
		ADDRPT(s);
		
		/* Finally, identify histo peaks and valleys and print those, too */
		
		for (i=1; i<numbins; i++)
		{
			if (pvcls[i]==1) 
			{
				sprintf (s, "%8.2f: Peak\n", hxmin+delta*i);
				ADDRPT(s);
			}
			else if (pvcls[i]==2) 
			{
				sprintf (s, "%8.2f: Valley\n", hxmin+delta*i);
				ADDRPT(s);
			}
		}
		
		
		
	}

	last_numbins = numbins;

	strcpy (s, "\n\n"); ADDRPT(s);
	buffer = gtk_text_buffer_new (NULL);
	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_insert (buffer, &start, report, -1);
	gtk_text_view_set_buffer (GTK_TEXT_VIEW(stat_textview), buffer);


}



void statop_redraw_all (char sliceflg)
{
int numbins;
float fQuantile;
char foor;
char oneslice;
char fCumul;
char fCluster;
	
	numbins = gtk_spin_button_get_value_as_int ((GtkSpinButton*)stat_numbins_spinbutton);
	fQuantile = gtk_spin_button_get_value_as_float ((GtkSpinButton*)stat_quantile_spinbutton);
	foor = gtk_toggle_button_get_active ((GtkToggleButton*)stat_zeroisoor_button);
	oneslice = gtk_toggle_button_get_active ((GtkToggleButton*)stat_currentslice_button);
	fCumul = gtk_toggle_button_get_active ((GtkToggleButton*)stat_cumulative_button);
	fCluster = gtk_toggle_button_get_active ((GtkToggleButton*)stat_cluster_button);

	if (!sliceflg || oneslice)
	{
		statistics (stat_drawingarea, &mainimg, numbins, foor, oneslice, fQuantile, fCumul, fCluster);
	}
}


/* This helper function attempts to determine a good number of bins based on
	image type and image data. The idea here is to find a bin size that minimizes
	"missing-value" bins and minimizes histogram roughness.
*/

int statop_estimate_bins (image_type* img, int z)
{
float min, max, cost, mincost,buf;
int numbins,x,y,i,idx;
int bins[256];


	if (img->imgtype == B_RGB)
		return 128;						/* Always use 128 bins for RGB images, 3x8 bits */

	imgminmax (img, &min, &max);
	if (img->imgtype == B_CHAR)		/* 8-bit images, try to get a 2-wide bin size */
	{
		numbins = IROUND (1.0+max-min)/2;
		if (numbins < 4) numbins=4;
		return numbins;
	}

	/* All other types, start with 200 bins and work our way down to 100.
		We define a cost function, which is the squared deviation of each bin from its
		neighbor. Note that fewer bins automatically mean a higher cost (larger values!).
		Then we simply brute-force the lowest cost, which hopefully balances against
		the smoothness. */

	for (i=256; i>=100; i--)
	{
		delta = (1.0+max-min) / i;
		for (y=0; y<256; y++) bins[y]=0;

		for (y=0; y<img->ymax; y++)
			for (x=0; x<img->xmax; x++)
			{
				buf = readbuf_flt (img, x,y,z);
				idx = floor ((buf-min)/delta);	/* Index into bin */
				if ((idx>=0) && (idx<256)) bins[idx]++;
			}

		cost=0;
		for (y=1; y<255; y++)
			cost += SQR(bins[y]-bins[y-1]) + SQR(bins[y]-bins[y+1]);

		dp (3, "Histogram: %d bins, cost: %6.1f\n",i,cost);

		if ( (i==256) || (cost < mincost))
		{
			mincost = cost;
			numbins = i;
		}

	}
	return numbins;


	/* Default: Use 128 bins */

	return 128;
}




/*******************************************************************

	StatOP GUI
	
********************************************************************/




void on_stat_savedata_clicked (GtkButton *button, gpointer user_data)
{
gchar* filename;
FILE* FP;

	statop_redraw_all (0);
	ChooseFile ("Save histogram data as (XY tab delimited)", window1, 1, &filename);
	if (filename)
	{
		///FP = fopen (filename, "rt");
		FP = fopen (filename, "wt");
		fprintf (FP, "%s", report);
		fclose (FP);
		statusbar_display ("Histogram data file written");
		g_free (filename);
	}

}



void on_stat_gnuplot_clicked (GtkButton *button, gpointer user_data)
{
float *xdata, *ydata;
int n, i;
unsigned short pltopts;

	statop_redraw_all (0);
	if (last_numbins<0) return;		/* Computation unsuccessful */

	n = last_numbins;
	pltopts = PLOT_BARS;
	if (histogram_logdsp) pltopts |= PLOT_LOG_Y;

	if (mainimg.imgtype==B_RGB)	/* 3 staggered bars for RGB */
	{
		strcpy (chart_title, "RGB Intensity Histogram");
		strcpy (xaxislabel, "Image Value");
		strcpy (yaxislabel, "Frequency");

		xdata = calloc (n, sizeof(float)); ydata = calloc (3*n, sizeof(float));
		for (i=0; i<n; i++)
		{
			xdata[i] = delta*i+hxmin;
			ydata[i] = rcls[i];
			ydata[i+n] = gcls[i];
			ydata[i+n+n] = bcls[i];
		}

		pltopts |= PLOT_BARS_STAGGERED;
		send_to_gnuplot (xdata, ydata, n, 3, pltopts);

		free (xdata); free (ydata);
	}
	else		/* Single black bar graph for gray */
	{

		/* Glad I made those arrays global. */

		xdata = calloc (n, sizeof(float)); ydata = calloc (n, sizeof(float));
		for (i=0; i<n; i++)
		{
			xdata[i] = delta*i+hxmin;
			ydata[i] = cls[i];
		}

		strcpy (chart_title, "Gray-value Histogram");
		strcpy (xaxislabel, "Image Value");
		strcpy (yaxislabel, "Frequency");

		send_to_gnuplot (xdata, ydata, n, 1, pltopts);

		free (xdata); free (ydata);
	}

}



void on_stat_okbutton_clicked (GtkButton *button, gpointer user_data)
{
	stat_window_exists=0;
	gtk_widget_destroy (GTK_WIDGET(user_data));
}


void on_stat_numbins_spinbutton_changed (GtkEditable *editable, gpointer user_data)
{
	buffered_uid=-1;			/* Force a full recomputation */
	statop_redraw_all (0);

}


void on_stat_quantile_spinbutton_changed (GtkEditable *editable, gpointer user_data)
{
	statop_redraw_all (0);

}


void on_stat_zeroisoor_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	buffered_uid=-1;			/* Force a full recomputation */
	statop_redraw_all (0);

}

void on_stat_cluster_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	buffered_uid=-1;			/* Force a full recomputation */
	statop_redraw_all (0);

}

void on_stat_currentslice_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	buffered_uid=-1;			/* Force a full recomputation */
	statop_redraw_all (0);

}

void on_stat_recalc_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	buffered_uid=-1;			/* Force a full recomputation */
	statop_redraw_all (0);

}


void on_stat_cumulative_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	statop_redraw_all (0);		/* Redraw, no full recomputation */

}

void on_stat_logview_button_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	histogram_logdsp = gtk_toggle_button_get_active ((GtkToggleButton*)stat_logview_button);
	statop_redraw_all (0);		/* Redraw, no full recomputation */

}


gboolean on_statop_drawingarea_expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer drwgarea)
{

  	statop_redraw_all (0);
	return FALSE;
}






void create_stat_window (void)
{
  GtkWidget *stat_window;
  GtkWidget *stat_vbox1;
  GtkWidget *stat_label1;
  GtkWidget *stat_table1;
  GtkWidget *stat_savedata;
  GtkWidget *stat_gnuplot;
  GtkWidget *stat_okbutton;
  GtkWidget *stat_label2;
  GtkWidget *stat_label4;
  GtkObject *stat_numbins_spinbutton_adj;
  GtkObject *stat_quantile_spinbutton_adj;
  GtkWidget *hseparator1;
  GtkWidget *stat_scrolledwindow1;
  GtkTooltips *tt_numbins, *tt_quantile, *tt_recalc, *tt_oor, *tt_logdsp, *tt_cumulative;
  GtkTooltips *tt_curslice, *tt_clustmode, *tt_savebtn, *tt_dismiss, *tt_gnup;
  int default_bins;
  int y;


	default_bins = statop_estimate_bins (&mainimg, cur_slice);

	stat_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (stat_window), "stat_window", stat_window);
	gtk_container_set_border_width (GTK_CONTAINER (stat_window), 2);
	gtk_window_set_title (GTK_WINDOW (stat_window), "Stat Op");

	stat_vbox1 = vbox_in_window (stat_window, "stat_vbox1");

	stat_label1 = gtk_label_new ("Histogram");
	gtk_widget_ref (stat_label1);
	gtk_object_set_data_full (GTK_OBJECT (stat_window), "stat_label1", stat_label1,
								(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (stat_label1);
	gtk_box_pack_start (GTK_BOX (stat_vbox1), stat_label1, TRUE, TRUE, 0);

	stat_drawingarea = gtk_drawing_area_new ();
	gtk_widget_ref (stat_drawingarea);
	gtk_object_set_data_full (GTK_OBJECT (stat_window), "stat_drawingarea", stat_drawingarea,
								(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (stat_drawingarea);
	gtk_box_pack_start (GTK_BOX (stat_vbox1), stat_drawingarea, TRUE, TRUE, 0);
	gtk_widget_set_usize (stat_drawingarea, 400, 300);

	stat_table1 = MakeTable (stat_window, stat_vbox1,"stat_table1",2,7);



	/* Panel elements: Toggle buttons, save & dismiss */

	y=0;
	stat_label2 = MakeLabel (stat_window, stat_table1, "Number of bins", "stat_label2", 0,y);
	stat_numbins_spinbutton_adj = gtk_adjustment_new (default_bins, 1, 256, 1, 10, 0);
	stat_numbins_spinbutton = MakeSpinbutton (GTK_ADJUSTMENT(stat_numbins_spinbutton_adj), stat_window, 
								stat_table1, "stat_numbins_spinbutton", 1,y);
	gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (stat_numbins_spinbutton), TRUE);
	gtk_signal_connect (GTK_OBJECT (stat_numbins_spinbutton), "value-changed",
						GTK_SIGNAL_FUNC (on_stat_numbins_spinbutton_changed),NULL);
	tt_numbins = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_numbins, stat_numbins_spinbutton, "Choose number of histogram bins", NULL);

	y++;
	stat_label4 = MakeLabel (stat_window, stat_table1, "Quantile", "stat_label4", 0,y);
	stat_quantile_spinbutton_adj = gtk_adjustment_new (50, 0, 100, 1, 10, 0);
	stat_quantile_spinbutton = MakeSpinbutton (GTK_ADJUSTMENT(stat_quantile_spinbutton_adj), stat_window, 
								stat_table1, "stat_quantile_spinbutton", 1,y);
	gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (stat_quantile_spinbutton), TRUE);
	gtk_signal_connect (GTK_OBJECT (stat_quantile_spinbutton), "value-changed",
						GTK_SIGNAL_FUNC (on_stat_quantile_spinbutton_changed),NULL);
	tt_quantile = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_quantile, stat_quantile_spinbutton, "Calculate a histogram quantile (percentile)\nand display as red bar", NULL);

	y++;
	stat_recalc_button = Button_in_table (stat_window,stat_table1,"Recompute","stat_recalc_button",0,y,NULL);
	stat_logview_button = Togglebutton_in_table (stat_window,stat_table1,"Log display","stat_logview_button",1,y,NULL);
	tt_recalc = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_recalc, stat_recalc_button, "Recalculate the histogram\nfor example, with a new ROI", NULL);
	tt_logdsp = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_logdsp, stat_logview_button, "Toggle linear/logarithmic abscissa", NULL);

	y++;
	stat_zeroisoor_button = Togglebutton_in_table (stat_window,stat_table1,"Zero is OOR","stat_zeroisoor_button",0,y,NULL);
	stat_currentslice_button = Togglebutton_in_table (stat_window,stat_table1,"Current slice only (2D)","stat_curslce_button",1,y,NULL);
	tt_oor = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_oor, stat_zeroisoor_button, "Toggle exclusion of zero (black) pixels\nwhen zero is considered background.", NULL);
	tt_curslice = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_curslice, stat_currentslice_button, "Toggle histogram computation on current slice\nor on entire 3D stack (may be slow)", NULL);

	y++;
	stat_cumulative_button = Togglebutton_in_table (stat_window,stat_table1,"Show cumulative", "stat_cumulative_button", 0, y, on_stat_cumulative_button_toggled);
	stat_cluster_button = Togglebutton_in_table (stat_window,stat_table1,"Cluster mode", "stat_cumulative_button",1,y, on_stat_cluster_button_toggled);
	tt_cumulative = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_cumulative, stat_cumulative_button, "Toggle display of the cumulative histogram", NULL);
	tt_clustmode = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_clustmode, stat_cluster_button, "Toggle cluster distribution histogram\nNOTE: Requires cluster labeling by ranked clusters,\notherwise this function will display nonsensical data!", NULL);

	y++;
	stat_savedata = Button_in_table (stat_window,stat_table1,"Save Data","stat_savedata",
										0,y,on_stat_savedata_clicked);
	stat_okbutton = Button_in_table (stat_window,stat_table1,"Dismiss","stat_okbutton",
										1,y,on_stat_okbutton_clicked);
	tt_savebtn = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_savebtn, stat_savedata, "Save histogram report (below) in text file", NULL);
	tt_dismiss = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tt_dismiss, stat_okbutton, "Dismiss histogram and close window.", NULL);

	y++;
	if ((strlen(undo_path)>0) && (file_exists (gnuplotpath)))
	{
		stat_gnuplot = Button_in_table (stat_window,stat_table1,"Send to Gnuplot","stat_gnubutton",
										0,y,on_stat_gnuplot_clicked);
		tt_gnup = gtk_tooltips_new ();
		gtk_tooltips_set_tip (tt_gnup, stat_gnuplot, "Plot the histogram with Gnuplot", NULL);
	}

		/* If we link the callback function in the gtkhelpers, the set_active statement
		calls the callback, which causes a redraw on the non-existing window, leading
		to a painful death of the program. */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (stat_zeroisoor_button), TRUE);
	gtk_signal_connect (GTK_OBJECT (stat_zeroisoor_button), "toggled",
						GTK_SIGNAL_FUNC (on_stat_zeroisoor_button_toggled),stat_zeroisoor_button);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (stat_currentslice_button), TRUE);
	gtk_signal_connect (GTK_OBJECT (stat_currentslice_button), "toggled",
						GTK_SIGNAL_FUNC (on_stat_currentslice_button_toggled),stat_currentslice_button);
	gtk_signal_connect (GTK_OBJECT (stat_currentslice_button), "toggled",
						GTK_SIGNAL_FUNC (on_stat_cluster_button_toggled),stat_cluster_button);
	gtk_signal_connect (GTK_OBJECT (stat_logview_button), "toggled",
						GTK_SIGNAL_FUNC (on_stat_logview_button_toggled),stat_logview_button);
	gtk_signal_connect (GTK_OBJECT (stat_recalc_button), "clicked",
						GTK_SIGNAL_FUNC (on_stat_recalc_button_toggled),stat_recalc_button);



	/* Text Window */

	hseparator1 = MakeHsep (stat_window,stat_vbox1,"hseparator1",-1,-1);

	stat_scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_ref (stat_scrolledwindow1);
	gtk_object_set_data_full (GTK_OBJECT (stat_window), "stat_scrolledwindow1", stat_scrolledwindow1,
								(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_set_usize (stat_scrolledwindow1, 300,150);
	gtk_widget_show (stat_scrolledwindow1);
	gtk_box_pack_start (GTK_BOX (stat_vbox1), stat_scrolledwindow1, TRUE, TRUE, 0);
	
	stat_textview = gtk_text_view_new ();

	gtk_widget_ref (stat_textview);
	gtk_object_set_data_full (GTK_OBJECT (stat_window), "entry1", stat_textview,
								(GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (stat_textview);
	gtk_container_add (GTK_CONTAINER (stat_scrolledwindow1), stat_textview);
	gtk_text_view_set_editable (GTK_TEXT_VIEW(stat_textview), FALSE);

	gtk_signal_connect (GTK_OBJECT (stat_window), "destroy",
						GTK_SIGNAL_FUNC (on_stat_okbutton_clicked),stat_window);
	gtk_signal_connect (GTK_OBJECT (stat_drawingarea), "expose_event",
						GTK_SIGNAL_FUNC (on_statop_drawingarea_expose_event),
						stat_drawingarea);

	stat_window_exists=1;
	gtk_widget_show (stat_window);

}


void statop_main ()
{

	hxmin=0; hymin=0; hxmax=20; hymax=20;
	last_numbins = -1;
	histogram_logdsp = 0;
	buffered_uid=-1;
	create_stat_window ();
	
}

