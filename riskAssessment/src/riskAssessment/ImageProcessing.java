package riskAssessment;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.*;

public class ImageProcessing {
	
	//final float hxmin = 0;
	//final float hxmax = 255;
	
	//static int imageMin;
	//static int imageMax;
	//static double delta;
	
	static final int MODE = 0;
	static final int MEDIAN = 1;
	static final int MEAN = 2;
	static final int VARIANCE = 3;
	static final int SKEW = 4;
	static final int ENERGY = 5;
	static final int ENTROPY = 6;
	
	static final double RED_WEIGHT = 0.299;
	static final double GREEN_WEIGHT = 0.587;
	static final double BLUE_WEIGHT = 0.114;
	
	final static int BANANA = 0;
	final static int STRAWBERRY = 1;
	final static int ORANGE = 2;
	final static int GREENBEAN = 3;
	final static int TOMATO = 4;
	

	//Load image
	public Image LoadImage(String imagePath){
		
		Image imageObject = null;
		BufferedImage image = null;
		
		try {
			image = ImageIO.read(new File(imagePath));
			
		} catch (IOException e){
			System.out.println("ERROR importing image");
			return null;
		}
		
		int imageMin = 255;
		int imageMax = 0;
		
		//loop through image pixels
		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++) 
			{
				//get color code for image and break into RGB values  
                Color imageColor = new Color(image.getRGB(x, y));
                int red = (imageColor.getRed());
                int green = (imageColor.getGreen());
                int blue = (imageColor.getBlue());
                
                //get grayscale color value for pixel, and set pixel to new shade of gray
				int grayscale = RGBtoGrayscale(red, green, blue);
				
				//keep track of min and max values
				if (grayscale < imageMin)
					imageMin = grayscale;
				if (grayscale > imageMax)
					imageMax = grayscale;
			}
		}
		
		imageObject = new Image(image, imageMin, imageMax);
		
		return imageObject;
	}
	
	//convert image to grayscale
	public void RGBImageToGrayscale(Image imageObject){
		
		Image image = imageObject;
		
		//loop through image pixels
		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++) 
			{
				//get color code for image and break into RGB values          
                Color imageColor = new Color(image.getBufferedImage().getRGB(x, y));
                int red = (imageColor.getRed());
                int green = (imageColor.getGreen());
                int blue = (imageColor.getBlue());
                
                //get grayscale color value for pixel, and set pixel to new shade of gray
				int grayscale = RGBtoGrayscale(red, green, blue);
				
				int grayColor = (grayscale << 16) + (grayscale << 8) + grayscale; 
				image.getBufferedImage().setRGB(x, y, grayColor);
			}
		}
		
		File ouptut = null;
		try{
			
			ouptut = new File("grayscale.jpg");
			ImageIO.write(image.getBufferedImage(), "jpg", ouptut);
			
		} catch (Exception e){
			System.out.println ("Error creating gray JPG");
		}
		
	}
 
	//create a histogram of the data contained an image
	public Histogram createHistogram(Image imageToProcess, int numBins) { 

		Histogram histogram = null;
		
		int bins[] = null;
		double delta = 0; 
		double sum = 0, sumSquared = 0,
				mean = 0, stddev = 0, variance = 0;
		int bin = 0, numberOfPixels = 0;

		//load image to process
		Image imageObject = imageToProcess;
		
		//convert image to grayscale and get min and max brightness values
		RGBImageToGrayscale(imageObject);
		
		//get appropriate number of bins
		bins = new int[numBins +1];
		
		//get the width of one bin, starting with min
		delta = (double)(imageObject.getMaxPixelValue()-imageObject.getMinPixelValue())/(double)numBins;
		
		//loop through image pixels
		for (int y = 0; y < imageObject.getHeight(); y++)
		{
			for (int x = 0; x < imageObject.getWidth(); x++) 
			{
				//get grayscale value
				int imageColor = imageObject.getBufferedImage().getRGB(x, y);
                int grayscale = imageColor & 0x000000ff;
				
                //get the bin number to put value in
                bin = (int)((grayscale - imageObject.getMinPixelValue())/delta);
                
                //if bin number is valid
                if ((bin >= 0) && (bin <= numBins)){
                	
                	//increment value for specified bin
                	if (grayscale == imageObject.getMaxPixelValue()){
                		bins[bin-1]++;
                	}
                	else {
                		bins[bin]++;
                	}
                	
                	//increment total number of pixels
                	numberOfPixels++;
                	
                	//keep track of grayscale sums
                	sum += grayscale; 
                }
			}
		}
		
		
		//******************************************//
		//**********HISTOGRAM CALCULATIONS**********//
		//******************************************//
		
		//MEAN CALCULATION
		mean = sum/(double)numberOfPixels;
		
		//VARIANCE CALCULATION
		//loop through image pixels to get standard deviation
		for (int y = 0; y < imageObject.getHeight(); y++)
		{
			for (int x = 0; x < imageObject.getWidth(); x++) 
			{
				//get grayscale value
				int imageColor = imageObject.getBufferedImage().getRGB(x, y);
                int grayscale = imageColor & 0x000000ff;
                
                //if bin number is valid
                if ((bin >= 0) && (bin <= numBins)){

                	sumSquared+= Math.pow(grayscale - mean, 2);
                }
			}
		}
		
		//caluclate standard deviation
		stddev = Math.sqrt(sumSquared / (double)numberOfPixels);
		
		//variance
		variance = stddev * stddev;

		//MODE CALCULATION
		int high = 0, highBin = 0;
		double mode = 0;
		for (int i = 0; i <= numBins; i++){
			
			if (bins[i] > high){
				highBin = i; 
				high = bins[i];
			}
		}
		mode = highBin * delta + (double)imageObject.getMinPixelValue();	

		//SKEW CALCULATION
		double skew = 0;
		
		for (int y = 0; y < imageObject.getHeight(); y++)
		{
			for (int x = 0; x < imageObject.getWidth(); x++) 
			{
				//get grayscale value
				int imageColor = imageObject.getBufferedImage().getRGB(x, y);
                int grayscale = imageColor & 0x000000ff;
                
                //get the bin number to put value in
                bin = (int)((grayscale - imageObject.getMinPixelValue())/delta);
                if ((bin>=0) && (bin <= numBins)) 
				{
					skew += Math.pow(grayscale-mean, 3);
				}
			}
		}
		skew = skew / ( (numberOfPixels-1) * Math.pow(stddev,3));

		//ENERGY CALCULATION
		int squaredSum = 0;
		float cumul = 0;
		double energy = 0;
		for (int i = 0; i < numBins; i++){
			
			cumul += bins[i];
			squaredSum += Math.pow(bins[i], 2);
		}
		energy = squaredSum / Math.pow(cumul, 2);

		//ENTROPY CALCULATION
		float entropy = 0;
		cumul = 0;
		for (int i = 0; i < numBins; i++){
			
			cumul += bins[i];
		}
		
		for (int i = 0; i < numBins; i++){
						
			if (bins[i] > 0){
				
				entropy -= bins[i]*Math.log10(bins[i]/cumul);
			}
		}
		entropy /= cumul*Math.log10(2.0);

		//create histogram object
		histogram = new Histogram(bins, delta, mean, variance, mode, 
								  skew, entropy, energy);
		
		return histogram;
		
	}
	
	//use of Freedman�Diaconis rule to determine appropriate bin width/number of bins
	public int calculateBinNumber(Image image) {
		
		double h = 0;
		int[] dataP = new int[256];
		
		//create histogram of pixels
		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++) 
			{
				//get grayscale value
				int imageColor = image.getBufferedImage().getRGB(x, y);
                int grayscale = imageColor & 0x000000ff;
                
                dataP[grayscale]++;
			}
		}
		
		//get 25% value
		int numberOfPixels = image.getHeight() * image.getWidth();
		int twenty5 = (int)Math.round(numberOfPixels * 0.25);
		
		//determine Q1
		int pixelCount = 0;
		int i = -1;
		do {
			
			i++;
			pixelCount += dataP[i];
			
		} while (pixelCount < twenty5);
		int q1 = i;
		
		//determine Q3
		pixelCount = 0;
		i = 256;
		do {
			
			i--;
			pixelCount += dataP[i];
			
		} while (pixelCount < twenty5);	
		int q3 = i;

		//inter-quartile range
		double IQR = 0;
		IQR = q3-q1;

		//Freedman-Diaconis rule
		h = 2 * IQR * Math.pow(numberOfPixels, -1.0/3.0);
		
		int binNumber = 0;
		
		if (IQR == 0){
			binNumber = (int)Math.round(numberOfPixels*0.0005);
			return binNumber;
		}
		
		//number of bins = (max-min)/h
		binNumber = (int) Math.round((image.getMaxPixelValue() - image.getMinPixelValue()) / h);

		return binNumber;
		
	}
	
	public Image hysteresisSegmentation (Image image, int tlow, int thigh, Histogram histogram)
	{
	Image returnImage = image;
	int x,y;
	int pixelcnt;
	int redFill = 255 << 8;

		/* Allocate result image. This is a binary mask, so we use B_CHAR */

		returnImage.setMaxPixelValue(255);
		returnImage.setMinPixelValue(0);

		int pixelChangeCount = 0;
		int nonwhitePixels = 0;
		
		for (y=0; y<returnImage.getHeight(); y++){
			
			for (x=0; x<returnImage.getWidth(); x++)
			{
				//get grayscale value
				Color imageColor = new Color(image.getBufferedImage().getRGB(x, y));
                int red = (imageColor.getRed());
                int green = (imageColor.getGreen());
                int blue = (imageColor.getBlue());
                
                //get grayscale color value for pixel, and set pixel to new shade of gray
				int grayscale = RGBtoGrayscale(red, green, blue);
				
				//account for white background
				if (grayscale >= thigh && grayscale < 235)
				{
					returnImage.getBufferedImage().setRGB(x, y, redFill);
					pixelChangeCount++;
				}
				
				if (grayscale < 255){
					nonwhitePixels++;
				}
			}
		}

		/* Pass 2: Iteratively set all mask pixels in the 8-neighborhood of a set
			mask pixel provided that the image value exceeds tlow.
			This specific implementation is short, but slightly on the inefficient side. */

		do
		{
			pixelcnt  = 0;			/* Number of pixels we set during this iteration */
		
				for (y=0; y<returnImage.getHeight(); y++)
				{
				
					for (x=0; x<returnImage.getWidth(); x++)
					{
						//get grayscale value
						Color imageColor = new Color(image.getBufferedImage().getRGB(x, y));
		                int red = (imageColor.getRed());
		                int green = (imageColor.getGreen());
		                int blue = (imageColor.getBlue());
		                
		                //get grayscale color value for pixel, and set pixel to new shade of gray
						int grayscale = RGBtoGrayscale(red, green, blue);
						
						if (grayscale != RGBtoGrayscale(255, 0, 0)		/* Mask bit must not be set */
							&& grayscale <= tlow)		/* Do this first for computational efficiency */
						{
							returnImage.getBufferedImage().setRGB(x, y, redFill);
							pixelChangeCount++;
							pixelcnt++;
						}
					}
				}

			//dp (1, "Hyteresis_threshold: Iteration %d, pixels added to mask: %d\n",++itercnt, pixelcnt);	
		}
		while (pixelcnt > 0);
		
		double pixelChangePercentage = (double)pixelChangeCount / (double)nonwhitePixels;
		System.out.println(pixelChangePercentage);
		histogram.setUnhealthyPixelPercentage(pixelChangePercentage);
		
		/* At this time, we can do one of two things.
			- we can hand back the mask, or
			- we can multiply the original image with the mask and return the thresholded image */

		return returnImage;
	}
	
	public int[] getLowHighThresholds(Histogram histogram, int minPixelValue, int fruit)
	{
		int[] thresholdValues = new int[2];
		int numberOfBins = histogram.getNumberOfBins();
		int[] bins = histogram.getHistogramBins();
		
		int maxBinVal = 0, maxBin = 0;
		
		//ADDITION
		int expectedPixelValue;
		double binFactor = 0.2;
		switch (fruit){
			case BANANA:
				//experimentally determined shade of yellow
				expectedPixelValue = 145;
				break;
			case STRAWBERRY:
				//experimentally determined shade of red
				expectedPixelValue = 51;
				break;
			case ORANGE:
				//experimentally determined shade of orange
				expectedPixelValue = 141;
				break;
			case GREENBEAN:
				//experimentally determined shade of green
				expectedPixelValue = 66;
				binFactor = 0.10;
				break;
			case TOMATO:
				//experimentally determined shade of red
				expectedPixelValue = 86;
				break;
			default:
				//error
				expectedPixelValue = 0;
				break;
		}

		
		//-2 to account for extra bin in histogram + white space (255)
		for (int i = 0; i< numberOfBins - 2; i++){
			
			if (bins[i] > maxBinVal){
				
				//ADDITION
				int binMin = (int)Math.round(i * histogram.getDelta() + minPixelValue);
				int binMax = (int)Math.round(i * histogram.getDelta() + histogram.getDelta() + minPixelValue);
				int binAverage = (int)Math.round((binMax + binMin) / 2.0);
				
				//ADDITION
				//TODO: 10??
				if (binAverage > (expectedPixelValue - 20) && binAverage < (expectedPixelValue + 20))
				{
					maxBinVal = bins[i];
					maxBin = i;
				}
			}
				
		}

		//find average pixel value of the selected bin
		int binMin = (int)Math.round(maxBin * histogram.getDelta() + minPixelValue);
		int binMax = (int)Math.round(maxBin * histogram.getDelta() + histogram.getDelta() + minPixelValue);
		int binAverage = (int)Math.round((binMax + binMin) / 2.0);
		
		//threshhold to define healthy part of fruit
		int thresh = (int)Math.round(histogram.getDelta() * histogram.getNumberOfBins() * binFactor);
		
		thresholdValues[0] = binAverage - thresh;
		thresholdValues[1] = binAverage + thresh;
		
		return thresholdValues;
	}
	
	public double getHealthRisk(Histogram histogram, int fruit){
		
		double healthRisk = 0;
		double healthyEntropy = 0;
		double healthyEnergy = 0;
		double healthySkew = 0;
		double slope = 0;
		double intercept = 0;
		
		switch (fruit){
			case BANANA:
			
				//experimentally determined banana values
				slope = 125.678;
				intercept = -258.813;
				healthyEntropy = 2.1;
				healthyEnergy = 0.005;
				healthySkew = -0.85;
				break;
			
			case STRAWBERRY:
			
				//experimentally determined strawberry values
				slope = 111.005;
				intercept = -326.972;
				healthyEntropy = 2.65;
				healthyEnergy = 0.1;
				healthySkew = 0.4;
				break;
			
			case ORANGE:
				/*
				//experimentally determined orange values
				slope = ;
				intercept = ;
				healthyEntropy = ;
				healthyEnergy = ;
				healthySkew = ;
				break;
				*/
			
			case GREENBEAN:
				/*
				//experimentally determined greenbean values
				slope = ;
				intercept = ;
				healthyEntropy = 2.55;
				healthyEnergy = 0.2;
				healthySkew = 0.65;
				break;
				*/
			case TOMATO:
				/*
				//experimentally determined tomato values
				slope = ;
				intercept = ;
				healthyEntropy = 4.68;
				healthyEnergy = 0.07;
				healthySkew = 1.1;
				break;	
				*/
		}
		
		healthRisk = slope * histogram.getEntropy() + intercept;
		
		/*
		double entropyRatio, energyRatio, skewRatio;
		
		//get entropy ratio
		if (histogram.getEntropy() > healthyEntropy)
			entropyRatio = histogram.getEntropy() / healthyEntropy;
		else 
			entropyRatio = healthyEntropy / histogram.getEntropy();
		
		//get energy ratio
		if (histogram.getEnergy() > healthyEnergy)
			energyRatio = histogram.getEnergy() / healthyEnergy;
		else 
			energyRatio = healthyEnergy / histogram.getEnergy();
		
		//get skew ratio
		if (histogram.getSkew() > healthySkew)
			skewRatio = histogram.getSkew() / healthySkew;
		else 
			skewRatio = healthySkew / histogram.getSkew();
		
		healthRisk = (histogram.getUnhealthyPixelPercentage() + 1) * (0.9*entropyRatio + 0.05*skewRatio + 0.05*energyRatio);
		*/
		
		//get on 0-100 scale
		if (healthRisk > 100)
			healthRisk = 100;
		else if (healthRisk < 0)
			healthRisk = 0;	
		
		return healthRisk;
	}
	
	public int getRemainingLife(double healthRisk, int fruit){
		
		int remainingLife = 0;
		int shelfLife = 0;
		double cutoff = 0;
		double healthRiskDec = healthRisk / 100;
		
		switch (fruit){
		case BANANA:
			shelfLife = 7;
			cutoff = 0.8;
			break;
		case STRAWBERRY:
			shelfLife = 6;
			cutoff = 0.52;
			break;
		case GREENBEAN:
			shelfLife = 8;
			break;
		case TOMATO:
			shelfLife = 7;
			break;
		}
		
		//certain health risk per fruit that is too bad to eat based on picture perception
		if (healthRiskDec > cutoff){
			remainingLife = 0;
		}
		else{
			double factor = (cutoff - healthRiskDec) / cutoff;
			remainingLife = (int)Math.round(shelfLife * factor);
		}
		
		return remainingLife;
	}
	
	//convert a RGB pixel value to a grayscale value
	public static int RGBtoGrayscale(int red, int green, int blue){
		
		int gray = (int)Math.round(((double)red * RED_WEIGHT) + ((double)green * GREEN_WEIGHT) + ((double)blue * BLUE_WEIGHT));
		return gray;		
	}
	
}

