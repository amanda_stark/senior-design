package riskAssessment;

public class Histogram {

	private int[] histogramBins = null;
	private double delta = 0;
	private double mean = 0;
	private double mode = 0;
	private double variance = 0;
	private double skew = 0;
	private double entropy = 0;
	private double energy = 0;
	private double unhealthyPixelPercentage = 0;
	
	public Histogram(int[] histogramBins, double delta, double mean, double variance,
					double mode, double skew, double entropy, double energy){
		
		this.histogramBins = histogramBins;
		this.delta = delta;
		this.mean = mean;
		this.mode = mode;
		this.variance = variance;
		this.skew = skew;
		this.entropy = entropy;
		this.energy = energy;
	}
	
	public int getNumberOfBins(){
		
		return histogramBins.length;
	}
	
	public int[] getHistogramBins(){
		
		return histogramBins;
	}

	public double getDelta() {
		return delta;
	}

	public double getMean() {
		return mean;
	}

	public double getMode() {
		return mode;
	}

	public double getVariance() {
		return variance;
	}

	public double getSkew() {
		return skew;
	}

	public double getEntropy() {
		return entropy;
	}

	public double getEnergy() {
		return energy;
	}
	
	public double getUnhealthyPixelPercentage(){
		return unhealthyPixelPercentage;
	}
	
	public void setUnhealthyPixelPercentage(double perc){
		this.unhealthyPixelPercentage = perc; 
	}
}
