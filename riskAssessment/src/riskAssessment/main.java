package riskAssessment;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;



public class main {
	
	final static int W_HAAR = 0;
	final static int W_DAUB4 = 1;
	final static int W_DAUB8 = 2;
	final static int W_DAUB12 = 3;
	final static int W_DAUB20 = 4;
	final static int W_COIF6 = 5;
	final static int W_COIF12 = 6;
	final static int W_SYM4 = 7;
	final static int W_SYM8 = 8;
	final static int W_SYM12 = 9;
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String imageToImport = "fruit/berry1_1.png";
		
		ImageProcessing ip = new ImageProcessing();
		Image originalImage = ip.LoadImage(imageToImport);
		Image grayScaleImage = ip.LoadImage(imageToImport);
		Image segmentedImage = ip.LoadImage(imageToImport);
		
		int numberOfBins = ip.calculateBinNumber(grayScaleImage);
		Histogram grayscaleHist = ip.createHistogram(grayScaleImage, numberOfBins);
		
		
		System.out.println("Histogram:");
		for (int i = 0; i < grayscaleHist.getNumberOfBins() -1; i++){
			
			System.out.println("Bin " + i + "[" + (i*grayscaleHist.getDelta() + grayScaleImage.getMinPixelValue()) + " - " + 
					((i*grayscaleHist.getDelta())+grayscaleHist.getDelta() + grayScaleImage.getMinPixelValue()) + "] count: " + grayscaleHist.getHistogramBins()[i]);
		}
		
		
		System.out.println("\n\nHistogram data:");
		System.out.println("DELT: " + grayscaleHist.getDelta() + "\n" +
				"MEAN: " + grayscaleHist.getMean() + "\n" +
				"MODE: " + grayscaleHist.getMode() + "\n" + 
				"VAR: " + grayscaleHist.getVariance() + "\n" +
				"SKEW: " + grayscaleHist.getSkew() + "\n" +
				"ENERGY: " + grayscaleHist.getEnergy() + "\n" +
				"ENTROPY: " + grayscaleHist.getEntropy());
		
		//Hysteresis Thresholding
		
		int[] thresh = ip.getLowHighThresholds(grayscaleHist, grayScaleImage.getMinPixelValue(), ImageProcessing.BANANA);
		
		Image hysteresisImage = ip.hysteresisSegmentation(originalImage, thresh[0], thresh[1], grayscaleHist);
		File output = null;
		try{
			
			output = new File("hysteresis_thresholding.jpg");
			ImageIO.write(hysteresisImage.getBufferedImage(), "jpg", output);
			
		} catch (Exception e){
			System.out.println ("Error creating wavelet JPG");
		}
		
		
		// Risk algorithm
		double risk = ip.getHealthRisk(grayscaleHist, ImageProcessing.BANANA);
		
		System.out.println("Health Risk: " + risk);
		
		//Remaining life
		int remaining = ip.getRemainingLife(risk, ImageProcessing.BANANA);
		
		System.out.println("Remaining Life: " + remaining);
		
	}

}
