package riskAssessment;

import java.awt.image.BufferedImage;

public class Image {

	private BufferedImage image;
	private int pixelValueMin = 0;
	private int pixelValueMax = 0;
	
	public Image(BufferedImage image, int min, int max){
		
		this.image = image;
		this.pixelValueMin = min;
		this.pixelValueMax = max;
	}
	
	public Image(Image imageToCopy){
		
		this.image = imageToCopy.image;
		this.pixelValueMin = imageToCopy.pixelValueMin;
		this.pixelValueMax = imageToCopy.pixelValueMax;
	}
	
	public int getMaxPixelValue(){
		return pixelValueMax;
	}
	
	public void setMaxPixelValue( int max){
		pixelValueMax = max;
	}
	
	public int getMinPixelValue(){
		return pixelValueMin;
	}
	
	public void setMinPixelValue( int min){
		pixelValueMin = min;
	}
	
	public int getHeight(){
		return image.getHeight();
	}
	
	public int getWidth(){
		return image.getWidth();
	}
	
	public BufferedImage getBufferedImage(){
		return image;
	}
}
